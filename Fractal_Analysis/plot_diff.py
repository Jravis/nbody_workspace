import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
import argparse


#mpl.rcParams['font.family'] = 'sans-serif'
#mpl.rcParams['font.sans-serif'] = ['Tahoma']
#mpl.rcParams['text.usetex'] = True

#set some plotting parameters
#rcParams['figure.figsize'] = (8,6)
#rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
#plt.style.use("classic")
plt.style.use("ggplot")
#plt.style.use("seaborn-white")

qd = 2

# rbin	rbin_mid	Dq_mean	Dq_std	(D-Dqd)_mean	(D-Dqd)_std	Dq_ana	avpart	Dq_clust_mean	Dq_clust_std
rmid    = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=0)
                        
Dq_mean    = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=1)
Dq_std    = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=2)
                        
Delat_Dq_mean = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=3)
Delat_Dq_std = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=4)
 
#Dq_ana = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
#                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
#                        "500000.txt"%qd, usecols=5)
 
#Dq_clus = np.genfromtxt("./data/Theory_CqDq_Lbox_1024Mpc_NpartSim_512"\
#                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
#                        "500000.txt"%qd, usecols=7)
 
D=3

fig= plt.figure(1, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
#NUM_COLORS = len(snapshots)
#cm = plt.get_cmap('hsv')
#ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
mark = ['o', 'd', '^', '<', '+']

ax1.errorbar(rmid, Delat_Dq_mean, yerr=Delat_Dq_std, fmt='o', ecolor='g', 
             elinewidth=1.5, capsize=5, color='g')#, label=r"$\rm \Delta D_{q=2}$")

#ax1.set_title(r"$%d\rm{Mpc} h^{-1},\rm{N}_{part}:512^{3}, REAL$"%1024 , fontsize=16)
ax1.set_xlabel(r'$\rm{r}$ [$\rm{Mpc}$ $\rm{h}^{-1}$]', fontsize=18)
ax1.set_ylabel(r"$\rm \Delta D_{q=2}$ $\rm (r)$", fontsize=16)
#ax1.set_ylim(2.1, 3.2) # q=2
#ax1.set_ylim(0.5, 3.2)
ax1.set_xlim(10, 160.)
ax1.set_xscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=18, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.5)
ax1.spines['left'].set_linewidth(1.5)
ax1.spines['top'].set_linewidth(1.5)
ax1.spines['right'].set_linewidth(1.5)
plt.tight_layout()
fout = "DeltaDq2ofr_qd_2.png"
fig.savefig(fout, bbox_inches='tight', dpi=300)


fig= plt.figure(2, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
ax1.errorbar(rmid, Dq_mean, yerr=Dq_std, fmt='o', ecolor='b', 
             elinewidth=1.5, capsize=5, color='b')#, alpha=0.7, label=r"$\rm N_{Mc}=2 lac$")

plt.plot(rmid, D-(Dq_ana+2*Dq_clus), marker='o',mew=1.8, mfc="none", ms=5,linestyle='',
         mec='r', label=r'$D-(\Delta D_{q})_{\bar{N}} -(\Delta D_{q})_{clus}$')
         
ax1.axhline(y=3, linestyle = '--', linewidth=1.8, color='g')           

#ax1.text(50, 2.1, r'$q=2$', fontsize=20)
#ax1.set_title(r"$%d\rm{Mpc} h^{-1},\rm{N}_{part}:512^{3}, REAL$"%1024 , fontsize=16)
ax1.set_xlabel(r'$\rm{r}$ [$\rm{Mpc}$ $\rm{h}^{-1}$]', fontsize=18)
ax1.set_ylabel(r'$\rm D_{q=2}(r)$', fontsize=18)
#ax1.set_ylim(2.3, 3.2) # q=2
#ax1.set_ylim(0.5, 3.2)
ax1.set_xlim(10, 160.)
ax1.set_xscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=18, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.5)
ax1.spines['left'].set_linewidth(1.5)
ax1.spines['top'].set_linewidth(1.5)
ax1.spines['right'].set_linewidth(1.5)
plt.tight_layout()
fout = "Danal_Dq2ofr_qd_2.png"
fig.savefig(fout, bbox_inches='tight', dpi=300)



UDq_mean    = np.genfromtxt("./data/Theory_Uniform_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=1)
UDq_std    = np.genfromtxt("./data/Theory_Uniform_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=2)
 



Dq_mean_bf    = np.genfromtxt("./data/Theory_bf_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=2)
Dq_std_bf    = np.genfromtxt("./data/Theory_bf_CqDq_Lbox_1024Mpc_NpartSim_512"\
                        "_SnapId_16_SampSize_1200000_qd_%d_NMc_"\
                        "500000.txt"%qd, usecols=3)
 


fig= plt.figure(3, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.errorbar(rmid, Dq_mean, yerr=Dq_std, fmt='o', ecolor='b', 
             elinewidth=1.5, capsize=5, color='b', alpha=0.7, label=r"$\rm Simulation$")

#ax1.errorbar(rmid, UDq_mean, yerr=UDq_std, fmt='o', ecolor='r', 
#             elinewidth=1.5, capsize=5, color='r', alpha=0.7, label=r"$\rm Uni$ $\rm Dist $")

#ax1.errorbar(rmid, Dq_mean_bf, yerr=Dq_std_bf, fmt='s', ecolor='m', mfc='none',mew=1.5,
#             elinewidth=1.5, capsize=5, color='m', alpha=0.7, label=r"$\rm Brute$ $\rm force $")

#plt.plot(rmid, D-Dq_clus, marker='d',mew=1.8, mfc="none", ms=5,linestyle='',
#         mec='c', label=r'$D-(\Delta D_{q})_{\bar{N}} -(\Delta D_{q})_{clus}$')

 

         
ax1.axhline(y=3, linestyle = '--', linewidth=1.8, color='g')           

#ax1.text(50, 2.1, r'$q=2$', fontsize=20)
#ax1.set_title(r"$%d\rm{Mpc} h^{-1},\rm{N}_{part}:512^{3}, REAL$"%1024 , fontsize=16)
ax1.set_xlabel(r'$\rm{r}$ [$\rm{Mpc}$ $\rm{h}^{-1}$]', fontsize=18)
ax1.set_ylabel(r'$\rm D_{q=2}(r)$', fontsize=18)
#ax1.set_ylim(2.3, 3.2) # q=2
#ax1.set_ylim(1.6, 3.2)
ax1.set_xlim(10, 160.)
ax1.set_xscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=18, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.5)
ax1.spines['left'].set_linewidth(1.5)
ax1.spines['top'].set_linewidth(1.5)
ax1.spines['right'].set_linewidth(1.5)
plt.tight_layout()
fout = "DUni_Dq2ofr_qd_2.png"
fig.savefig(fout, bbox_inches='tight', dpi=300)











plt.show()

