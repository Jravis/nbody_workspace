#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
#################################################################


import numpy as np
import sys,os,string,time,re,struct
import argparse
import matplotlib.pyplot as plt



def main():
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn'\
         'from parent sample e.g 50 or 40')
    
    parser.add_argument("qd", type=int, 
    help='fractal dimension default q=2')


    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox      = args.Lbox
    NpartSim  = args.NpartSim
    snapID    = args.snapID
    samp_size = args.samp_size
    nsamples  = args.nsamples
    qd        = args.qd

    
    dirc_name = ("/mnt/data1/astro_student/Corr_analysis/"\
            	"sub_sample_data_%dMpc_%d/%d_sample/REAL/"\
		"snapshot_%d_data/Fractal_results/RandCen"\
		"_test/"%(Lbox, NpartSim, samp_size, snapID))
    NMc  = np.array([2000, 5000, 10000, 15000], dtype=np.int32) 
    Nbr  = 100
    rmin = 10.0
    rmax = 150.0


    rbin = np.logspace(np.log10(rmin), np.log10(rmax), num=Nbr+1, endpoint=True) 
    avpart = np.zeros(Nbr, dtype = np.float64)

    D_th = np.zeros(Nbr, dtype = np.float64)
    numden = np.float64(samp_size)/np.float64(Lbox**3)
    print numden
    for i in xrange(Nbr):
        vol = (4.* np.pi/3.0) * ((rbin[i+1])**3.0 - (rbin[i])**3.0)
    	avpart[i]  = np.float64(samp_size) * vol * numden
    D=3
    temp = (qd-2.)/(2.*avpart)
    plt.plot(rbin[:-1], D*(1.-temp), 'b-')
    plt.xlim(10, 150)
    plt.xscale("log")
    plt.show()


#-----------------------------------------------------------------




    for nmc in NMc: 
        infile= (dirc_name+"logbin_CorrInt_SnapID_%d_SampSize"\
	    	 "%d_Rmax_%0.1fMpc_Nbr_%d_NMc_%d_%d.dat"%( snapID, 
		 samp_size, rmax, Nbr, nmc, 0))
     
    	ni_r = np.genfromtxt(infile)
    	ni_r = ni_r.reshape(np.int32(nmc), np.int32(Nbr))
    	Cqd = np.zeros(np.int32(Nbr), dtype= np.float64) 
    
        for i in xrange(np.int32(Nbr)):
            Cqd[i] = np.sum(ni_r[:, i]**(qd-1))

    	Cqd = Cqd/np.float64(samp_size)/np.float64(nmc)
    	Dqd =  np.diff(np.log10(Cqd))
    	Dqd = Dqd/np.diff(np.log10(rbin))
    	Dqd = Dqd/np.float64(qd-1)
   
    	rbin_mid = [(rbin[j]+rbin[j+1])*0.5 for j in xrange(len(rbin)-1)]
    	fout = (dirc_name+"CqDq_Lbox_%dMpc_NpartSim_%d_SnapId_"\
	        "%d_SampSize_%d_qd_%d_NMc_%d.txt"%(Lbox, 
	        NpartSim, snapID, samp_size, qd, nmc))

        np.savetxt(fout, zip(rbin, rbin_mid, Cqd, Dqd), 
		   fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f", 
                   header='rbin\trbin_mid\tCq\tDq', delimiter='\t')



if __name__ == "__main__":
    main()



