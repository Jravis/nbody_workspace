#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# Fast grid based correlation fucntion routine for DM subsample #
# it use Corrfunc package by Manodeep Sinha.                    #
#################################################################


from __future__ import print_function
import numpy as np
import sys,os,string,time,re,struct

import argparse
import Corrfunc
from Corrfunc.theory.vpf import vpf
from astropy.cosmology import LambdaCDM
import pandas as pd 
sys.path.insert(1, '../Gadget2_helper_routines/')
import readsnap



def MDPL2_Count_in_Cell(dir_name, samp_size, n_samples, Lbox, numden, 
                          NpartSim, snapID, nbins, nthreads, seed, counter):
    """
    Parameters
    ----------

    Returns
    ---------
    """


    #rmin = 2*(np.float64(Lbox)/np.float64(NpartSim)/25.)
    rmin = 10.0    
    print("Lbox: ", Lbox, "Npart_Sim: ", NpartSim)
    rmax = 10
    rmax = 20
    print("rmin: %0.4f, rmax:%0.4f"%(rmin, rmax))
    
    rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
    NMc = 5000
    autocorr = 0


    ravg    = np.zeros(nbins, dtype=np.float64)
    r_min   = np.zeros(nbins, dtype=np.float64)
    r_max   = np.zeros(nbins, dtype=np.float64)
    
    # Input subsample file name
    infile = dir_name+"/KMDPL2_SubSam_%d_%d_%d_comov.bin"%(snapID, samp_size, counter)    
    print(infile)

    f = open(infile,'rb')    # rb is for read binary
    blockini = np.fromfile(f, dtype=np.float64, count=1)
    data_pos = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    data_vel = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    blockfin = np.fromfile(f, dtype=np.float64, count=1)
    f.close()
    if blockini != blockfin:
        raise ValueError("Block doesn't match")
    del data_vel

    data_pos = data_pos.reshape(samp_size, 3)
    X = data_pos[:, 0]
    Y = data_pos[:, 1]
    Z = data_pos[:, 2]

    numN = 10 #len(X)

    results = vpf(rmax, nbins, NMc, numN, seed, X, Y, Z, boxsize=Lbox)
#    Pnofr = np.zeros((nbins, numN), dtype=np.float64)

    for r in results:
    	print("{0:10.1f} ".format(r[0]), end="")
     # doctest: +NORMALIZE_WHITESPACE
     	for pn in r[1]:
        	print("{0:10.3f} ".format(pn), end="")
         	# doctest: +NORMALIZE_WHITESPACE
     	print("") # doctest: +NORMALIZE_WHITESPACE

    """
    ix= 0
    iy= 0
    for r in results:
        for pn in r[1]:
	    Pnofr[ix, iy] = pn
	    iy+=1
	ix+=1
     
    Cqd = np.zeros(nbins)
    nn  = np.arange(NumN)
    for i in xrange(nbins):
	Cqd[i] = np.sum( (nn**(qd-1.)) * Pnofr[i, :])


    Dqd =  np.diff(np.log10(Cqd))
    Dqd = Dqd/np.diff(np.log10(rbins[:-1]))
    """
 
    del data_pos
    del X, Y, Z





def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn from parent sample e.g 50 or 40')

    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    snapID     = args.snapID
    samp_size  = args.samp_size
    nsamples   = args.nsamples

    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)


    inp_file = dir_name +"snapshot_%03d.0"%snapID
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    
    L_boxhalf = Lbox/2.
    numden = np.float64(samp_size)/np.float64(Lbox**3)
   
    print("Lbox: ", Lbox, "Npart_Sim: ", NpartSim, "numden:", numden)

    #dir_name = ("/mnt/data1/astro_student/Corr_analysis"\
    dir_name = ("/mnt/data1/astro_student/test/corr"\
                "/sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data"%(Lbox, NpartSim, samp_size, snapID))
    print(dir_name)

    print("Enter number of rbins\n")
    nbins   = np.int32(raw_input(""))
    print("Enter number of thread you want to use\n")
    nthreads= np.int32(raw_input(""))
 
    np.random.seed(77777)
    for ii in xrange(nsamples):    
       MDPL2_Count_in_Cell(dir_name, samp_size, nsamples, Lbox, 
                           numden, NpartSim, snapID, nbins, nthreads, 77777, ii)
       

if __name__ == "__main__":
    main()



