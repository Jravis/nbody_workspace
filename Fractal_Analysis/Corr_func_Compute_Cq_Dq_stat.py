#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
#################################################################


import numpy as np
import sys,os,string,time,re,struct
import argparse



def main():
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn'\
         'from parent sample e.g 50 or 40')
    
    parser.add_argument("qd", type=int, 
    help='fractal dimension default q=2')


    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox      = args.Lbox
    NpartSim  = args.NpartSim
    snapID    = args.snapID
    samp_size = args.samp_size
    nsamples  = args.nsamples
    qd        = args.qd

    #dirc_name = ("/mnt/data1/astro_student/Corr_analysis/"\
    dirc_name = ("/mnt/data1/astro_student/test/corr/"\
            	"sub_sample_data_%dMpc_%d/%d_sample/REAL/"\
		"snapshot_%d_data/corr_func_fractal/"
		%(Lbox, NpartSim, samp_size, snapID))

    NMc  = 500000
    Nbr  = 20 
    rmin = 10.0
    rmax = Lbox/5.# 150.0

    infile = (dirc_name+"logbin_CorrInt_Lbox_%d_"\
	       "NparSim_%d_snapID_%d_SampSize_%d_Rmax_%0.1fMpc_Nbr_%d"\
	       "_NMc_%d_%d.dat"%(Lbox, NpartSim, snapID, samp_size, rmax, 
	       Nbr, NMc, 0))


    avpart   = np.genfromtxt(infile, usecols=3) 
    rbin_mid = np.genfromtxt(infile, usecols=2) 


    D=3
    """
    Dq_ana = D*(qd-2.)/(2.*avpart) # no weak clustering contribution
    
    Dq_clust = np.zeros((nsamples, np.int32(Nbr)), dtype=np.float64)
    Corr_dirc = "/mnt/data1/astro_student/Corr_analysis/sub_sample_"\
		"data_1024Mpc_512/1200000_sample/REAL/snapshot_16_data"\
		"/corr_func_results/"

    for nn in xrange(nsamples):
	infile = (Corr_dirc+"KMDPL2_Xi_1024Mpc512_SnapID_16"\
		  "_SampSize_1200000_%d.txt"%nn)
	print infile
    	xi    = np.genfromtxt(infile, usecols=6)
    	xibar = np.genfromtxt(infile, usecols=7)
    	
	D*qd*0.5*(xibar-xi)
    	Dq_clust[nn,:] = D*qd*0.5*(xibar-xi)

    Dq_clust_mean = np.mean(Dq_clust, axis=0)
    Dq_clust_std  = np.std(Dq_clust, axis=0)

    print Dq_clust_mean 
    """
    print"-----------------------------------------------------\n"
#-----------------------------------------------------------------

    Dqd = np.zeros((nsamples, np.int32(Nbr)-1), dtype=np.float64)

    for nn in xrange(nsamples):

	infile = (dirc_name+"logbin_CorrInt_Lbox_%d_"\
	       "NparSim_%d_snapID_%d_SampSize_%d_Rmax_%0.1fMpc_Nbr_%d"\
	       "_NMc_%d_%d.dat"%(Lbox, NpartSim, snapID, samp_size, rmax, 
	       Nbr, NMc, nn))

    	Dqd[nn, :] = np.genfromtxt(infile, usecols=6)
   

    Dqd_mean = np.mean(Dqd, axis=0)
    Dqd_std  = np.std(Dqd, axis=0)


    Dqd1_mean = np.mean(D-Dqd, axis=0)
    Dqd1_std  = np.std(D-Dqd, axis=0)


    fout = (dirc_name+"Theory_CqDq_Lbox_%dMpc_NpartSim_%d_SnapId"\
	    "_%d_SampSize_%d_qd_%d_NMc_%d_Nbr_%d.txt"%(Lbox, NpartSim, snapID, 
	    samp_size, qd, NMc, Nbr))

    np.savetxt(fout, zip(rbin_mid, Dqd_mean, Dqd_std, Dqd1_mean, Dqd1_std),
	       fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
               header='rbin_mid\tDq_mean\tDq_std\t(D-Dqd)_mean\t(D-Dqd)_std', 
	       delimiter='\t')




    #np.savetxt(fout, zip(rbin_mid, Dqd_mean, Dqd_std, Dqd1_mean, Dqd1_std, Dq_ana, avpart, Dq_clust_mean, Dq_clust_std),
#	       fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
#               header='rbin_mid\tDq_mean\tDq_std\t(D-Dqd)_mean\t(D-Dqd)_std\tDq_ana\tavpart\tDq_clust_mean\tDq_clust_std', 
#	       delimiter='\t')


if __name__ == "__main__":
    main()



