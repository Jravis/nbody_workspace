#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
#################################################################


import numpy as np
import sys,os,string,time,re,struct
import argparse



def main():
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn'\
         'from parent sample e.g 50 or 40')
    
    parser.add_argument("qd", type=int, 
    help='fractal dimension default q=2')


    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox      = args.Lbox
    NpartSim  = args.NpartSim
    snapID    = args.snapID
    samp_size = args.samp_size
    nsamples  = args.nsamples
    qd        = args.qd

    dirc_name = ("/mnt/data1/astro_student/Corr_analysis/"\
            	"sub_sample_data_%dMpc_%d/%d_sample/REAL/"\
		"snapshot_%d_data/Fractal_results/"
		%(Lbox, NpartSim, samp_size, snapID))

    NMc  = 20000 
    Nbr  = 50
    rmin = 10.0
    rmax = 150.0

    rbin = np.logspace(np.log10(rmin), np.log10(rmax), num=Nbr, endpoint=True) 
    rbin_mid = [(rbin[j]+rbin[j+1])*0.5 for j in xrange(len(rbin)-1)]

    """ 
    avpart = np.zeros(Nbr, dtype = np.float64)
    D_th = np.zeros(Nbr, dtype = np.float64)
    numden = np.float64(samp_size)/np.float64(Lbox**3)
    print numden
    for i in xrange(Nbr-1):
        vol = (4.* np.pi/3.0) * ((rbin[i+1])**3.0 - (rbin[i])**3.0)
    	avpart[i]  = np.float64(samp_size) * vol * numden
    D=3
    Dq_ana = D*(1. -  (qd-2.)/(2.*avpart)) # no weak clustering contribution
    """
#-----------------------------------------------------------------

    Cqd = np.zeros((nsamples, np.int32(Nbr)), dtype=np.float64)
    Dqd = np.zeros((nsamples, np.int32(Nbr)-1), dtype=np.float64)

    for nn in xrange(nsamples):
	infile = (dirc_name+"logbin_CorrInt_SnapID_%d_SampSize"\
	    	 "%d_Rmax_%0.1fMpc_Nbr_%d_NMc_%d_%d.dat"%(snapID, 
		  samp_size, rmax, Nbr, NMc, nn))
        print infile
    	ni_r  = np.genfromtxt(infile)
    	ni_r  = ni_r.reshape(np.int32(NMc), np.int32(Nbr))
    
    	for i in xrange(np.int32(Nbr)):
            Cqd[nn, i] = np.sum(ni_r[:, i])
	    Cqd[nn, i] = Cqd[nn, i]/np.float64(samp_size)/np.float64(NMc)
    
    	Dqd[nn, :] =  np.diff(np.log10(Cqd[nn, :]))
    	Dqd[nn, :] = Dqd[nn, :]/np.diff(np.log10(rbin))
    	Dqd[nn, :] = Dqd[nn, :]/np.float64(qd-1)
   
    
    Cqd_mean = np.mean(Cqd, axis=0)
    Cqd_std  = np.std(Cqd, axis=0)
    print Cqd_std


    Dqd_mean = np.mean(Dqd, axis=0)
    Dqd_std  = np.std(Dqd, axis=0)

    fout = (dirc_name+"CqDq_Lbox_%dMpc_NpartSim_%d_SnapId"\
	    "_%d_SampSize_%d_qd_%d.txt"%(Lbox, NpartSim, snapID, 
	    samp_size, qd))

    np.savetxt(fout, zip(rbin, rbin_mid, Cqd_mean, Cqd_std, Dqd_mean, Dqd_std), 
	       fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
               header='rbin\trbin_mid\tCq_mean\tCq_std\tDq_mean\tDq_std', 
	       delimiter='\t')


#    np.savetxt(fout, zip(rbin, rbin_mid, Cqd, Dqd, Dq_ana, (Dqd-Dq_ana)), 
#	       fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
#               header='rbin\trbin_mid\tCq\tDq\tDq_ana\t\delta_Dq', 
#	       delimiter='\t')



if __name__ == "__main__":
    main()



