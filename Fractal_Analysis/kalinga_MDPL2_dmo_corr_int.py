

#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# Fast grid based correlation fucntion routine for DM subsample #
# it use Corrfunc package by Manodeep Sinha.                    #
#################################################################


from __future__ import print_function
import numpy as np
import sys,os,string,time,re,struct

import argparse
import Corrfunc
from Corrfunc.theory.DD import DD
from astropy.cosmology import LambdaCDM
import pandas as pd 
from tqdm import tqdm
import matplotlib.pyplot as plt
sys.path.insert(1, '../Gadget2_helper_routines/')
import readsnap




def MDPL2_Uniform_corrint(dir_name, samp_size, n_samples, Lbox, numden, 
                          NpartSim, snapID, nbins, nthreads, counter):
    """
    Parameters
    ----------

    Returns
    ---------
    """


    #rmin = 2*(np.float64(Lbox)/np.float64(NpartSim)/25.)
    rmin = 10.0    
    print("Lbox: ", Lbox, "Npart_Sim: ", NpartSim)
    #rmax = Lbox/5.0
    rmax = 150.0
    print("rmin: %0.4f, rmax:%0.4f"%(rmin, rmax))
    
    rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
    NMc = 500000
    autocorr = 0


    ravg    = np.zeros(nbins, dtype=np.float64)
    r_min   = np.zeros(nbins, dtype=np.float64)
    r_max   = np.zeros(nbins, dtype=np.float64)
    npairs  = np.zeros(nbins, dtype=np.float64)
    Avpart  = np.zeros(nbins, dtype=np.float64)

    
    X1 =  np.float64(np.random.uniform(0, Lbox, samp_size))#, dtype=np.float64)
    Y1 =  np.float64(np.random.uniform(0, Lbox, samp_size))#, dtype=np.float64)
    Z1 =  np.float64(np.random.uniform(0, Lbox, samp_size))#, dtype=np.float64)
    weights = np.ones_like(X1)  
 
    index = np.random.randint(0, samp_size, size=NMc)
    X2 = np.float64(X1[index])
    Y2 = np.float64(Y1[index])
    Z2 = np.float64(Z1[index])

    results = DD(autocorr, nthreads, rbins, X1, Y1, Z1,
	         X2=X2, Y2=Y2, Z2=Z2, verbose=True,
		 boxsize=np.float64(Lbox), output_ravg=True)

    count=0
    for r in results: 
        vol    = (4.0*np.pi/3.0) * ( r['rmax']**3.0 - r['rmin']**3.0 )
        avpart = np.float64(samp_size) * vol * numden;
        npairs[count]  = r['npairs']
        ravg[count]    = r['ravg']
        r_min[count]   = r['rmin']
        r_max[count]   = r['rmax']
        Avpart[count]  = avpart
        count+=1


    Cqd = npairs/np.float64(samp_size)/np.float64(NMc)
    Dqd =  np.diff(np.log10(Cqd))
    Dqd = Dqd/np.diff(np.log10(rbins[:-1]))


    outfile = (dir_name+ "/corr_func_fractal/logbin_Uniform_CorrInt_Lbox_%d_"\
	       "NparSim_%d_snapID_%d_SampSize_%d_Rmax_%0.1fMpc_Nbr_%d"\
	       "_NMc_%d_%d.dat"%(Lbox, NpartSim, snapID, samp_size, rmax, 
	       nbins, NMc, counter))

    np.savetxt(outfile, zip(r_min, r_max, ravg, Avpart, npairs, Cqd, Dqd), 
               fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
	       header='r_min\tr_max\travg\tAvpart\tnpairs\tCqd2\tDqd2',delimiter='\t')

    del X1, Y1, Z1, X2, Y2, Z2


#-------------------------------------------------------------------------------------------


def MDPL2_dmo_correlation(dir_name, samp_size, n_samples, Lbox, numden, 
                          NpartSim, snapID, nbins, nthreads, counter):
    """
    Parameters
    ----------

    Returns
    ---------
    """


    #rmin = 2*(np.float64(Lbox)/np.float64(NpartSim)/25.)
    rmin = 10.0    
    print("Lbox: ", Lbox, "Npart_Sim: ", NpartSim)
    rmax = Lbox/5.0
    #rmax = 150.0
    print("rmin: %0.4f, rmax:%0.4f"%(rmin, rmax))
    
    rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
    NMc = 500000
    autocorr = 0


    ravg    = np.zeros(nbins, dtype=np.float64)
    r_min   = np.zeros(nbins, dtype=np.float64)
    r_max   = np.zeros(nbins, dtype=np.float64)
    npairs  = np.zeros(nbins, dtype=np.float64)
    Avpart  = np.zeros(nbins, dtype=np.float64)
    
    # Input subsample file name
    infile = dir_name+"/KMDPL2_SubSam_%d_%d_%d_comov.bin"%(snapID, samp_size, counter)    
    print(infile)

    f = open(infile,'rb')    # rb is for read binary
    blockini = np.fromfile(f, dtype=np.float64, count=1)
    data_pos = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    data_vel = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    blockfin = np.fromfile(f, dtype=np.float64, count=1)
    f.close()
    if blockini != blockfin:
        raise ValueError("Block doesn't match")
    del data_vel

    data_pos = data_pos.reshape(samp_size, 3)
    X1 = data_pos[:, 0]
    Y1 = data_pos[:, 1]
    Z1 = data_pos[:, 2]
    weights = np.ones_like(X1)   
    index = np.random.randint(0, samp_size, size=NMc)
    X2 = np.float64(data_pos[index, 0])
    Y2 = np.float64(data_pos[index, 1])
    Z2 = np.float64(data_pos[index, 2])

    results = DD(autocorr, nthreads, rbins, X1, Y1, Z1,
	         X2=X2, Y2=Y2, Z2=Z2, verbose=True,
		 boxsize=np.float64(Lbox), output_ravg=True)

    count=0
    for r in results: 
        vol = (4.0*np.pi/3.0) * ( r['rmax']**3.0 - r['rmin']**3.0 )
        avpart= np.float64(samp_size) * vol * numden;
        npairs[count]  = r['npairs']
        ravg[count]    = r['ravg']
        r_min[count]   = r['rmin']
        r_max[count]   = r['rmax']
        Avpart[count]  = avpart
        count+=1


    Cqd = npairs/np.float64(samp_size)/np.float64(NMc)
    Dqd =  np.diff(np.log10(Cqd))
    Dqd = Dqd/np.diff(np.log10(rbins[:-1]))


    outfile = (dir_name+ "/corr_func_fractal/logbin_CorrInt_Lbox_%d_"\
	       "NparSim_%d_snapID_%d_SampSize_%d_Rmax_%0.1fMpc_Nbr_%d"\
	       "_NMc_%d_%d.dat"%(Lbox, NpartSim, snapID, samp_size, rmax, 
	       nbins, NMc, counter))

    np.savetxt(outfile, zip(r_min, r_max, ravg, Avpart, npairs, Cqd, Dqd), 
               fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f", 
	       header='r_min\tr_max\travg\tAvpart\tnpairs\tCqd2\tDqd2',delimiter='\t')

    del data_pos
    del X1, Y1, Z1, X2, Y2, Z2

#--------------------------------------------------------------------------

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn from parent sample e.g 50 or 40')

    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    snapID     = args.snapID
    samp_size  = args.samp_size
    nsamples   = args.nsamples

    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)


    inp_file = dir_name +"snapshot_%03d.0"%snapID
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    
    L_boxhalf = Lbox/2.
    numden = np.float64(samp_size)/np.float64(Lbox**3)
   
    print("Lbox: ", Lbox, "Npart_Sim: ", NpartSim, "numden:", numden)

    #dir_name = ("/mnt/data1/astro_student/Corr_analysis"\
    dir_name = ("/mnt/data1/astro_student/test/corr"\
                "/sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data"%(Lbox, NpartSim, samp_size, snapID))
    print(dir_name)

    print("Enter number of rbins\n")
    nbins   = np.int32(raw_input(""))
    print("Enter number of thread you want to use\n")
    nthreads= np.int32(raw_input(""))
 
    np.random.seed(77777)
    for ii in xrange(nsamples):    
       MDPL2_dmo_correlation(dir_name, samp_size, nsamples, Lbox, 
                             numden, NpartSim, snapID, nbins, nthreads, ii)
       
#       MDPL2_Uniform_corrint(dir_name, samp_size, nsamples, Lbox, 
#                             numden, NpartSim, snapID, nbins, nthreads, ii)
       




if __name__ == "__main__":
    main()










