
///////////////////////////////////////////////////////////////////////
// Fractal Dimension code                                            //                 
// Copyright 2020 Sandeep Rana                 			     //
//                                                                   //
// This is an open-mpi enabled brute force correlation Integral code,// 
// which is equation 5 from Bagla, Yadav & Sheshdri.                 //             
// Here we take random positions in a simulation box and compute     //
// number of pair within the a distance r from the randomly chosen   //
// point.						             //
///////////////////////////////////////////////////////////////////////


#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<math.h>
#include<string>
#include <algorithm> 
#include <bits/stdc++.h> 
#include <sstream> // for ostringstream
using namespace std;

char *filename;
double *pos;
int nsub, sub_samp_size;
int ndim=3, ndm;
long int *pairs_r ;   // pairs as a function of r
long int *Corr_Int ;   

int Nbr; // Radial correlation bins angle avg bin
int NMc; // Radial correlation bins angle avg bin

//-------------------------------------------------------------------------------

void init_pair_arrays();
void cleanup();
                  
void  corr_2D_box_bf(int np, int nbin, int m_ind,
                     double *pos, long int *p_r, 
		     double rmin, double rmax, double l_box, 
                     double l_box_half, int bin_key);

//-------------------------------------------------------------------------------

int main(int argc, char **argv){

  int snapid, i, k, j;
  double nblock_start;//, nblock_end;
  double numden;
  double  rmin, rmax;
  double Lbox, Lbox_half;
  double scale_factor, HubbleParam;
  int bin_type_r;
  double npart_sim, softening_length;
  

  if(argc != 8){
    cout<< "\n./a.out   [snapid] [Nbr] [sub_samp_size] [npartsim] [boxsize] [NMc]     				[bin_type_r;]   "<<"\n";
    cout<< "\n./a.out     17     50     800000           512       1024     No random center e.g 1000         1 for logbin 0 linear"<<"\n";
    return(-1);
  }//if


//-------------------------------------------------------------------------------------------------
// Setting up initial parameters

  snapid      = atoi(argv[1]);
  Nbr         = atoi(argv[2]);
  npart_sim   = (double) atof(argv[4]); 
  NMc	      = atoi(argv[6]);
  bin_type_r  = atoi(argv[7]); 
  filename  = (char *) malloc(500*sizeof(char));


  sprintf(filename,"/mnt/data1/astro_student/Corr_analysis/"
           "sub_sample_data_%sMpc_%s/%s_sample/REAL/snapshot"
	   "_%s_data/subsamp_param_%s.dat",argv[5], argv[4], 
	   argv[3], argv[1], argv[1]);

  cout << filename << "\n";
  FILE  *param_file=NULL;
  param_file = fopen(filename, "r");
  
  fscanf(param_file,"%d %d %d %d %d %lf %lf %lf %lf\n", 
            &ndim, &sub_samp_size, &nsub, &ndm, &snapid, &scale_factor, 
            &HubbleParam, &Lbox, &Lbox_half);
  fclose(param_file);
 
  cout<<"\n"; 
  cout<<"-------------------------------------"<<"\n"; 
  cout<<"sub_samp_size: "<<sub_samp_size<<"\n" ;
  cout<<"snapid: "<<snapid<<"\n" ;
  cout<<"ndm: "<<ndm<<"\n" ;
  cout<<"Lbox: "<<Lbox<<"\n"; 
  cout<<"-------------------------------------"<<"\n"; 
  

  softening_length  = (Lbox*1000.)/npart_sim;
  softening_length *= (1./25.);
  
  cout<<"softening length in kpc h^-1: " << softening_length << "\n";
  
  if(bin_type_r==1){
    cout<<"You have chosen log binning therefore you need to choose rmin as"
    "multiple of softening_length e.g 4*softening length or 2*softening length"<<"\n";
    cout<<"Enter the multiple for rmin"<<"\n";
    cin>> rmin;
  //  rmin *= (softening_length/1000.);

  }
  else{
    rmin = 0.;
  }
  
  cout<<"Enter the rmax"<<"\n";
  cin>> rmax;
  
  numden=(double)(sub_samp_size)/(double)(Lbox*Lbox*Lbox);
  
  printf("numden = %2.10f, nsub = %d ndim=%d "
         "Rmin=%0.3f Rmax=%0.3f \n",numden, nsub, 
         ndim, rmin, rmax);
 
  cout<<"-------------------------------------"<<"\n"; 
 

//  Setting mersine twister Psuedo Random number gen

  const int range_from = 0;
  const int range_to = sub_samp_size;
  seed_seq                            seed{2593697};
  mt19937_64                          gen(seed);
  uniform_real_distribution<double>  dist(range_from, range_to);
 
  //cout<<"Enter the Number of random M centers you want"<<"\n";
  //cin>>NMc;
  
  sprintf(filename,"/mnt/data1/astro_student/test/corr/"
	    "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_"
	    "%d_data/Fractal_results/Fractal_param"\
	    "_used_%d.txt",(int)Lbox, (int) npart_sim,(int) sub_samp_size, 
	    (int) snapid, NMc);

  FILE  *param_file1=NULL;
  param_file1 = fopen(filename, "w");
  
  fprintf(param_file1,"#NMc  Nbr  rmin  rmax\n"); 
  fprintf(param_file1,"%d %d %lf %lf\n", 
            NMc, Nbr, rmin, rmax);
  fclose(param_file1);

//------------------------------------------------------------------------------------------------------
//Setting bins and computing bin volume also

  pairs_r  = (long int*) malloc(Nbr*sizeof(long int));
  Corr_Int = (long int*) malloc(NMc*Nbr*sizeof(long int));

//------------------------------------------------------------------------------------------------------
// reading data

  FILE *snapfile = NULL;
  pos = (double *)malloc(sub_samp_size*ndim*sizeof(double));
  //nsub = 1;
  for (k = 0; k<nsub;k++){    

    memset(pos, 0, sub_samp_size*ndim*sizeof(double)); 
    
    sprintf(filename,"/mnt/data1/astro_student/test/corr/"
	    "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_"
	    "%d_data/KMDPL2_SubSam_%d_%d_%d_comov.bin",(int)Lbox,
	    (int) npart_sim,(int) sub_samp_size, (int) snapid, 
	    (int) snapid,(int) sub_samp_size,(int) k);

    cout<<filename<<"\n"; 

    snapfile = fopen(filename, "r");
    fread(&nblock_start,sizeof(double),1,snapfile);
    fread(pos,sizeof(double), sub_samp_size*ndim, snapfile);
    fclose(snapfile);
    
// reading data done
//------------------------------------------------------------------------------------------------------
//  Computing Cq2
    
     memset(Corr_Int, 0, Nbr*NMc*sizeof(long int)); 
    
    for(i=0;i<NMc;i++) {
      j=(int)dist(gen);
      init_pair_arrays();
      corr_2D_box_bf(sub_samp_size, Nbr, j, pos, 
		     pairs_r, rmin, rmax, Lbox, 
		     Lbox_half, bin_type_r);
      for(j=0; j<Nbr; j++)
        Corr_Int[i+NMc*j] += pairs_r[j];
    }
    if(bin_type_r==1) {
      sprintf(filename, "/mnt/data1/astro_student/test/corr/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_"
	    "%d_data/Fractal_results/logbin_CorrInt_SnapID_%d_SampSize"
	    "%d_Rmax_%0.1fMpc_Nbr_%d_NMc_%d_%d.dat",(int)Lbox, 
	    (int)npart_sim,(int) sub_samp_size, (int) snapid,(int) 
 	    snapid,(int) sub_samp_size, rmax, (int)Nbr, (int) NMc, 
	    (int)k);
    }
    else {
      sprintf(filename, "/mnt/data1/astro_student/test/corr/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_"
	    "%d_data/Fractal_results/linbin_CorrInt_SnapID_%d_SampSize"
	    "%d_Rmax_%0.1fMpc_Nbr_%d_NMc_%d_%d.dat",(int)Lbox, 
	    (int)npart_sim,(int) sub_samp_size, (int) snapid,(int) 
 	    snapid,(int) sub_samp_size, rmax, (int)Nbr, (int) NMc, 
	    (int)k);
    }

      
    cout<<filename<<"\n"; 
    FILE *outfile;
    outfile = fopen(filename, "w");
    fprintf(outfile,"#ni_r\n");

    for(i=0;i<NMc;i++) {
      for(j=0;j<Nbr;j++) {
        fprintf(outfile,"%2.14f\n", (double) Corr_Int[i+NMc*j]);
      }//for i
    }// for j

    fclose(outfile);
  }//for k 
  cleanup();
  return 0;
}//main

void init_pair_arrays(){
  int i;
  for(i=0; i<Nbr; i++)
    pairs_r[i] = 0;


}//init_pair_arrays()


void cleanup(){
  
  free(pos);
  free(pairs_r);
  free(Corr_Int);
  free(filename);
}

//------------------------------------------------------------------------

void  corr_2D_box_bf(int np, int nbin, int m_ind,
                     double *pos, long int *p_r, 
		     double rmin, double rmax, 
 		     double l_box, double l_box_half, 
		     int bin_key) {

 //////
 // Correlator for monopole in the periodic-box case
 // by brute-force
 double delta_r;
 if(bin_key==1){ 
   delta_r     = log10(rmax/rmin)/(double) nbin;
 }
 else{
   delta_r     = (rmax-rmin)/(double) nbin;
 }
 
#pragma omp parallel default(none)      \
  shared(np, nbin, m_ind,\
         pos, p_r,\
         rmin, rmax, delta_r, \
         l_box, l_box_half, bin_key)
  {
    int ii; 
    long int p_r_hthread[nbin]; //Histogram filled by each thread for log(r)-mu
    for(ii=0;ii<nbin;ii++)
        p_r_hthread[ii]=0.; //Clear private histogram
    int jj; 
    double *pos1=&(pos[3*m_ind]);

#pragma omp for nowait schedule(dynamic)
    for(jj=0; jj<np; jj++) {

      double xr[3];
      double r2;
      int ir;        

      xr[0]=(pos1[0]-pos[3*jj]);
      xr[1]=(pos1[1]-pos[3*jj+1]);
      xr[2]=(pos1[2]-pos[3*jj+2]);
 
      if(fabs(xr[0])>l_box_half)
        xr[0] = l_box-fabs(xr[0]);
       
      if(fabs(xr[1])>l_box_half)
        xr[1] = l_box-fabs(xr[1]);
      
      if(fabs(xr[2])>l_box_half)
        xr[2] = l_box-fabs(xr[2]);
        
      r2 = xr[0]*xr[0]+xr[1]*xr[1]+xr[2]*xr[2]; 

      if( (sqrt(r2)>=rmin) && (sqrt(r2)<=rmax) ) {
        if(bin_key==1) { // log binning
          ir =(int)(log10(sqrt(r2)/rmin)/delta_r); 
        }//if bin_key
    	else { // lin bin
          ir =(int)((sqrt(r2)-rmin)/delta_r); 
	}//bin_key
        p_r_hthread[ir]+=1;
      }//if r

    }//jj
    
#pragma omp critical
    {
      for(ii=0;ii<nbin;ii++) //Check bound
        p_r[ii]+= p_r_hthread[ii]; //Add private histograms to shared one
    } // end pragma omp critical
    
  } // end pragma omp parallel

}


