import argparse
import numpy as np
import sys
#sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
import readsnap


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 117, 110, 005')
    parser.add_argument("nfiles", type=int, 
    help='Number of subfiles written by Gadget e.g snapshot.{0-4}')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

#------------------------------------------------
    Lbox      = np.int32(args.Lbox)
    snapID    = np.int32(args.snapID)
    nfiles    = np.int32(args.nfiles)
    npart_sim = np.int32(args.npart_sim)
#------------------------------------------------

    dir_name= ("/mnt/data1/sandeep/Kalinga_run_Analysis/%dMpc_%d/"%(Lbox, npart_sim))
    
    if nfiles<=1:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d"%(snapID)
    else:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d.%d"%(snapID, 0)

    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()




if __name__ == "__main__":
    main()



