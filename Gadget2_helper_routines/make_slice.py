import numpy as np
import os
import argparse
from astropy.io import ascii
import sys
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
import readsnap


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 117, 110, 005')
    parser.add_argument("nfiles", type=int, 
    help='Number of subfiles written by Gadget e.g snapshot.{0-4}')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    parser.add_argument("Slice_width", type=int, 
    help='Slice width of the simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

#------------------------------------------------
    Lbox      = np.int32(args.Lbox)
    snapID    = np.int32(args.snapID)
    nfiles    = np.int32(args.nfiles)
    npart_sim = np.int32(args.npart_sim)
    Slice_width = np.float64(args.Slice_width)
    
#------------------------------------------------

    dir_name= ("/mnt/data1/sandeep/Kalinga_run_Analysis/%dMpc_%d/"%(Lbox, npart_sim))
    print dir_name 
    if nfiles<=1:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d"%(snapID)
    else:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d.%d"%(snapID, 0)

    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    scale_fac = 1./(1.+snapshot.redshift)
    unit_kpc_Mpc = np.float32(0.001)
    vel_conv_factor = np.float32(1./np.sqrt(scale_fac))

    posx = np.zeros(1, dtype=np.float64)
    posy = np.zeros(1, dtype=np.float64)
    posz = np.zeros(1, dtype=np.float64)
    
    velx = np.zeros(1, dtype=np.float64)
    vely = np.zeros(1, dtype=np.float64)
    velz = np.zeros(1, dtype=np.float64)

    for i in xrange(nfiles):
        
        if nfiles<=1:
            inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d"%(snapID)
        else:
            inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d.%d"%(snapID, i)

        print inp_file
        
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=False)
        Pos  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
        
        Pos[:,0] = np.float64(Pos[:,0]*unit_kpc_Mpc)
        Pos[:,1] = np.float64(Pos[:,1]*unit_kpc_Mpc)
        Pos[:,2] = np.float64(Pos[:,2]*unit_kpc_Mpc)
        
        posx = np.concatenate((posx, Pos[:,0]))
        posy = np.concatenate((posy, Pos[:,1]))
        posz = np.concatenate((posz, Pos[:,2]))


        Vel  = snapshot.read_Vel() # to convert the Gadget velocity to comoving velocity
        Vel[:,0] *= np.float64(vel_conv_factor)
        Vel[:,1] *= np.float64(vel_conv_factor)
        Vel[:,2] *= np.float64(vel_conv_factor)

        velx = np.concatenate((velx, Vel[:,0]))        
        vely = np.concatenate((vely, Vel[:,1]))        
        velz = np.concatenate((velz, Vel[:,2]))        
        del Pos, Vel 
    
    #taking slice of 10 h^-1 Mpc along the z direction
    index_z = (posz>=0.0)*(posz<=Slice_width)
    print len(posx[index_z])
    
    my_table = [(posx[index_z])[1:], (posy[index_z])[1:], (posz[index_z])[1:],
                (velx[index_z])[1:], (vely[index_z])[1:], (velz[index_z])[1:]
               ]

    my_names = ['x', 'y', 'z',
                'vx', 'vy', 'vz' 
               ]
    oformats = {'x':'%.6f', 'y':'%.6f', 'z':'%.6f',
                'vx':'%.6f', 'vy':'%.6f', 'vz':'%.6f'
               }
               
    print oformats 
    
    fname ="snapshot_slice_%dMpc_%03d.dat"%(Slice_width, snapID) 
    ascii.write(my_table, fname, names=my_names, delimiter='\t', 
                overwrite=True, formats=oformats, fast_writer=True)


if __name__ == "__main__":
    main()



