

#############################################################################
# Copyright (c) 2019, Sandeep Rana
# Module defining the growth factor `Component`.
# The primary class, :class:`GrowthFactor`, executes a full
# numerical calculation in standard flat LambdaCDM. Simplifications
# which may be more efficient, or extensions to alternate cosmologies,
# may be implemented.
# Formula used is given by Lahav et al 1991  10.1093/mnras/251.1.128
# 
#############################################################################


import numpy as np
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from astropy.cosmology import LambdaCDM
from astropy.io import ascii


__author__ = "Sandeep Rana"

__credits__ = ["Nishikanta Khandai"]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"



class Growth_Factor:

#------- class to compute growth factor--------------

    def __init__(self, h, Om, Onu, amin, dlna):

        """
        Parameters
        ----------
        Om: omega matter
        Onu: Omega Lambda
        h : Hubble param
        from gadget2 header

        if flag astropy=true is yes then use 
        cosmo : ``astropy.cosmology.LambdaCDM`` instance
                  Cosmological model.
        This to have general lambda CDM one can use 
        else:
         mention your cosmology and define HO and H(z) 
        :dlna: Step-size in log-space for scale-factor integration
        :amin: Minimum scale-factor (i.e.e maximum redshift) to integrate to.
             Only used for :meth:`growth_factor_fn`.
        """
        
        
        cosmo  = LambdaCDM(H0  = 100.*h, Om0 = Om,Ode0= Onu, Tcmb0=0.)
        self.cosmo = cosmo
        self.amin = amin
        self.dlna = dlna 
        self.Omega0 = self.cosmo.Om0+self.cosmo.Ode0

#--------------------------------------------------------------------------------

    def Hz(self, zepoch):
        return (H0 * np.sqrt(self.como.Om0*(1+zepoch)**3+
                       (1-self.Omega0)*(1+zepoch)**2 +
                       self.cosmo.Ode0))

    def X_z(self, zepoch):
        return 1.+ self.cosmo.Om0*zepoch + self.cosmo.Ode0*((1.+zepoch)**(-2) - 1.)

#--------------------------------------------------------------------------------

    def Dplus(self, z, getvec=False):
    
        """
        Finds the factor :math:`D^+(a)`, from Lukic et. al. 2007, eq. 8.

        Parameters
        ----------
        z : float
            The redshift

        getvec : bool, optional
            Whether to treat `z` as a maximum redshift and return a whole vector
            of values up to `z`. In this case, the minimum scale factor and the
            step size are defined in :attr:`_defaults` and can be over-ridden
            at instantiation.

        Returns
        -------
        Dplus : float
            The un-normalised growth factor.
        """


        a_upper = 1.0 / (1.0 + z)
        lna = np.arange(np.log(self.amin), np.log(a_upper) + self.dlna / 2, self.dlna)
        self.lna = lna
        self.zvec = 1.0 / np.exp(lna) - 1.0
        integrand = 1./self.X_z(self.zvec)**1.5

        if not getvec:    
        
            integral = integrate.simps(np.exp(lna)*integrand, dx=self.dlna)
            dplus = 5.0 * self.cosmo.Om0 * (1.+ z) * self.cosmo.H0**(-2) * self.X_z(z)**(0.5) * integral / 2.0
            
        else:

            integral = integrate.cumtrapz(np.exp(lna) * integrand, dx=self.dlna, initial=0.0)
            dplus = 5.0 * self.cosmo.Om0 * (1.+ self.zvec) * self.cosmo.H0**(-2) * self.X_z(self.zvec)**(0.5) * integral / 2.0

        return dplus
        

#--------------------------------------------------------------------------------

    def Dplus_general(self, z, getvec=False):
    
        """
        Finds the factor :math:`D^+(a)`, from Lukic et. al. 2007, eq. 8.

        Parameters
        ----------
        z : float
            The redshift

        getvec : bool, optional
            Whether to treat `z` as a maximum redshift and return a whole vector
            of values up to `z`. In this case, the minimum scale factor and the
            step size are defined in :attr:`_defaults` and can be over-ridden
            at instantiation.

        Returns
        -------
        Dplus : float
            The un-normalised growth factor.
        """


        a_upper = 1.0 / (1.0 + z)

        lna = np.arange(np.log(self.amin), np.log(a_upper) + self.dlna / 2, self.dlna)
        self.lna = lna
        self.zvec = 1.0 / np.exp(lna) - 1.0
        integrand = 1.0 / (np.exp(lna) * self.cosmo.efunc(self.zvec)) ** 3

        if not getvec:    
        
            integral = integrate.simps(np.exp(lna)*integrand, dx=self.dlna)
            dplus = 5.0 * self.cosmo.Om0 * self.cosmo.efunc(z) * integral / 2.0

        else:

            integral = integrate.cumtrapz(np.exp(lna) * integrand, dx=self.dlna, initial=1e-12)
            dplus = 5.0 * self.cosmo.Om0 * self.cosmo.efunc(self.zvec) * integral / 2.0

        return dplus

#--------------------------------------------------------------------------------
	
    def growth_factor(self, z, general=False):
    
        """
        Calculate :math:`d(a) = D^+(a)/D^+(a=1)`
        Parameters
        ----------
        z : float
            The redshift
        Returns
        -------
        float
            The normalised growth factor.
        """


        if not general:    
            growth = self.Dplus(z, True)/self.Dplus(0.0)
        else:
    	    growth = self.Dplus_general(z, True)/self.Dplus_general(0.0)
        return growth
	

#---------------------------------------------------------------------------------

    def growth_rate(self, zmin, key=False):
    
        """
        Growth rate, dln(d)/dln(a) from Hamilton 2000 eq. 4
        Calculate :math:`d(a) = D^+(a)/D^+(a=1)`,
        Returns a function G(z) using cubic spline interpolation.
        Parameters
        ----------
        zmin : float, optional
            The minimum redshift of the function. Default 0.0
        Returns
        -------
        callable
            The normalised growth factor as a function of redshift, or
            redshift as a function of growth factor if ``inverse`` is True.
    	Taking deriivative w.r.t dlna using gradient function.
        """
    
        gf = self.growth_factor(zmin, key)
        s = np.gradient(np.log(gf), self.dlna)
        
        return s

#---------------------------------------------------------------------------------

    def growth_rate_analytical(self, z, getvec=False):
        
        
        a_upper = 1.0 / (1.0 + z)

        lna = np.arange(np.log(self.amin), np.log(a_upper) + self.dlna / 2, self.dlna)
        self.lna = lna
        self.zvec = 1.0 / np.exp(lna) - 1.0
        integrand = 1./self.X_z(self.zvec)**1.5

        if not getvec:    

            integral = integrate.simps(np.exp(lna)*integrand, dx=self.dlna)
            temp4 =  self.X_z(z)**-1 * ( self.cosmo.Ode0*(1+z)**(-2) - 0.5 * self.como.Om0 * (1+z)) - 1
            f_analytical  = temp4 + ((1+z)**-1 * self.X_z(z)**(-1.5) /integral)
            
        else:
        
            integral = integrate.cumtrapz(np.exp(lna) * integrand, dx=self.dlna, initial=0.0)
            temp4 =  self.X_z(self.zvec)**-1 * ( self.cosmo.Ode0*(1+self.zvec)**(-2) - 0.5 * self.cosmo.Om0 * (1+self.zvec)) - 1
            f_analytical  = temp4 + ((1+self.zvec)**-1 * self.X_z(self.zvec)**(-1.5) /integral)

        return f_analytical
        
#---------------------------------------------------------------------------------

    def growth_rate_fnc(self, z):
        """
        Using interpolation get a functional 
        form of growth rate.
        """
        dummy_scale = 1./(1.+z) 
        gr = self.growth_rate(0.0)
        ind = (gr==np.inf)
        gr =gr[~ind] 
        grfn = _spline(np.exp(self.lna[~ind]), gr)
        return grfn(dummy_scale)
    
#---------------------------------------------------------------------------------

    def growth_factor_fnc(self, z):
        """
        Using interpolation get a functional 
        form of growth factor.
        """
        dummy_scale = 1./(1.+z) 
        gf = self.growth_factor(0.0)
        ind = (gf==np.inf)
        gf =gf[~ind] 

        gfn = _spline(np.exp(self.lna[~ind]), gf)
        return gfn(dummy_scale)


#---------------------------------------------------------------------------------

    def tofk2log10pofk_func(self, tofk, spectral_index, nk_modes):
        
        """
        This routine is to get power spectrum from transfer function.

        Params:
        tofk: Input transfer function.
        spectral_index: Primodial spectral index.
        nk_modes: k modes.
        """
        pofk_in = np.log10( tofk**2 * nk_modes**spectral_index)
        k_in  = np.log10(nk_modes)
        fint_tofk = _spline(k_in, pofk_in)
        return fint_tofk        

#---------------------------------------------------------------------------------

    def compute_sigma_sqr(self, pofk, k_range, Sigma, fname):
    
        """
        This routine is to normalize the input power spectrum.
        We have assumed smoothing scale to be 8 h^-1 Mpc.

        Params
        
        pofk: Input power spectrum to be normalized.
        
        k_range: log10(k) values.

        Sigma: Sigma_8 value used to obtain CAMB transer 
               function as input cosmology. 

        fname: Give either file name or entire directory path 
               e.g /mnt/data1/sandeep/multidark_pofk_norm_z0.dat
        
        Returns
        --------
        Double
        norm_delta_sqr: Normalize dimension less powerspectrum.
        Also writes a file to save quantities.

        """

        Smoothing_scale = 8. # h^-1 Mpc
        
        theta = (10**k_range)*Smoothing_scale #

        filter_func = 9*(np.sin(theta) - theta*np.cos(theta))**2/ theta**6
        
        integrand = (10**k_range)**3 * 10**pofk * filter_func * np.log(10.)
        integral = integrate.simps(integrand, x=k_range)#, initial=1e-8)

        sigma_sqr = integral/2./np.pi**2

        #normalization = 0.8102*0.8102/sigma_sqr # planck 2018
        #normalization = 0.8228*0.8228/sigma_sqr # multidark
        normalization = Sigma*Sigma/sigma_sqr # planck 2018
        
        delta_sqr = (10**k_range)**3 * 10**pofk /2/np.pi/np.pi
        
        norm_delta_sqr = (10**k_range)**3 * 10**pofk * normalization/2/np.pi**2
        
        maximum = np.max(10**pofk* normalization)
        
        ind = ((10**pofk*normalization)==maximum)

        self.DeltaPk = norm_delta_sqr
        self.k_range = k_range
#        print "maximum:",maximum
#        print "Scale of maximum:", (10**k_range)[ind]
        table = [k_range,  delta_sqr, norm_delta_sqr]        
        names = ['k_range', 'delta_sqr', 'norm_delta_sqr']        
        Formats = {'k_range':'%0.8f', 'delta_sqr':'%0.8f', 'norm_delta_sqr':'%0.8f'}
        ascii.write(table, fname, names=names, delimiter='\t', formats=Formats, overwrite=True)

        return norm_delta_sqr

#---------------------------------------------------------------------------------

    def filter_func1(self, r, k):
        theta=r*k 
        return np.sin(theta)/theta
        
    def filter_func2(self, r, k): 
        theta=r*k 
        return (np.sin(theta) - theta*np.cos(theta))/ theta**3.

#---------------------------------------------------------------------------------

    def Xi_theory(self, Rmin, Rmax, bins, fname, bin_type=True):
    
        """
        This routine is to compute linear theory correlation function
        from normalized powespectrum and given k range.
       
        Parameters
        -----------
        Rmin: Smallest r scale in h^-1 Mpc.

        Rmax: Maximum  r scale in h^-1 Mpc.
       
        bins: Total number of bins in r range.
       
        fname: Give either file name or entire directory path 
               e.g /mnt/data1/sandeep/multidark_Xiofr_z0.dat
               
        bin_type: Default true mean r range is lograthmic spaced
        
        Returns
        --------
        Double
        r_range: r range   
        Xi_r: Xi of r angle average correalation function.
        Xi_r_av: Volume average correlation function.
        sigma_sqr_r: RMS fluctuations as a function r.
        
        And also writes a file and save the above quantities.
        """

        if bin_type == True:    
            r_range = np.logspace(np.log10(Rmin),np.log10(Rmax),bins)
        else:                   
            r_range = np.linspace(Rmin,Rmax,bins)
        
        Xi_r       = np.zeros(int(bins), dtype=np.float64) 
        Xi_r_av    = np.zeros(int(bins), dtype=np.float64) 
        sigma_sqr_r= np.zeros(int(bins), dtype=np.float64) 

        lnk_range = np.log(10**self.k_range)
        for i in xrange(len(r_range)):

            integrand = self.DeltaPk*self.filter_func1(r_range[i], 10**self.k_range)
            integral = integrate.simps(integrand, x=lnk_range)
            Xi_r[i] = integral

            integrand = self.DeltaPk*self.filter_func2(r_range[i], 10**self.k_range)
            integral = integrate.simps(integrand, x=lnk_range)
            Xi_r_av[i] = 3*integral

            integrand = self.DeltaPk*self.filter_func2(r_range[i], 10**self.k_range)**2
            integral = integrate.simps(integrand, x=lnk_range)
            sigma_sqr_r[i] = 3*3*integral


        table = [r_range,  Xi_r, Xi_r_av, sigma_sqr_r]        
        names = ['r_range',  'Xi_r', 'Xi_r_av', 'sigma_sqr_r']        
        Formats = {'r_range':'%0.8f', 'Xi_r':'%0.8f', 'Xi_r_av':'%0.8f', 'sigma_sqr_r':'%0.8f'}
        ascii.write(table, fname, names=names, delimiter='\t', formats=Formats, overwrite=True)
        
        return r_range,  Xi_r, Xi_r_av, sigma_sqr_r



