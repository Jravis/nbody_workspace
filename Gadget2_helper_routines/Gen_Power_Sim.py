
#############################################################################
#Copyright (c) 2019, Sandeep Rana

# This is a generalized class to compute weighted
# dimension less 3D auto and cross power spectrum 
# of the density field on a grid smoothed using 
# CIC interpolation. Here we are using Gadget2 
# simulation output but this is generalized to 
# any output

# To Add: 1D power spectrum and  weights for 
# cases like lyman alpha forest.

#############################################################################

import numpy as np
import sys
import os
import pyfftw
import time
from numba import njit, jit

__author__ = "Sandeep Rana"

__credits__ = ["Nishikanta Khandai"]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"



#------------fftw plan----------------------------------
# This function performs the 3D FFT of a field in double precision

def FFT3Dr_d(a, threads):

    # align arrays
    dims  = len(a)
    a_in  = pyfftw.empty_aligned((dims,dims,dims), dtype='complex128')
    a_out = pyfftw.empty_aligned((dims,dims,dims),dtype='complex128')

    # plan FFTW
    fftw_plan = pyfftw.FFTW(a_in,a_out,axes=(0,1,2),
                            flags=('FFTW_ESTIMATE',),
                            direction='FFTW_FORWARD',threads=threads)
                            
    # put input array into delta_r and perform FFTW
    a_in [:] = a;  fftw_plan(a_in,a_out);  return a_out



#------------fftw plan----------------------------------
# This function performs the 3D FFT of a field in single precision

def FFT3Dr_s(a, threads):

    # align arrays
    dims  = len(a)
    a_in  = pyfftw.empty_aligned((dims,dims,dims), dtype='complex64')
    a_out = pyfftw.empty_aligned((dims,dims,dims),dtype='complex64')

    # plan FFTW
    fftw_plan = pyfftw.FFTW(a_in,a_out,axes=(0,1,2),
                            flags=('FFTW_ESTIMATE',),
                            direction='FFTW_FORWARD',threads=threads)
                            
    # put input array into delta_r and perform FFTW
    a_in [:] = a;  fftw_plan(a_in,a_out);  return a_out



#------------------------------------------------------------------------
#                               setweights()
#------------------------------------------------------------------------
#  This routine sets up the array weights which is used to compute the
#   power spectrum of density fluctuations.
#------------------------------------------------------------------------
@njit
def SetWeights(Nbox, w,  iweights, g, i1, i2, i3):
    
   
    for i in xrange(Nbox):
        for j in xrange(Nbox):
            for k in xrange(Nbox):

                m =  0.5+np.sqrt(i1[i]**2 + i2[j]**2 + i3[k]**2)
                iweights[np.int32(m)] += 1
                g[i, j, k] = w[i]**2+w[j]**2+w[k]**2
                
    return iweights, g

#-----------------------CIC Deconvolution--------------------------------------
#  Since CIC operation returns a smoothed density field which wash out the small 
#  scale information in power spectrum which will be disadvantageous for the case 
#  of high resolution simulations. This routine will do the deconvolution after fft
#  was performed.
#------------------------------------------------------------------------


@njit
def Deconv(Nbox, dummy_field, cic_deconv):
    for i in xrange(Nbox):
        for j in xrange(Nbox):
            for k in xrange(Nbox):
                dummy_field[i, j, k] *= cic_deconv[i]*cic_deconv[j]*cic_deconv[k]
    return dummy_field 


#-----------------------bin power--------------------------------------

@njit
def binpow(PowerSpec_3d, garray, co_eff, red_dk_real, red_dk_imag):

    temp = red_dk_real**2 +  red_dk_imag**2
    for i in xrange(len(garray)):
        i1 = np.int32(0.5+np.sqrt(garray[i]*co_eff))
        PowerSpec_3d[i1] = PowerSpec_3d[i1]+temp[i]
    return PowerSpec_3d



#-----------------------bin Cross power--------------------------------------

@njit
def binCpow(Crosspower, garray, co_eff, 
              red_dk1_real, red_dk1_imag, 
              red_dk2_real, red_dk2_imag):

    temp1 = (red_dk2_real*red_dk1_real) + (red_dk2_imag*red_dk1_imag)
    
    temp2 = (red_dk2_real*red_dk1_imag) - (red_dk2_imag*red_dk1_real)

    for i in xrange(len(garray)):
        i1 = np.int32(0.5+np.sqrt(garray[i]*co_eff))
        Crosspower[i1,0] = Crosspower[i1,0]+temp1[i]
        Crosspower[i1,1] = Crosspower[i1,1]+temp2[i]

    return Crosspower

#------------ Power Spectrum class ------------------------

class Sim_Power():
    

    def __init__(self, nbox, Lbox, snapnumber, Npart_sim):

        """
        
        Param: 
        
        nbox: Grid size you have like 400, 1024 or 2048
        
        Lbox:  Size of the Box preferably given from Gadget2 header

        snapnumber: The snapID or epoch ID of snapshot file

        Npart_sim: (Total number of particle)^1/3
        
        """
        
        self.boxsize = Lbox
        self.nbox = nbox
        self.w = np.zeros(nbox, dtype=np.float64)
        self.iw = np.zeros(nbox, dtype=np.int32)
        self.cic_deconv = np.zeros(nbox, dtype=np.float64)
        self.weights = np.zeros(nbox, dtype=np.float64)
        self.iweights = np.zeros(nbox, dtype=np.float32)
        self.kf = 2*np.pi/np.float64(nbox)
        self.scale = Lbox/np.float64(nbox)
        self.nhalf = nbox/2
        self.snapnumber = snapnumber
        self.fft_amp_3d = np.float64(nbox)**1.5
        self.npart_sim = Npart_sim

        
#------------------------------------------------------------------------
#                             setk()
#------------------------------------------------------------------------
# Compute the look up table for wave number k for any index in the
# box according to the convention used by FFT.  Positive wave numbers
# from index 0 to nbox/2.  Negative ones from nbox/2 to nbox-1.
# 
# Also creates an image array for the indices. Image wrt to the origin.
# E.g the (i, image[i]) are the indices of the vector (i*kf, -i*kf) 
# in the array w[i]
#------------------------------------------------------------------------

    def setk(self):
        grid_index = np.arange(0, self.nbox, 1, dtype=np.int32)
        half_index    = (grid_index>self.nhalf)
        self.iw[half_index] = (grid_index[half_index] - self.nbox)
        self.iw[~half_index] = grid_index[~half_index]
        self.w = self.iw * self.kf
        print "SETK() DONE\n"
    
#------------------------------------------------------------------------

    def setweights(self):
    
        i1 = np.arange(0, self.nbox, 1, dtype=np.int32)
        i2 = np.arange(0, self.nbox, 1, dtype=np.int32)
        i3 = np.arange(0, self.nbox, 1, dtype=np.int32)
        nhalf = self.nbox/2
        half_index  = (i1>=nhalf)
        i1[half_index] = self.nbox-i1[half_index]
        i2[half_index] = self.nbox-i2[half_index]
        i3[half_index] = self.nbox-i3[half_index]

        tpisq=2.0*np.pi**2
        g = np.zeros((self.nbox, self.nbox, self.nbox), dtype=np.float64)
        x, y = SetWeights(self.nbox, self.w , self.iweights, g, i1, i2, i3)
        self.iweights=x; self.g = y
        
        grid_index = np.arange(0, self.nbox, 1, dtype=np.int32)
        half_index  = (grid_index>0)*(grid_index<=nhalf)
        temp = self.w[half_index]**2.+3.* (0.5*self.w[1])**2.        
        self.weights[half_index] = self.w[half_index]*temp/(self.iweights[half_index]*tpisq)
        print ("SETWEIGHTS() DONE\n")


#------------------------------------------------------------------------


    def setdeconv(self, CIC=True):
    
        nhalf = self.nbox/2
        self.cic_deconv = np.zeros(self.nbox, dtype=np.float64)
        bb = 0.5*np.pi/self.w[nhalf]
        self.cic_deconv[0] = 1.0 
        theta =  self.w*bb
        denom = np.sin(theta[1:])/theta[1:] 
        
        if not CIC:
            self.cic_deconv[1:] = 1./(denom)**3.0 #TSC shape cloud
        else:
            self.cic_deconv[1:] = 1./(denom)**2.0
            
        print "SETDECONV() DONE\n"


#--------------------- Compute auto power spectrum -------------------

    def compute_3dpower(self, field, threads, save=False):

        """
        Parameters:
        field: Input 3D CIC density field 
        threads: number of threads you will use for FFTW
        Save: do you wana save 3D power spectrum in a file
              default is False. If true it won't return anyting
        """
    
        shape = field.shape
        if shape==(self.nbox, self.nbox, self.nbox):
            delta_k = FFT3Dr_d(np.complex128(field+0*1j), threads) 
            print"3D FFT DONE()\n"
        else:
            print "rhodm (Density) array shape is not (nbox,nbox,nbox)"
            print "Give Proper dimension array"
            sys.exit() 

        #normalise after FFT
        delta_k = delta_k/self.fft_amp_3d
        delta_k = Deconv(self.nbox, delta_k, self.cic_deconv)
        print"DECONVOLUTION Done\n"

        gg = self.g
        delta_k_real = delta_k.real
        delta_k_imag = delta_k.imag
        ind_g = (gg>0)        
        reduce_garray = gg[ind_g]
        reduce_dk_real = delta_k_real[ind_g]
        reduce_dk_imag = delta_k_imag[ind_g]
        coeff = (np.float64(self.nbox)/(2.*np.pi))**2. #this is (1/kf)^2
        powerspec_3d = np.zeros(self.nbox, dtype=np.float64)
        powerspec_3d = binpow(powerspec_3d, reduce_garray, 
                         coeff, reduce_dk_real, reduce_dk_imag)

        grid_index = np.arange(0, self.nbox, 1, dtype=np.int32)
        half_index  = (grid_index<=self.nhalf)*(grid_index>0)
        w_scale = self.w[half_index]/self.scale
        power_times_weights = powerspec_3d[half_index]*self.weights[half_index]        

        if not save: 
            print "return: k-scale\tdelta^2\n"
            return w_scale, power_times_weights
        else:
            print("Enter the name of the directory you want\n")
            dirc_name = raw_input("")
            os.mkdir(dirc_name) 
            file_out = (dirc_name+"3dpower_dm_Lbox_%dMpc_"\
                        "NpartSim_%d_Ngrid_%d_SnapId_%d_"\
                        "REAL.txt"%(self.boxsize, self.npart_sim, 
                        self.nbox, self.snapnumber))
            
            np.savetxt(file_out, zip(w_scale, power_times_weights, 
                       1./np.sqrt(self.iweights)), header='K-scale\tdelta^2\tbin_error'
                       , fmt="%.8f\t%0.8f\t%0.8f", delimiter='\t')
    
#--------------------- Compute Cross power spectrum -------------------


    #def compute_3d_CrossPower(self, field1, field2, threads, BIAS, save=False):
    
    def compute_3d_CrossPower(self, field1, field2, threads, save=False):
    
        """
        Parameters:
        field: Input 3D CIC density field 
        field 2 is the matter density 
        threads: number of threads you will use for FFTW
        Save: do you wana save 3D power spectrum in a file
              default is False
        """
    
        shape = field1.shape
        if shape==(self.nbox, self.nbox, self.nbox):
            delta_k1 = FFT3Dr_d(np.complex128(field1+0*1j), threads) 
            print"3D FFT DONE()\n"
        else:
            print "rhodm (Density) array shape is not (nbox,nbox,nbox)"
            print "Give Proper dimension array"
            sys.exit() 

        shape = field2.shape
        if shape==(self.nbox, self.nbox, self.nbox):
            delta_k2 = FFT3Dr_d(np.complex128(field2+0*1j), threads) 
            print"3D FFT DONE()\n"
        else:
            print "rhodm (Density) array shape is not (nbox,nbox,nbox)"
            print "Give Proper dimension array"
            sys.exit() 


        #normalise after FFT
        delta_k1 = delta_k1/self.fft_amp_3d
        delta_k1 = Deconv(self.nbox, delta_k1, self.cic_deconv)
        print"DECONVOLUTION field 1 Done\n"

        delta_k2 = delta_k2/self.fft_amp_3d
        delta_k2 = Deconv(self.nbox, delta_k2, self.cic_deconv)
        print"DECONVOLUTION field 2 Done\n"

        gg = self.g

        delta_k1_real = delta_k1.real
        delta_k1_imag = delta_k1.imag

        delta_k2_real = delta_k2.real
        delta_k2_imag = delta_k2.imag

        ind_g = (gg>0)        

        reduce_garray = gg[ind_g]
        reduce_dk1_real = delta_k1_real[ind_g]
        reduce_dk1_imag = delta_k1_imag[ind_g]

        reduce_dk2_real = delta_k2_real[ind_g]
        reduce_dk2_imag = delta_k2_imag[ind_g]

        coeff = (np.float64(self.nbox)/(2.*np.pi))**2. #this is (1/kf)^2

        crosspower = np.zeros((self.nbox, 2), dtype=np.float64)
        crosspower = binCpow(crosspower, reduce_garray, coeff
                            , reduce_dk1_real, reduce_dk1_imag
                            , reduce_dk2_real, reduce_dk2_imag)

        powerspec_3d1 = np.zeros(self.nbox, dtype=np.float64)
        powerspec_3d1 = binpow(powerspec_3d1, reduce_garray, 
                         coeff, reduce_dk1_real, reduce_dk1_imag)


        powerspec_3d2 = np.zeros(self.nbox, dtype=np.float64)
        powerspec_3d2 = binpow(powerspec_3d2, reduce_garray, 
                         coeff, reduce_dk2_real, reduce_dk2_imag)

        bias = np.zeros((self.nbox, 2), dtype=np.float64)
        bias[:, 0] = crosspower[:, 0]/powerspec_3d2
        bias[:, 1] = crosspower[:, 1]/powerspec_3d2


        cross_denom = np.sqrt(powerspec_3d2*powerspec_3d1) 
        crosscoeff = np.zeros((self.nbox, 2), dtype=np.float64)
        crosscoeff[:, 0] = crosspower[:, 0]/ cross_denom
        crosscoeff[:, 1] = crosspower[:, 1]/ cross_denom

        grid_index = np.arange(0, self.nbox, 1, dtype=np.int32)
        half_index  = (grid_index<=self.nhalf)*(grid_index>0)


        w_scale = self.w[half_index]/self.scale
        power_times_weights1 = powerspec_3d1[half_index]*self.weights[half_index]        
        power_times_weights2 = powerspec_3d2[half_index]*self.weights[half_index]        
        crosspower_weights1  = (crosspower[:, 0])[half_index]*self.weights[half_index]        
        crosspower_weights2  = (crosspower[:, 1])[half_index]*self.weights[half_index]        
        crosscoeff1          = (crosscoeff[:, 0])[half_index]
        crosscoeff2          = (crosscoeff[:, 1])[half_index]
        bias1                = (bias[:, 0])[half_index]
        bias2                = (bias[:, 1])[half_index]

        if not save: 
            print "return : k-scale\tdelta1^2\tdelta2^2\t"\
                  "CrossPow_+ve\tCrossPow_-ve\t"\
                  "CrossCoeff_+ve\tCrossCoeff_-ve\t"\
                  "bias_+ve\tbias_-ve\n"
            return (w_scale, power_times_weights1, power_times_weights2, 
                    crosspower_weights1, crosspower_weights2, crosscoeff1,
                    crosscoeff2, bias1, bias2)
        else:
            #file_out = file_out+"3dCrossPow_dm_%dMpc_%d_%d_thresh_%d_scaled_test.txt"%(self.boxsize, self.nbox, self.snapnumber, BIAS)
            print("Enter the name of the directory you want\n")
            dirc_name = raw_input("")
            os.mkdir(dirc_name) 
            file_out = (dirc_name+"3dCrossPow_dm_Lbox_%dMpc_"\
                        "NpartSim_%d_Ngrid_%d_SnapId_%d_"\
                        "REAL.txt"%(self.boxsize, self.npart_sim, 
                        self.nbox, self.snapnumber))

            np.savetxt(file_out, zip(w_scale, power_times_weights1, power_times_weights2, 
                       crosspower_weights1, crosspower_weights2, crosscoeff1,
                       crosscoeff2, bias1, bias2), 
                       header=("k-scale\tdelta1^2\tdelta2^2\t"\
                              "CrossPow_Re\tCrossPow_Img\t"\
                              "CrossCoeff_Re\tCrossCoeff_Img\t"\
                              "bias_Re\tbias_Img"),
                       fmt=("%.8f\t%0.8f\t%0.8f\t%0.8f"\
                           "\t%0.8f\t%0.8f\t%0.8f\t%0.8f\t%0.8f"), delimiter='\t')
                           

#--------------------- Compute mode counting -------------------


    def compute_modes(self, field1, field2, threads, nmax):

        """
        Parameters:
        field  : Input 3D CIC density field 
        threads: number of threads you will use for FFTW
        nmax = maximum number of modes you want to commpute for
        Save   : compute first few modes of the field means 
                 delta(k) vs k scatter
        """
    
    #----------------------mode counting----------------------------------------

        @njit
        def mode_count(Nbox, Nhalf, scale, w, co_eff, kmax, Nmodes, d1r, d1c, d2r, d2c):
            
            wi_scale = []
            wj_scale = []
            wk_scale = []
            sqrt_gtot = []                           
            Musquare = []
            field1_r = []
            field1_c = []
            field2_r = []
            field2_c = []

                                  
            for k in xrange(0, Nhalf+1):
                if k>0:
                    for i in xrange(Nbox): 
                        for j in xrange(Nbox): 
                            gperp = w[i]**2 + w[j]**2 
                            iperp = 0.5 + np.sqrt(gperp*co_eff) # rounding
                            gpara = w[k]**2  
                            ipara = np.sqrt(gpara*co_eff)
                            gtot = w[i]**2+w[j]**2+w[k]**2
                            if  iperp<0:
                                raise ValueError('Something Wrong Here ...iperp ... is -ve...QUITTING')
                            if  ipara<0:
                                raise ValueError('Something Wrong Here ...iparap ... is -ve...QUITTING')
                                
                            if (w[k]  <= kmax and w[j] <= kmax and w[i] <=kmax):
                                if (gtot <= kmax*kmax and gtot != 0.0):
                                    Nmodes+=1
                                    musquare = gpara/gtot  # K_z/|K|
                                    Musquare.append(musquare)
                                    wi_scale.append(w[i]/(scale*1.))
                                    wj_scale.append(w[i]/(scale*1.))
                                    wk_scale.append(w[i]/(scale*1.))
                                    sqrt_gtot.append(np.sqrt(gtot))
                                    field1_r.append(d1r[i, j, k])            
                                    field1_c.append(d1c[i, j, k])            
                                    field2_r.append(d2r[i, j, k])            
                                    field2_c.append(d2c[i, j, k])            
#------------------------------------------------------------------------
                else:
                    for i in xrange(0, Nhalf+1): 
                        if i>0:
                            for j in xrange(Nbox): 
                                gperp = w[i]**2 + w[j]**2 
                                iperp = 0.5 + np.sqrt(gperp*co_eff) # rounding
                                gpara = w[k]**2  
                                ipara = np.sqrt(gpara*co_eff)
                                gtot = w[i]**2+w[j]**2+w[k]**2
                                if  iperp<0:
                                    raise ValueError('Something Wrong Here ...iperp ... is -ve...QUITTING')
                                if  ipara<0:
                                    raise ValueError('Something Wrong Here ...iparap ... is -ve...QUITTING')
                                    
                                if (w[k]  <= kmax and w[j] <= kmax and w[i] <=kmax):
                                    if (gtot <= kmax*kmax and gtot != 0.0):
                                        Nmodes+=1
                                        musquare = gpara/gtot  # K_z/|K|
                                        Musquare.append(musquare)
                                        wi_scale.append(w[i]/(scale*1.))
                                        wj_scale.append(w[i]/(scale*1.))
                                        wk_scale.append(w[i]/(scale*1.))
                                        sqrt_gtot.append(np.sqrt(gtot))
                                        field1_r.append(d1r[i, j, k])            
                                        field1_c.append(d1c[i, j, k])            
                                        field2_r.append(d2r[i, j, k])            
                                        field2_c.append(d2c[i, j, k])            
#------------------------------------------------------------------------
                        else:
                            for j in xrange(1, Nhalf+1): 
                                gperp = w[i]**2 + w[j]**2 
                                iperp = 0.5 + np.sqrt(gperp*co_eff) # rounding
                                gpara = w[k]**2  
                                ipara = np.sqrt(gpara*co_eff)
                                gtot = w[i]**2+w[j]**2+w[k]**2
                                if  iperp<0:
                                    raise ValueError('Something Wrong Here ...iperp ... is -ve...QUITTING')
                                if  ipara<0:
                                    raise ValueError('Something Wrong Here ...iparap ... is -ve...QUITTING')
                                    
                                if (w[k]  <= kmax and w[j] <= kmax and w[i] <=kmax):
                                    if (gtot <= kmax*kmax and gtot != 0.0):
                                        Nmodes+=1
                                        musquare = gpara/gtot  # K_z/|K|
                                        Musquare.append(musquare)
                                        wi_scale.append(w[i]/(scale*1.))
                                        wj_scale.append(w[i]/(scale*1.))
                                        wk_scale.append(w[i]/(scale*1.))
                                        sqrt_gtot.append(np.sqrt(gtot))
                                        field1_r.append(d1r[i, j, k])            
                                        field1_c.append(d1c[i, j, k])            
                                        field2_r.append(d2r[i, j, k])            
                                        field2_c.append(d2c[i, j, k])            
            

            data = np.zeros((9, Nmodes), dtype=np.float64)
            data[0,:] = np.asarray(wi_scale)
            data[1,:] = np.asarray(wj_scale)
            data[2,:] = np.asarray(wk_scale)
            data[3,:] = np.asarray(Musquare)
            data[4,:] = np.asarray(sqrt_gtot)
            data[5,:] = np.asarray(field1_r)
            data[6,:] = np.asarray(field1_c)
            data[7,:] = np.asarray(field2_r)
            data[8,:] = np.asarray(field2_c)

            return Nmodes, data
            


    #----------------------basic routine----------------------------------------
    
        shape = field1.shape
        if shape==(self.nbox, self.nbox, self.nbox):
            delta_k1 = FFT3Dr_d(np.complex128(field1+0*1j), threads) 
            print"3D FFT DONE()\n"
        else:
            print "rhodm (Density) array shape is not (nbox,nbox,nbox)"
            print "Give Proper dimension array"
            sys.exit() 
        
        shape = field2.shape
        if shape==(self.nbox, self.nbox, self.nbox):
            delta_k2 = FFT3Dr_d(np.complex128(field2+0*1j), threads) 
            print"3D FFT DONE()\n"
        else:
            print "rhodm (Density) array shape is not (nbox,nbox,nbox)"
            print "Give Proper dimension array"
            sys.exit() 

        #normalise after FFT
        delta_k1 = delta_k1/self.fft_amp_3d
        delta_k1 = Deconv(self.nbox, delta_k1, self.cic_deconv)
        print"DECONVOLUTION field 1 Done\n"

        delta_k2 = delta_k2/self.fft_amp_3d
        delta_k2 = Deconv(self.nbox, delta_k2, self.cic_deconv)
        print"DECONVOLUTION field 2 Done\n"

        delta_k1_real = delta_k1.real*self.scale**1.5
        delta_k1_imag = delta_k1.imag*self.scale**1.5

        delta_k2_real = delta_k2.real*self.scale**1.5
        delta_k2_imag = delta_k2.imag*self.scale**1.5

        #remember the modes are not dimensionless
        
        nhalf = self.nbox/2
        Kmax = self.kf*np.float64(nmax)
        nmodes_total = 0 
        coeff = (np.float64(self.nbox)/(2.*np.pi))**2. #this is (1/kf)^2
        outfile_dir="/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/%dMpc_256_analysis/"% (self.boxsize)
        #outfile_dir="/mnt/data1/sandeep/Simulation_data/Cross_Power_Mode_Count_Data/"
        fname = outfile_dir+"modes_%03d_%03d_%03d_%d.txt"%(self.boxsize, self.nbox, self.snapnumber, nmax)
        
        nmodes_total, data = mode_count(self.nbox, nhalf, self.scale, self.w, 
                                        coeff, Kmax, nmodes_total, delta_k1_real, 
                                        delta_k1_imag, delta_k2_real, delta_k2_imag)

        np.savetxt(fname, data.T, fmt="%g", delimiter='\t')
        #header='Wi_scale\tWj_scale\tWk_scale\tMuSquare\tSqrt_Gtot\tField1_r\tField1_c\tField2_r\tField2_c')

        print"Mode Counting Done files written\n"
        fname = outfile_dir+"nmodes_%03d_%03d_%0.3d_%d.txt"%(self.boxsize, self.nbox, self.snapnumber, nmax)
        print "nmode=%d kf=%g \n"%(nmodes_total,self.kf)
        fp = open(fname, "w")
        fp.write("%d\t%0.8f\t%d\t%d\n"%(nmodes_total, self.kf, self.boxsize, self.nbox))
        fp.close()

