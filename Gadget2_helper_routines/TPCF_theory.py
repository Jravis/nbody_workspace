
#############################################################################

#Copyright (c) 2019, Sandeep Rana
# This is module is to compute linear 
#angle average and volume average correlation 
#function and mass (rms) fluctuiation given the
#smoothing length of 8 h^-1 Mpc.

# I have also added functionlaity for velocity dispersion 
# , following Sheth and Diaferio 2001 prescription
# module will compute the f(v) one-dimensional distribution of peculiar velocities
#############################################################################


import numpy as np
from scipy import integrate
from astropy.cosmology import LambdaCDM
from astropy.io import ascii
import sys,os

__author__ = "Sandeep Rana"

__credits__ = [""]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"



class TPCF_Theory:

#------- class to compute growth factor--------------

    def __init__(self, h, Om, Onu, DeltaPk, log_10k):

        """
        Parameters
        ----------
        Om: omega matter
        Onu: Omega Lambda
        h : Hubble param
        from gadget2 header

        if flag astropy=true is yes then use 
        cosmo : ``astropy.cosmology.LambdaCDM`` instance
                  Cosmological model.
        This to have general lambda CDM one can use 
        else:
         mention your cosmology and define HO and H(z) 
        :dlna: Step-size in log-space for scale-factor integration
        :amin: Minimum scale-factor (i.e.e maximum redshift) to integrate to.
             Only used for :meth:`growth_factor_fn`.
        """
        cosmo  = LambdaCDM(H0  = 100.*h, Om0 = Om,Ode0= Onu, Tcmb0=0.)
        self.cosmo = cosmo
        self.k_range = log_10k
        self.Omega0 = self.cosmo.Om0+self.cosmo.Ode0
        self.DeltaPk = DeltaPk


    def filter_func1(self, r, k):
        theta=r*k 
        return np.sin(theta)/theta
        
    def filter_func2(self, r, k): 
        theta=r*k 
        return (np.sin(theta) - theta*np.cos(theta))/ theta**3.

    def filter_func3(self, r, k): 
        theta=r*k 
        tt = (15*np.sin(theta) - 6*theta**2*np.sin(theta) - 
             15.*theta*np.cos(theta) + theta**3*np.cos(theta))
        return tt/ theta**5.

#---------------------------------------------------------------------------------

    def Xi_theory(self, Rmin, Rmax, bins, fname, bin_type=True):

        if bin_type == True:    
            r_range = np.logspace(np.log10(Rmin),np.log10(Rmax),bins)
        else:                   
            r_range = np.linspace(Rmin,Rmax,bins)
        
        Xi_r       = np.zeros(int(bins), dtype=np.float64) 
        Xi_r_av    = np.zeros(int(bins), dtype=np.float64) 
        rsd_correc = np.zeros(int(bins), dtype=np.float64) 
        sigma_sqr_r= np.zeros(int(bins), dtype=np.float64) 

        lnk_range = np.log(10**self.k_range)
        for i in xrange(len(r_range)):

            integrand = self.DeltaPk*self.filter_func1(r_range[i], 10**self.k_range)
            integral = integrate.simps(integrand, x=lnk_range)
            Xi_r[i] = integral

            integrand = self.DeltaPk*self.filter_func2(r_range[i], 10**self.k_range)
            integral = integrate.simps(integrand, x=lnk_range)
            Xi_r_av[i] = 3*integral

            integrand = self.DeltaPk*self.filter_func3(r_range[i], 10**self.k_range)
            integral = integrate.simps(integrand, x=lnk_range)
            rsd_correc[i] = integral

            integrand = self.DeltaPk*self.filter_func2(r_range[i], 10**self.k_range)**2
            integral = integrate.simps(integrand, x=lnk_range)
            sigma_sqr_r[i] = 3*3*integral


        table = [r_range,  Xi_r, Xi_r_av, sigma_sqr_r, rsd_correc]        
        names = ['r_range',  'Xi_r', 'Xi_r_av', 'sigma_sqr_r', "rsd_correc"]        
        Formats = {'r_range':'%0.8f', 'Xi_r':'%0.8f', 'Xi_r_av':'%0.8f', 'sigma_sqr_r':'%0.8f','rsd_correc':'%0.8f' }
        ascii.write(table, fname, names=names, delimiter='\t', formats=Formats, overwrite=True)
        return r_range,  Xi_r, Xi_r_av, sigma_sqr_r, rsd_correc


#------------------------------------------------------------------------------
    

    def sigma_halo(self, Rmin, Rmax, bins, fname, bin_type=True, box_size_effect=False):
        """
        Sheth and Diaferio 2001
        -----
        Paramters:
        ------
        Results
        """



        if not bin_type:    
            r_range = np.linspace(Rmin,Rmax,bins)
        else:                   
            r_range = np.logspace(np.log10(Rmin),np.log10(Rmax),bins)
        
        if not  box_size_effect:
            lnk_range = np.log(10**self.k_range)
            Delta_Pk  = self.DeltaPk
        else:
            print("Enter the Box size Lbox\n")
            Lbox = np.float64(raw_input(""))
            keep = (lnk_range< np.log(np.pi*2./Lbox))
            Delta_Pk = self.DeltaPk
            Delta_Pk[keep] = 0.0

        sigma_sqr_0= np.zeros(int(bins), dtype=np.float64) 
        sigma_sqr_1= np.zeros(int(bins), dtype=np.float64) 
        sigma_sqr_minus1= np.zeros(int(bins), dtype=np.float64) 

        for i in xrange(len(r_range)):


            integrand = Delta_Pk*self.filter_func2(r_range[i], 10**self.k_range)**2 
            integral  = integrate.simps(integrand, x=lnk_range)
            sigma_sqr_0[i] = 3*3*integral
            
            integrand = Delta_Pk*self.filter_func2(r_range[i], 10**self.k_range)**2 * (10**self.k_range)**2.0
            integral  = integrate.simps(integrand, x=lnk_range)
            sigma_sqr_1[i] = 3*3*integral

            integrand = Delta_Pk*self.filter_func2(r_range[i], 10**self.k_range)**2 * (10**self.k_range)**(-2.0)
            integral  = integrate.simps(integrand, x=lnk_range)
            sigma_sqr_minus1[i] = 3*3*integral
        
        temp = sigma_sqr_0**2.0/sigma_sqr_1
        temp = temp/sigma_sqr_minus1
        
        sigma_halo_m = self.cosmo.HO.vlaue * self.Omega0**0.6 * np.sqrt(sigma_sqr_1) * np.sqrt(1 - temp)
       return sigma_halo_m
       


