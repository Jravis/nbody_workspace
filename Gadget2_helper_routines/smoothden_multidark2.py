


import numpy as np
from numba import njit
import itertools
import sys
import os
from astropy.cosmology import LambdaCDM
from astropy import constants as const
import astropy.units as u



#--------------- cosmology func --------------

def H_z(z_epoch, small_h, Omega_matter, 
            Omega_not, Omega_Lambda):
    E_z = np.sqrt(Omega_matter*(1+z_epoch)
          +(1-Omega_not)*(1+z_epoch)**2
          +Omega_Lambda)

    return small_h * 100. * E_z # km s^-1 Mpc^-1


#---------------------------------------------------------------
#CIC Breaking
# ints contains the integer values of the positions. 
# fracs contains the values [(1-f)_x,(1-f)_y,(1-f)_z,f_x,f_y,f_z ]
# where f is the fractional part of the position. 
#---------------------------------------------------------------

def CIC_component(Nbox, NpartTotal, Pos1):
        
    ints = np.floor(Pos1).astype(np.int32)
    ints_x = ints[:,0]
    ints_y = ints[:,1]
    ints_z = ints[:,2]

    j1_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    j2_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    j3_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    for i in xrange(0, 2):
        j1_ind[:, i] = (i+ints_x)%Nbox
        j2_ind[:, i] = (i+ints_y)%Nbox
        j3_ind[:, i] = (i+ints_z)%Nbox
    
    frac = np.modf(Pos1)[0]
    remfrac = 1 - frac

    return j1_ind, j2_ind, j3_ind, frac, remfrac


#---------------------------------------------------------------


@njit
def TSC_interpolation(Nbox, NpartTotal, masspart, Pos1):
    """
    """
    denpart = np.zeros((Nbox, Nbox, Nbox), dtype=np.float64)
    
    TSC_W_x = np.zeros(3 , dtype=np.float64)
    TSC_W_y = np.zeros(3 , dtype=np.float64)
    TSC_W_z = np.zeros(3 , dtype=np.float64)
    
    #Compute CIC weights

    index_x = np.zeros(3 , dtype=np.int32)
    index_y = np.zeros(3 , dtype=np.int32)
    index_z = np.zeros(3 , dtype=np.int32)


        
    for i in xrange(NpartTotal):
        
        for j in xrange(-1, 2):
            
            intpos = np.rint(Pos1[i, 0])
            diff = np.fabs(intpos+j - Pos1[i, 0])
            index_x[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_x[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)

            
            intpos = np.rint(Pos1[i, 1])
            diff = np.fabs(intpos+j - Pos1[i, 1])
            index_y[j+1] = (intpos + j)%Nbox 

           
            if   j==-1: 
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_y[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


            intpos = np.rint(Pos1[i, 2])+0.5
            diff = np.fabs(intpos+j - Pos1[i, 2])
            index_z[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_z[j+1] =  0.75 - diff*diff
            else:  
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


        for l in xrange(3):  
            for m in xrange(3):  
                for n in xrange(3):
                    denpart[index_x[l], index_y[m], index_z[n]] += np.float64(masspart) * TSC_W_x[l] * TSC_W_y[m]* TSC_W_z[n]
       
        
    return denpart 



class SmoothDensity:

#------- class to compute growth factor


    def __init__(self, header , nbox): 
        
        """
        Parameters:
          header  : Gadget2 header
          Position: Position data frame
        Velocities: velocity data frame
              nbox: Is the Grid resolution 400,1024 or 2048
              RSD : Default False if True then do RSD along default 
                    reference the direction that is Z direction 
           Sliced : Deafult False if True then make slice of a width 
                    given by user and do 2D CIC on that.
        Slice_axis: Slice along which axis 0-x, 1-y, 3-z
        """

        self.nbox =  nbox
        self.header  = header
        self.ParticleMass =header.massarr[1]
        #----- computing multiplicative RSD boost factor------------
        # In Gadget Peculiar veocities  = sqrt(a)* Physical velocity
        # In Gadget Peculiar veocities  = sqrt(a)^{-1}* comoving velocity

        self.cosmo  = LambdaCDM(H0= 100.*header.HubbleParam, 
                       Om0 = header.Omega0,
                       Ode0= header.OmegaLambda,
                       Tcmb0=0.)
                        
        #self.Omega0 = snapshot.Omega0+snapshot.OmegaLambda
        #self.H_z = self.cosmo.H(header.redshift).value
        self.boxfac = np.float32(nbox/header.BoxSize)
   

        

#--------------------Redshift Space mapping  --------------------------------------------------------

    def RSD_Mapping(self, Positions, Velocities):

            #----- In case of RSD the data frame will have 4 coloumn
            #----- the last coloumn is the redshifted axis for e.g 
            #----- if the LOS direction is z then 4th coloumn is sz
            

            H_z = self.cosmo.H(self.header.redshift).value
        
            #--- since gadget2 internal position units are in kpc h^-1

            conv_gadget_units = 1000.*self.header.HubbleParam
            boost_factor = np.sqrt(1.+ self.header.redshift)*conv_gadget_units/H_z
            
            print "redshfhift: %0.4e Hubble parameter H: %f  factor: %f"%(self.header.redshift, H_z, boost_factor) 
            print " Redshift mapping in z direction"

            Positions['sz'] = Positions['z']+boost_factor*Velocities['vz']
            
            RSD_Pos = np.asarray(Positions.sz.values)
            ind = (RSD_Pos<0.0)
            RSD_Pos[ind]= (RSD_Pos[ind]+self.header.BoxSize)%self.header.BoxSize
            ind = (RSD_Pos>self.header.BoxSize)
            RSD_Pos[ind]= (RSD_Pos[ind]+self.header.BoxSize)%self.header.BoxSize
            Positions['sz1'] = RSD_Pos
            print " Redshift mapping done"
            del Velocities 
            return Positions


#--------------------3D Real Space  --------------------------------------------------------


    def Setting_3D_CIC_Real(self, Positions):

            self.npart = self.header.npart[1]
            
            #compute total masses
            totmdm = self.npart*self.header.massarr[1] #*1e10
            #rhodm_avg = (slice_width*boxfac)*totmdm/np.float64(self.nbox)**2.
            self.rhodm_avg = totmdm/np.float64(self.nbox)**3.
            print"-------------------------------------------"
            print "totodm : %f  rhodm_avg : %f"%(totmdm, self.rhodm_avg)
            print "The Total Number of Particles: %d"%self.npart
            print"-------------------------------------------"
            
            Pos = Positions.to_numpy()
            Pos[:, 0] *= self.boxfac
            Pos[:, 1] *= self.boxfac
            Pos[:, 2] *= self.boxfac

            self.j1_ind, self.j2_ind, self.j3_ind, self.frac, self.remfrac = CIC_component(self.nbox,self.npart, Pos)   

#--------------------3D Redshift Space  --------------------------------------------------------

    def Setting_3D_CIC_RSD(self, Positions, Velocities):
    

            self.npart = self.header.npart[1]
            
            #compute total masses
            totmdm = self.npart*self.header.massarr[1] #*1e10
            self.rhodm_avg = totmdm/np.float64(self.nbox)**3.
            
            print"-------------------------------------------"
            print "totodm : %f  rhodm_avg : %f"%(totmdm, self.rhodm_avg)
            print "The Total Number of Particles: %d"%self.npart
            print"-------------------------------------------"
    
            pos = self.RSD_Mapping(Positions, Velocities)
            temp = pos.to_numpy()
            Pos  = np.zeros((self.npart, 3), dtype=np.float32)
            Pos[:, 0] = temp[:, 0]
            Pos[:, 1] = temp[:, 1]
            Pos[:, 2] = temp[:, 4]
            del temp
            Pos[:, 0] *= self.boxfac
            Pos[:, 1] *= self.boxfac
            Pos[:, 2] *= self.boxfac
            self.j1_ind, self.j2_ind, self.j3_ind, self.frac, self.remfrac = CIC_component(self.nbox,self.npart, Pos)   
            
            
#--------------------3D Redshift Space  --------------------------------------------------------
            
    def Setting_2D_CIC_Real(self, Positions):

            
            #print "Enter the width of the Slice Gadget 2 position are in kpc h^-1. So you want 6Mpc h^-1 is 6000kpc h^-1 width"
            print "Enter the width of the Slice Gadget 2 in Mpc h^-1"
            #slice_width = np.float64(raw_input(""))*1000 # Mpc to kpc conversion 
            slice_width = np.float64(20.0)*1000 # Mpc to kpc conversion 
            print "-------------------------------------------------------------------"
            print "2D slice along x axis: 1 ,  2D slice along y axis: 2,  2D slice along z axis: 3"
            #self.cic_axis = np.int32(raw_input(""))
            self.cic_axis = np.int32(2)

            if self.cic_axis == 1:
                Positions_sliced = Positions[(Positions.x.values>=0.0) & (Positions.x.values<=slice_width)]
            if self.cic_axis == 2:
                Positions_sliced = Positions[(Positions.y.values>=0.0) & (Positions.y.values<=slice_width)]
            if self.cic_axis == 3:
                Positions_sliced = Positions[(Positions.z.values>=0.0) & (Positions.z.values<=slice_width)]
           
            
            self.npart = Positions_sliced.shape[0]
            print "The Total Number of Particles in the slice: %d"%self.npart

            #compute total masses
            totmdm = self.npart*self.header.massarr[1] #*1e10
            self.rhodm_avg = (slice_width*self.boxfac)*totmdm/np.float64(self.nbox)**2.
            print"-------------------------------------------"
            print "totodm : %f  rhodm_avg : %f"%(totmdm, self.rhodm_avg)
            print "The Total Number of Particles: %d"%self.npart
            print"-------------------------------------------"
            
            self.slice_width =slice_width            

            Pos = Positions_sliced.to_numpy()
            Pos[:, 0] *= self.boxfac
            Pos[:, 1] *= self.boxfac
            Pos[:, 2] *= self.boxfac
            
            self.slice_pos = Pos
            self.j1_ind, self.j2_ind, self.j3_ind, self.frac, self.remfrac = CIC_component(self.nbox,self.npart, Pos)   

#-------------------------------------------------------------------------------------------------------------------


    def Setting_2D_CIC_RSD(self, Positions, Velocities):
    
                
            positions = self.RSD_Mapping(Positions, Velocities)
                        
            print "Enter the width of the Slice Gadget 2 position are in kpc h^-1. So you want 6Mpc h^-1 is 6000kpc h^-1 width\n"
            slice_width = np.float64(raw_input(""))
            print "Since RSD mapping by default along z direction therefore 2D slice is along y axis"

            positions_sliced = positions[(positions.y.values>=0.0) & (positions.y.values<=slice_width)]
            self.npart = positions_sliced.shape[0]
            print "The Total Number of Particles in the slice: %d"%self.npart

            #compute total masses
            totmdm = self.npart*self.header.massarr[1] #*1e10
            rhodm_avg = (slice_width*boxfac)*totmdm/np.float64(self.nbox)**2.
            
            print"-------------------------------------------"
            print "totodm : %f  rhodm_avg : %f"%(totmdm, self.rhodm_avg)
            print "The Total Number of Particles: %d"%self.npart
            print"-------------------------------------------"

            temp = positions_sliced.to_numpy()
            Pos  = np.zeros((self.npart, 3), dtype=np.float32)
            Pos[:, 0] = temp[:, 0]
            Pos[:, 1] = temp[:, 1]
            Pos[:, 2] = temp[:, 4]
            del temp

            Pos[:, 0] *= self.boxfac
            Pos[:, 1] *= self.boxfac
            Pos[:, 2] *= self.boxfac
            self.j1_ind, self.j2_ind, self.j3_ind, self.frac, self.remfrac = CIC_component(self.nbox,self.npart, Pos)   
 

#--------------------------------------------------------------------------------------------------

    def Compute_CIC_weight(self):
        """
        Compute CIC weights
        """
        self.CIC_W_x = np.zeros((self.npart,2) , dtype=np.float64)
        self.CIC_W_y = np.zeros((self.npart,2) , dtype=np.float64)
        self.CIC_W_z = np.zeros((self.npart,2) , dtype=np.float64)
        
        self.CIC_W_x[:,1] = self.frac[:,0]
        self.CIC_W_x[:,0] = self.remfrac[:,0]
        self.CIC_W_y[:,1] = self.frac[:,1]
        self.CIC_W_y[:,0] = self.remfrac[:,1]
        self.CIC_W_z[:,1] = self.frac[:,2]
        self.CIC_W_z[:,0] = self.remfrac[:,2]
        


#-------------------------------------------------------------------    

    def SmoothDen3d_REAL(self, Position_DF):
        """
        Params:
        CIC_Dim: Give the CIC dimesions i.e do you want 
        2D of 3D CIC. 2d for plotting you may want.
        Density: is the grid density rhodm to be calculated.
        """
        

        #-------------------------------------------------------------------------
        @njit
        def SDen3D(ind1, ind2, ind3, cic_w_x, cic_w_y, cic_w_z,
                   masspart, npart, denpart, shiftlist):
                  
            masspart = np.float64(masspart)
            npart = np.int32(npart)
            for i in xrange(npart):
                for j in xrange(len(shiftlist)):
                    i1, i2,  i3 = shiftlist[j]
                    j1 = ind1[i, i1]
                    j2 = ind2[i, i2]
                    j3 = ind3[i, i3]
                    denpart[j1][j2][j3] = (denpart[j1][j2][j3] + 
                                            masspart*cic_w_x[i, i1] * 
                                            cic_w_y[i, i2]*cic_w_z[i, i3]) 
            return denpart
            
        #-------------------------------------------------------------------------
        
        Density = np.zeros((self.nbox, self.nbox, self.nbox), dtype=np.float64)
        self.Setting_3D_CIC_Real(Position_DF)
        print "SET 3D REAL CIC()"

        print "Compute CIC WEIGHTS()"
        
        self.Compute_CIC_weight()
        slist_3D = list(itertools.product([0,1],repeat=3))
        slist_3D = np.asarray(slist_3D)
        partmass, npartT = self.ParticleMass, self.npart

        SDensity = SDen3D(self.j1_ind, self.j2_ind, self.j3_ind, 
                    self.CIC_W_x, self.CIC_W_y, self.CIC_W_z, 
                    partmass, npartT, Density, slist_3D)
                    
        print "Done CIC()"
        return SDensity, self.rhodm_avg
#        return np.ndarray.flatten(SDensity), self.rhodm_avg


    def SmoothDen3d_RSD(self, Position_DF, Velocities_DF):
        """
        Params:
        CIC_Dim: Give the CIC dimesions i.e do you want 
        2D of 3D CIC. 2d for plotting you may want.
        Density: is the grid density rhodm to be calculated.
        """
        

        #-------------------------------------------------------------------------
        @njit
        def SDen3D(ind1, ind2, ind3, cic_w_x, cic_w_y, cic_w_z,
                   masspart, npart, denpart, shiftlist):
                  
            masspart = np.float64(masspart)
            npart = np.int32(npart)
            for i in xrange(npart):
                for j in xrange(len(shiftlist)):
                    i1, i2,  i3 = shiftlist[j]
                    j1 = ind1[i, i1]
                    j2 = ind2[i, i2]
                    j3 = ind3[i, i3]
                    denpart[j1][j2][j3] = (denpart[j1][j2][j3] + 
                                            masspart*cic_w_x[i, i1] * 
                                            cic_w_y[i, i2]*cic_w_z[i, i3]) 
            return denpart
            
        #-------------------------------------------------------------------------
        
        Density = np.zeros((self.nbox, self.nbox, self.nbox), dtype=np.float64)
        self.Setting_3D_CIC_RSD(Position_DF, Velocities_DF)
        print "SET 3D RSD CIC ()"
        print "Compute CIC WEIGHTS()"
        
        self.Compute_CIC_weight()
        slist_3D = list(itertools.product([0,1],repeat=3))
        slist_3D = np.asarray(slist_3D)
        partmass, npartT = self.ParticleMass, self.npart

        SDensity = SDen3D(self.j1_ind, self.j2_ind, self.j3_ind, 
                    self.CIC_W_x, self.CIC_W_y, self.CIC_W_z, 
                    partmass, npartT, Density, slist_3D)
                    
        print "Done CIC()"
        return SDensity, self.rhodm_avg
        #return np.ndarray.flatten(SDensity), self.rhodm_avg


#----------------------------2D Slice CIC--------------------------------------------------------


    def SmoothDen2d(self, Position_DF, Velocities_DF, RSD=False):
    
        """
        Params:
        CIC_Dim: Give the CIC dimesions i.e do you want 
        2D of 3D CIC. 2d for plotting you may want.
        CIC_axis = 1 2D CIC in x-y plane
                  2 2D CIC in x-z plane
                  3 2d CIC in y-z plane
        Density: is the grid density rhodm to be calculated.
        """
 
        #---------------------2D----------------------------------------------

        @njit
        def SDen2D(ind1, ind2, cic_w_1, cic_w_2,
                   masspart, npart, denpart, shiftlist):

            for i in xrange(npart):
                for j in xrange(len(shiftlist)):
                    i1, i2  = shiftlist[j]
                    j1 = ind1[i, i1]
                    j2 = ind2[i, i2]
                    denpart[j1][j2] = denpart[j1][j2] + masspart*cic_w_1[i, i1]*cic_w_2[i, i2]
            return denpart
   
        #-------------------------------------------------------------------------
        
        Density = np.zeros((self.nbox, self.nbox), dtype=np.float64)
        if not RSD:
            self.Setting_2D_CIC_Real(Position_DF)
            del Velocities_DF
            print "SET 3D REAL CIC()\n"
        else:
            self.Setting_2D_CIC_RSD(Position_DF, Velocities_DF)
            print "SET 3D RSD CIC()\n"
            
        self.Compute_CIC_weight()
        print "Compute CIC WEIGHTS()\n"
        
        #-------------------------------------------------------------------------
        
        if self.cic_axis==1:
            slist_2D = list(itertools.product([0,1],repeat=2))
            slist_2D = np.asarray(slist_2D)
            SDensity = SDen2D(self.j1_ind, self.j2_ind, 
                              self.CIC_W_x, self.CIC_W_y, 
                              np.float64(self.ParticleMass), 
                              np.int32(self.npart), Density, slist_2D)
                              
            print "Done CIC()"
            return  SDensity, self.rhodm_avg


        #-------------------------------------------------------------------------

        if self.cic_axis==2:
            
            slist_2D = list(itertools.product([0,1],repeat=2))
            slist_2D = np.asarray(slist_2D)
            SDensity = SDen2D(self.j1_ind, self.j3_ind, 
                    self.CIC_W_x, self.CIC_W_z, 
                    np.float32(self.ParticleMass), 
                    np.int32(self.npart), Density, slist_2D)
                    
            print "Done CIC()"
            return  SDensity, self.rhodm_avg, self.slice_width, self.boxfac, self.slice_pos

        #-------------------------------------------------------------------------

        if self.cic_axis==3:
            
            slist_2D = list(itertools.product([0,1],repeat=2))
            slist_2D = np.asarray(slist_2D)
            SDensity = SDen2D(self.j2_ind, self.j3_ind, 
                            self.CIC_W_y, self.CIC_W_z, 
                            np.float32(self.ParticleMass), 
                            np.int32(self.npart), Density, slist_2D)
                            
            print "Done CIC()"
            return  SDensity, self.rhodm_avg

        #-------------------------------------------------------------------------
        



