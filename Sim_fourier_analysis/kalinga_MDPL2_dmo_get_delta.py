
#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# This routine will compute the density field and auto power 
# spectrum for a large snapshot written into multiple sub files #
#################################################################


import numpy as np
import os
import sys
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl 
import time
from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
import argparse
sys.path.insert(1, '../Gadget2_helper_routines/')
import readsnap
import smoothden_multidark2 as smoothden
import Gen_Power_Sim_multidark2 as Gen_Power_Sim

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")

#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox
def driver_func(dir_name, nbox, Lbox, snapnumber, npart_sim, nfiles):
  

  redshift = np.genfromtxt('snapshot_output_redshift.txt', usecols=1, dtype=np.float64)
  snapshots = np.genfromtxt('snapshot_output_redshift.txt', usecols=2, dtype=np.int32)
  print nbox
  ind = (redshift<=1.0)
  snapshots = snapshots[ind]
 
  for snapnumber in snapshots:
  
    rhodm_main  = np.zeros((nbox, nbox, nbox), dtype=np.float64)
    for i in xrange(nfiles):
      inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%0.3d.%d"%(snapnumber, i)
      print inp_file 
      snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
      Positions_df  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
      CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
      rhodm, rhodm_avg = CIC_object.SmoothDen3d_REAL(Positions_df)
      rhodm_main +=rhodm

    rhodm_main = np.ndarray.flatten(rhodm_main)
    rhodm_main.tofile("../kalinga_rhodm_data/kalinga_MDPL2_rhodm__%dMpc_ngrid_%d_snapID_"\
                "%d_NpartSim_%d_REAL.bin"%(Lbox, np.int32(nbox), snapnumber, npart_sim), format='%lf')
    del rhodm_main 
#------------------------------------------------------------------

def comp_pow(Lbox, nbox, npart_sim):


  redshift = np.genfromtxt('snapshot_output_redshift.txt', usecols=1, dtype=np.float64)
  snapshots = np.genfromtxt('snapshot_output_redshift.txt', usecols=2, dtype=np.int32)

  ind = (redshift<=1.0)
  snapshots = snapshots[ind]
 
  for snapnumber in snapshots:
 
    rhodm =np.fromfile("../kalinga_rhodm_data/kalinga_MDPL2_rhodm__%dMpc_ngrid_%d_snapID_"\
                       "%d_NpartSim_%d_REAL.bin"%(Lbox, np.int32(nbox), snapnumber, npart_sim), count=np.int32(nbox*nbox*nbox))

    print rhodm.shape
    rhodm = rhodm.reshape(nbox, nbox, nbox)
    rhodm = np.asarray(rhodm)
    print rhodm.shape
    print "%2.15f"%(rhodm.sum()/np.float64(nbox)**3.)
    rhodm_Avg = rhodm.sum()/np.float64(nbox)**3.

    rhodm = (rhodm / rhodm_Avg) - 1.
    Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
    Sim_pow.setk()
    Sim_pow.setweights()
    Sim_pow.setdeconv()
    out_file = ("../kalinga_power_data/3dPower_kalinga_MDPL2_%dMpc_ngrid_%d_snapID_"\
                "%d_NpartSim_%d_REAL.txt"%(Lbox, np.int32(nbox), snapnumber, npart_sim))
    Sim_pow.compute_3dpower(rhodm, 28, out_file, True)


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("nbox", type=float, 
    help='Grid size for e.g 400, 1024 or 2048')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 117, 110, 005')
    parser.add_argument("nfiles", type=int, 
    help='Number of subfiles written by Gadget e.g snapshot.{0-4}')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

#------------------------------------------------
    Lbox      = np.int32(args.Lbox)
    nbox      = np.int32(args.nbox)
    snapID    = np.int32(args.snapID)
    nfiles    = np.int32(args.nfiles)
    npart_sim = np.int32(args.npart_sim)
#------------------------------------------------

    dir_name= ("/mnt/data1/sandeep/Kalinga_run_Analysis/%dMpc_%d/"%(Lbox, npart_sim))
    
    if nfiles<=1:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d"%(snapID)
    else:
        inp_file = dir_name + "kalinga_snapdir_seed_1690811/snapshot_%03d.%d"%(snapID, 0)

    print"\n"
    print inp_file 
    print"\n"
    
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    totmdm = snapshot.npartTotal[1]*snapshot.massarr[1]
    rhodm_avg = totmdm/np.float64(nbox)**3.
    print "rhodm_avg: ", rhodm_avg
    print"\n"

    driver_func(dir_name, nbox, Lbox, snapID, npart_sim, nfiles)
    comp_pow(Lbox, nbox, npart_sim)



if __name__ == "__main__":
    main()


