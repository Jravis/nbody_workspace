import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from astropy.cosmology import LambdaCDM
from lmfit.models import LinearModel
import argparse
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')

import readsnap
import Cosmo_Growth_Fac_raw as CGF_raw

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True

#set some plotting parameters
#rcParams['figure.figsize'] = (8,6)
#rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
plt.style.use("classic")

#
bias=1
snapshots = np.array([5, 4, 3, 2, 1, 0])
snapshots_redshift = np.array([0.00, 1.0, 2.0, 3.0, 4.0, 5.0])

CAMB_dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/Cosmology/"
#------------------------------------------------

def plot_norm_pow_REAL(dir_name, snapshot, Lbox, nbox, npart_sim):
    """
    """

    OmegaMatter = snapshot.Omega0
    OmegaLambda = snapshot.OmegaLambda
    H0 = snapshot.HubbleParam*100.
    dirc_name = dir_name +"PostSim_analysis/power_spectrum/power_spec_data_1690811/"
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_REAL.txt"%(Lbox, nbox, 5)
    
    K1 = np.genfromtxt(fin, usecols=0)
    model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                                   Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

    Delta1 = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    err = np.zeros(len(snapshots), dtype=np.float64)
    err = np.genfromtxt(fin, usecols=2)

    factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
    count=0

    for num in snapshots:
        D = model.growth_factor_fnc(snapshots_redshift[count])    
        beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
        factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_REAL.txt"%(Lbox, nbox, num)
        Delta1[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        count+=1


    CAMB_K     = np.genfromtxt(CAMB_dirc_name+"multidark2_pofk_z0.dat", usecols=0)
    CAMB_Pk    = np.genfromtxt(CAMB_dirc_name+"multidark2_pofk_z0.dat", usecols=1)
    Sigma_multidark= 0.8228
    Fname = "multidark2_pofk_z0_norm.dat"
    norm_Pk1 = model.compute_sigma_sqr(CAMB_Pk, CAMB_K, Sigma_multidark, Fname)



    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snapshots)
    cm = plt.get_cmap('hsv')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['*', 'o', 'd', '^', '<', '+']
    for i in xrange(len(snapshots)):
        ax1.plot(np.log10(K1), np.log10(Delta1[i,:]),linestyle='', 
                 alpha=0.9, marker='*', mew=0.5, ms=12, linewidth=1.8,
                 label=r"$\rm{P}^{Z=%0.2f}_{m}(\rm{k})$"%snapshots_redshift[i])
                
    ax1.plot((CAMB_K), np.log10(norm_Pk1),linestyle='-', marker='', 
             mfc='none', mew=1.8, color='k', ms=10, linewidth=2.5, 
             label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

    ax1.set_title(r"$%d\rm{Mpc},\rm{Ngrid}:1024, REAL$"%Lbox, fontsize=16)
    ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})$', fontsize=20)
    ax1.set_ylabel(r'$\rm{log}_{10}(\Delta^2)$', fontsize=20)
    ax1.set_ylim(-4, 3.5)
    ax1.set_xlim(-2.5, 1.)

    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=2, fontsize=16, loc=4)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    plot_name = dir_name +"PostSim_analysis/power_spectrum/"
    fout = plot_name+"MultiSnap_Common_PowSpec_%dMpc_%d_REAL.png"%(Lbox, npart_sim)
    fig.savefig(fout, bbox_inches='tight')


def plot_norm_pow_RSD(dir_name, snapshot, Lbox, nbox, npart_sim):
    """
    """

    OmegaMatter = snapshot.Omega0
    OmegaLambda = snapshot.OmegaLambda
    H0 = snapshot.HubbleParam*100.
    dirc_name = dir_name +"PostSim_analysis/power_spectrum/power_spec_data_1690811/"
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, 5)
    
    K1 = np.genfromtxt(fin, usecols=0)
    model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                                   Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

    Delta1 = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    err = np.zeros(len(snapshots), dtype=np.float64)
    err = np.genfromtxt(fin, usecols=2)

    factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
    count=0

    for num in snapshots:
        D = model.growth_factor_fnc(snapshots_redshift[count])    
        beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
        factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, num)
        Delta1[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        count+=1


    CAMB_K     = np.genfromtxt(CAMB_dirc_name+"multidark2_pofk_z0.dat", usecols=0)
    CAMB_Pk    = np.genfromtxt(CAMB_dirc_name+"multidark2_pofk_z0.dat", usecols=1)
    Sigma_multidark= 0.8228
    Fname = "multidark2_pofk_z0_norm.dat"
    norm_Pk1 = model.compute_sigma_sqr(CAMB_Pk, CAMB_K, Sigma_multidark, Fname)



    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snapshots)
    cm = plt.get_cmap('hsv')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['*', 'o', 'd', '^', '<', '+']
    for i in xrange(len(snapshots)):
        ax1.plot(np.log10(K1), np.log10(Delta1[i,:]),linestyle='', 
                 alpha=0.9, marker='*', mew=0.5, ms=12, linewidth=1.8,
                 label=r"$\rm{P}^{Z=%0.2f}_{m}(\rm{k})$"%snapshots_redshift[i])
                
    ax1.plot((CAMB_K), np.log10(norm_Pk1),linestyle='-', marker='', 
             mfc='none', mew=1.8, color='k', ms=10, linewidth=2.5, 
             label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

    ax1.set_title(r"$%d\rm{Mpc},\rm{Ngrid}:1024, RSD$"%Lbox, fontsize=16)
    ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})$', fontsize=20)
    ax1.set_ylabel(r'$\rm{log}_{10}(\Delta^2)$', fontsize=20)
    ax1.set_ylim(-4, 3.5)
    ax1.set_xlim(-2.5, 1.)

    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=2, fontsize=16, loc=4)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    plot_name = dir_name +"PostSim_analysis/power_spectrum/"
    fout = plot_name+"MultiSnap_Common_PowSpec_%dMpc_%d_RSD.png"%(Lbox, npart_sim)
    fig.savefig(fout, bbox_inches='tight')


#------------------------------------------------------------------

def plot_norm_pow_ratio(dir_name, snapshot, Lbox, nbox, npart_sim):
    """
    """

    OmegaMatter = snapshot.Omega0
    OmegaLambda = snapshot.OmegaLambda
    H0 = snapshot.HubbleParam*100.
    dirc_name = dir_name +"PostSim_analysis/power_spectrum/power_spec_data_1690811/"
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, 5)
    
    K1 = np.genfromtxt(fin, usecols=0)
    model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                                   Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

    Delta1 = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    Delta1_RSD = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    err = np.zeros(len(snapshots), dtype=np.float64)
    err = np.genfromtxt(fin, usecols=2)

    factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
    count=0

    for num in snapshots:
        D = model.growth_factor_fnc(snapshots_redshift[count])    
        beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
        factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_REAL.txt"%(Lbox, nbox, num)
        Delta1[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, num)
        Delta1_RSD[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        count+=1

    fig= plt.figure(1, figsize=(10, 8))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])

    ax1.errorbar(K1, Delta1_RSD[0,:]/Delta1[0,:], yerr=err, fmt='o', ecolor='k', 
                 elinewidth=2.5, capsize=8, mec='blue',
                 ms=8, mfc='none', mew=2.0, label=r"Z=%0.1f"%snapshots_redshift[0])

    ax1.axhline(y= (Delta1_RSD[0,:]/Delta1[0,:])[0], linestyle='--', linewidth=2.5, 
                color='r', label=r"$\rm{f}_{\rm{fit}}$:%0.4f"%(Delta1_RSD[0,:]/Delta1[0,:])[0])
    ax1.axhline(y=factor_0[0], linestyle='--', linewidth=2.5, color='g', 
                label=r"$\rm{f}_{\rm{theory}}$:%0.4f"%factor_0[0])

    ax1.set_title(r"$%d\rm{Mpc},\rm{Ngrid}:1024,$"%Lbox, fontsize=16)
    ax1.set_xlabel(r'$\rm{k}[\rm{h}^{-1} \rm{Mpc}]$', fontsize=20)
    ax1.set_ylabel(r'$\rm{P}_{s}(\rm{k})/\rm{P}_{r}(\rm{k}) $', fontsize=20)
    #ax1.set_ylim(-4, 3.5)
    #ax1.set_xlim(-2.5, 2)
    ax1.set_ylim(0.2, 10**0.6)
    ax1.set_xscale("log")
    ax1.set_yscale("log")

    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3)
    ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    
    plot_name = dir_name +"PostSim_analysis/power_spectrum/"
    fout = plot_name+"PowSpec_Ratio_%dMpc_%d.png"%(Lbox, npart_sim)
    fig.savefig(fout, bbox_inches='tight')


#------------------------------------------------------------------

def plot_norm_pow_ratio_all(dir_name, snapshot, Lbox, nbox, npart_sim):
    """
    """

    OmegaMatter = snapshot.Omega0
    OmegaLambda = snapshot.OmegaLambda
    H0 = snapshot.HubbleParam*100.
    dirc_name = dir_name +"PostSim_analysis/power_spectrum/power_spec_data_1690811/"
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, 5)
    
    K1 = np.genfromtxt(fin, usecols=0)
    model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                                   Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

    Delta1 = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    Delta1_RSD = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
    err = np.zeros(len(snapshots), dtype=np.float64)
    err = np.genfromtxt(fin, usecols=2)

    factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
    count=0

    for num in snapshots:
        D = model.growth_factor_fnc(snapshots_redshift[count])    
        beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
        factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_REAL.txt"%(Lbox, nbox, num)
        Delta1[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, num)
        Delta1_RSD[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
        count+=1

    count = 0
    fig= plt.figure(3, figsize=(22, 18))
    gs = gridspec.GridSpec(3, 3, hspace=0.4, wspace=0.4)

    for i in xrange(0, 3):
        for j in xrange(0, 2):
            ax1 = plt.subplot(gs[i, j])
            ax1.errorbar(K1, Delta1_RSD[count,:]/Delta1[count,:], yerr=err, fmt='o', ecolor='k', 
                         elinewidth=2.5, capsize=8, mec='blue',
                         ms=8, mfc='none', mew=2.0, label=r"")
                         
            ax1.axhline(y= (Delta1_RSD[count,:]/Delta1[count,:])[0], linestyle='--', linewidth=2.5, 
                        color='r', label=r"$\rm{f}_{\rm{fit}}$:%0.4f"%(Delta1_RSD[count,:]/Delta1[count,:])[0])
                        
            ax1.axhline(y=factor_0[count], linestyle='--', linewidth=2.5, color='g', 
                        label=r"$\rm{f}_{\rm{theory}}$:%0.4f"%factor_0[count])

            ax1.set_title(r"$z=%0.2f$"%snapshots_redshift[count], fontsize=16)
            ax1.set_xscale("log")
            ax1.set_yscale("log")
            ax1.set_xlabel(r'$\rm{k}(\rm{h}/\rm{Mpc})$', fontsize=22)
            ax1.set_ylabel(r'$\Delta^2_{\rm{s}}/\Delta^2_{\rm{r}}$', fontsize=22)

            ax1.set_ylim(10**-1.5, 10**0.6)
            ax1.minorticks_on()
            ax1.legend(frameon=False, loc=3, ncol=1, fontsize=20 )
            ax1.tick_params(axis='both', which='minor', length=5,
                                  width=2, labelsize=16)
            ax1.tick_params(axis='both', which='major', length=8,
                                   width=2, labelsize=16)
            ax1.spines['bottom'].set_linewidth(1.8)
            ax1.spines['left'].set_linewidth(1.8)
            ax1.spines['top'].set_linewidth(1.8)
            ax1.spines['right'].set_linewidth(1.8)
            count+=1
    plt.tight_layout()
    plot_name = dir_name +"PostSim_analysis/power_spectrum/"
    fout = plot_name+"PowSpec_Ratio_Diff_Snapshot_%dMpc_%d_fit.png"%(Lbox, npart_sim)
    fig.savefig(fout, bbox_inches='tight')

#------------------------------------------------------------------

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("nbox", type=float, 
    help='Grid size for e.g 400, 1024 or 2048')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
# First analysis
#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox
    Lbox = args.Lbox
    nbox = args.nbox
    npart_sim = args.npart_sim
#------------------------------------------------

    dir_name= ("/mnt/data1/sandeep/New_Gadget2_run/"\
                "%dMpc_%d/"% (Lbox, npart_sim))
    inp_file = dir_name + "snapdir_seed_1690811/snapshot_%03d.%d"%(5, 0)

    print"\n"
    print inp_file 
    print"\n"
    print "+++++++++++++++++ Gadget header ++++++++++++++++++++++++++\n"
    
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    plot_norm_pow_REAL(dir_name, snapshot, Lbox, nbox, npart_sim)
    plot_norm_pow_RSD(dir_name, snapshot, Lbox, nbox, npart_sim)
    plot_norm_pow_ratio(dir_name, snapshot, Lbox, nbox, npart_sim)
    plot_norm_pow_ratio_all(dir_name, snapshot, Lbox, nbox, npart_sim)



if __name__ == "__main__":
    main()












