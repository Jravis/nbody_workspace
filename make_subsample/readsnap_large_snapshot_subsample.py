#############################################################################
#Copyright (c) 2019, Sandeep Rana                                           #
                                                                            #
# This routine will make number of subsamples you want drawn from           #
# the snapshot.                                                             #
# It use multiprocessing and can make multiple subsample in parallel using  #
# different seed or single process with single seed.                        #
# This routine is for large snapshot files written into multiple sub files. #
                                                                            #
#############################################################################



import numpy as np
import pandas as pd
import os
import sys
import argparse
from astropy.cosmology import LambdaCDM
from astropy import constants as const
from multiprocessing import Process
import astropy.units as u
sys.path.insert(1, '../Gadget_helper_routines/')
import readsnap





def driver_func(dir_name, Samp_Size, snapID, nsub, NpartSim, seed, RSD=False):
    
    inp_file = dir_name +"snapshot_%03d.0"%snapID

    print "+++++++++++++++++ Gadget header ++++++++++++++++++++++++++\n"
    
    snapshot        = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    
    Lbox      = snapshot.BoxSize/1000.
    L_boxhalf = Lbox/2.
    scale_fac = 1./(1.+snapshot.redshift)
    kpc_to_Mpc = 1./1000.
    vel_conv_factor = 1./np.sqrt(scale_fac)

    cosmo  = LambdaCDM(H0= 100.*snapshot.HubbleParam, 
                       Om0 = snapshot.Omega0,
                       Ode0= snapshot.OmegaLambda,
                       Tcmb0=0.)
                        
    
    H_z = cosmo.H(snapshot.redshift).value
        
    snapfname = ("/mnt/data1/sandeep/Kalinga_run_Analysis/"\
                 "kalinga_velocity_correlation/sub_sample_data_%dMpc_%d/%d_sample/"\
                 "RSD/snapshot_%d_data/subsamp_param_%d.dat"%(Lbox, NpartSim, 
                 Samp_Size, snapID, snapID))
               
    np.savetxt(snapfname, zip([3], [Samp_Size], [50], 
               [snapshot.npartTotal[1]], [snapID], [scale_fac], 
               [snapshot.HubbleParam*100], [Lbox], [L_boxhalf]), 
               fmt="%d %d %d %d %d %lf %lf %lf %lf") 

    #--- since gadget2 internal position units are in kpc h^-1

    conv_gadget_units = 1000.*snapshot.HubbleParam
    boost_factor = np.sqrt(1.+ snapshot.redshift)*conv_gadget_units/H_z
            
    #To get comoving Peculiar velocities from Gadget 
    #velocities multiply by this factor
#---------------------------------------------------------------------------------    

    Numpart_per_file= np.zeros(snapshot.num_files, dtype=np.int64)
    
    file_index = np.arange(snapshot.num_files, dtype=np.int32)
    file_count=0
    for i in xrange(snapshot.num_files):
    
        inp_file = dir_name +"snapshot_%03d.%d"%(snapID, i)
        print inp_file
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
        Numpart_per_file[i] = snapshot.npart[1]
        file_count+=1
   
   
    if np.sum(Numpart_per_file)!=snapshot.npartTotal[1]:
     raise ValueError('Total number of particles does not match')
    else:
        print("="*20)
        print ('Total number of particles does match')

#---------------------------------------------------------------------------------    

    npart_cumsum = np.cumsum(Numpart_per_file)
    npart_cumsum1 = npart_cumsum-Numpart_per_file
 
    nfile_index= np.arange(file_count)
   
    print'\n'
    print 'Samp_Size: ', Samp_Size, 'n_sub: ', nsub, 'SnapID: ', snapID
    print ("Npart_Sim %0.1f\n"%(NpartSim))
    print'\n'
    
    rand_index    = np.zeros(Samp_Size, dtype = np.int32)
    part_per_file = np.zeros(Samp_Size, dtype = np.int32)
    file_numbr    = np.zeros(Samp_Size, dtype = np.int32)
    select_part_index = np.zeros(Samp_Size, dtype = np.int32)
    np.random.seed(seed)


#    for j in xrange(0, nsub):
    for j in xrange(nsub, nsub+2):
        
        count=0
        halo_pos=np.zeros((Samp_Size, 3), dtype=np.float64)
        halo_vel=np.zeros((Samp_Size, 3), dtype=np.float64)
        indices = np.random.randint(snapshot.npartTotal[1], size=Samp_Size)
        for i in indices:
            tmp = (i<=npart_cumsum)
            file_num = (nfile_index[tmp])[0] 
            part_index =  np.uint64(abs(npart_cumsum1[tmp][0] - i))
            rand_index[count]   =  i
            part_per_file[count]= (Numpart_per_file[tmp])[0]
            file_numbr[count] = file_num
            select_part_index[count] = part_index
            
            inp_file = dir_name +"snapshot_%03d.%d"%(snapID, file_num)
#            print inp_file
            snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=False)
            part_index = np.random.randint(snapshot.npart[1], size=1)[0]

            f = open(inp_file,'rb')    # rb is for read binary
            
            offset = 4+256+4       
            f.seek(offset, os.SEEK_CUR)
            blocksize = np.fromfile(f,dtype=np.int32,count=1)

            offset=4*3*((part_index)-1)
            f.seek(int(offset), os.SEEK_CUR)
            dt = np.dtype((np.float32,3))  # Positions are in float not double
            Pos = np.fromfile(f,dtype=dt,count=1)
            
            tmp = snapshot.npart[1] - (part_index)
            offset=4*3*(tmp)
            f.seek(int(offset), os.SEEK_CUR)
            blk_check = np.fromfile(f,dtype=np.int32,count=1)
            if blocksize[0]!=blk_check[0]:
                raise ValueError("I/O:ERRORBLOCKS NOT MATCHING")
            
            #--------------------------------------------------------------
            
            #vel block check
            blocksize = np.fromfile(f,dtype=np.int32,count=1)
            offset=4*3*(part_index-1)
            f.seek(int(offset), os.SEEK_CUR)
            dt = np.dtype((np.float32,3))  # velocities are in float not double
            Vel = np.fromfile(f,dtype=dt,count=1)
            
            tmp = snapshot.npart[1] - part_index
            offset=4*3*(tmp)
            f.seek(offset, os.SEEK_CUR)
            blk_check = np.fromfile(f,dtype=np.int32,count=1)

            if blocksize[0]!=blk_check[0]:
                raise ValueError("I/O:ERRORBLOCKS NOT MATCHING")
             
            if RSD:
                print "redshfhift: %0.4e Hubble parameter H: %f  factor: %f"%(snapshot.redshift, H_z, boost_factor) 
                print "Redshift mapping in z direction where boxsize:", snapshot.BoxSize
                RSD_Pos = Pos[0, 2] + boost_factor * Vel[0, 2]
                Pos[0, 2] = RSD_Pos%snapshot.BoxSize 

            # Converting comoving positions into Mpc h^-1 physical
            #sqrt(a) v_G is the peculiar velocities in physical v_G/sqrt(a) Peculiar velocities in comoving

            halo_pos[count, 0] = np.float64(Pos[0, 0]*kpc_to_Mpc)
            halo_pos[count, 1] = np.float64(Pos[0, 1]*kpc_to_Mpc)
            halo_pos[count, 2] = np.float64(Pos[0, 2]*kpc_to_Mpc)

            halo_vel[count, 0] = np.float64(Vel[0, 0]*vel_conv_factor)
            halo_vel[count, 1] = np.float64(Vel[0, 1]*vel_conv_factor)
            halo_vel[count, 2] = np.float64(Vel[0, 2]*vel_conv_factor)

            count+=1
            f.close()
            
        
        halo_pos = np.ndarray.flatten(halo_pos)  
        halo_vel = np.ndarray.flatten(halo_vel)  
        
        temp = np.array([snapID], dtype=np.float64)
        file_data = np.concatenate((temp, halo_pos, halo_vel, temp))    
        snapfname = ("/mnt/data1/sandeep/Kalinga_run_Analysis/"\
                  "kalinga_velocity_correlation/sub_sample_data_%dMpc_%d/%d_sample/"\
                  "RSD/snapshot_%d_data/KMDPL2_SubSam_%d_%d_%d_phy.bin"%(Lbox, NpartSim, 
                   Samp_Size, snapID, snapID, Samp_Size, j))
        print snapfname
        file_data.tofile(snapfname, format="%lf")

        
def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize 1024 h^-1 Mpc')
    parser.add_argument("NpartSim", type=int, 
    help='1024^3')
    parser.add_argument("Samp_Size", type=int, 
    help='400000 sub sample size')
    parser.add_argument("n_sample", type=int, 
    help='50 or 100')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 130 for multidark2')
#
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

    Lbox      = args.Lbox
    NpartSim  = args.NpartSim
    Samp_Size = args.Samp_Size
    n_sample  = args.n_sample
    snapID    = args.snapID
    
    # Make changes for the directory where snapshot files are
    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)


    max_core =25  # number of process to initiate in parallel

    nsamp = np.int32(np.arange(0, n_sample, 2))
    print nsamp
    seed = np.array([1000003, 1000009, 1000033, 1000081, 1000099, 1000231, 1000669,
                     1000999, 1002887, 1003001, 1005581, 1005679, 1006301, 1008001,
                     1010203, 1011001, 1012573, 1015423, 1023467, 1023487, 1023557,
                     1023571, 1023947, 1024651, 1025327])
     
    strn = [] 
    for i in xrange(0, max_core):
        s = 'Cell_Count%d' % (i+1)
        strn.append(s)
    print strn
    for i in xrange(len(strn)):
        strn[i] = Process(target=driver_func, args=(dir_name, Samp_Size, snapID, nsamp[i], NpartSim, seed[i], True))
        strn[i].start()
    for i in xrange(len(strn)):
        strn[i].join()
    

if __name__ == "__main__":
    main()


