///////////////////////////////////////////////////////////////////////
//                                                                   //
// Copyright 2019 Sandeep Rana and Nishikanta Khandai                //
//                                                                   //
// This routine will make number of subsamples you want drawn from   //             
// your box. This code is for small Gadget2 runs like 512^3          //
// specific to xanadu 512GB ram if you have bigger memory JBOD you   //
// can use this for bigger simulations also or else use python code  //
// for making subsamples.                                            //                                           
///////////////////////////////////////////////////////////////////////

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<cmath>
#include<string>
#include <algorithm> 
#include <bits/stdc++.h> 
#include <sstream> // for ostringstream
using namespace std;


struct allglobal{
  long long TotNpart[6];           //Total Number of Particles in Simulation
  long long TotNpart_check[6];
} All;

// For Dark Matter only simulations
struct io_header{
      int npart[6];                 /*!< number of particles of each type in this file */
      double mass[6];               /*!< mass of particles of each type. If 0, then the masses are explicitly
                                      stored in the mass-block of the snapshot file, otherwise they are omitted */
      double time;                  /*!< time of snapshot file */
      double redshift;              /*!< redshift of snapshot file */
      int flag_sfr;                 /*!< flags whether the simulation was including star formation */
      int flag_feedback;            /*!< flags whether feedback was included (obsolete) */
      unsigned int npartTotal[6];   /*!< total number of particles of each type in this snapshot. This can be
                                       different from npart if one is dealing with a multi-file snapshot. */
      int flag_cooling;             /*!< flags whether cooling was included  */
      int num_files;                /*!< number of files in multi-file snapshot */
      double BoxSize;               /*!< box-size of simulation in case periodic boundaries were used */
      double Omega0;                /*!< matter density in units of critical density */
      double OmegaLambda;           /*!< cosmological constant parameter */
      double HubbleParam;           /*!< Hubble parameter in units of 100 km/sec/Mpc */
      int flag_stellarage;          /*!< flags whether the file contains formation times of star particles */
      int flag_metals;              /*!< flags whether the file contains metallicity values for gas and star
                                       particles */
      unsigned int npartTotalHighWord[6];   /*!< High word of the total number of particles of each type */
      int flag_entropy_instead_u;   /*!< flags that IC-file contains entropy instead of u */
      int flag_doubleprecision;     /*!< flags that snapshot contains double-precision instead of single precision */

      int flag_ic_info;             /*!< flag to inform whether IC files are generated with ordinary Zeldovich approximation,
                                         or whether they ocontains 2nd order lagrangian perturbation theory initial conditions.
                                         For snapshots files, the value informs whether the simulation was evolved from
                                         Zeldoch or 2lpt ICs. Encoding is as follows:
                                            FLAG_ZELDOVICH_ICS     (1)   - IC file based on Zeldovich
                                            FLAG_SECOND_ORDER_ICS  (2)   - Special IC-file containing 2lpt masses
                                            FLAG_EVOLVED_ZELDOVICH (3)   - snapshot evolved from Zeldovich ICs
                                            FLAG_EVOLVED_2LPT      (4)   - snapshot evolved from 2lpt ICs
                                            FLAG_NORMALICS_2LPT    (5)   - standard gadget file format with 2lpt ICs
                                         All other values, including 0 are interpreted as "don't know" for backwards compatability.
                                     */
      float lpt_scalingfactor;      /*!< scaling factor for 2lpt initial conditions */

      char fill[48];                /*!< fills to 256 Bytes */

}header;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++==

char *snapfname;
double *rhodm;
float *posdm;
float *veldm;
float *mdm;
int ndim, idm, ndm_check, ndm;


double *r,*v;
double dr[3], dv[3];
double *rbin,*vol;

double *pairs,*pairvel;
double *pairvdisp;
double box,boxhalf;
int samp_size,n_subsamples;

double H_z(double z_epoch, double small_h, double Omega_matter, 
           double Omega_not, double Omega_Lambda);
 
void  do_rsd_scaling();

//=======================================================
class Gadget2readsnap{
  public: 
    char *fname;
    int filenumber;
    int snapid;
    void read_snapshot_header_single();
    void print_snapshot_header();
    void initialise();
    void read_pos_mass(int fnumber);
};    


//=======================================================


int main(int argc, char **argv){
  
  int snapid, Lbox, nfiles, NpartSim;
  int  i,j,k;
  int ii, RSD_key=0;

  if(argc != 8){
    cout<< "\n./a.out  <snapfile>  [snapid]   [Lbox]  [NpartSim] [nfiles] [samp_size] [n_subsamples]"<<"\n";
    cout<< "\n./a.out ../snapshot     17      1024      512        4         400000       30 " <<"\n";
    return(-1);
  }//if
  
  
  snapid       = atoi(argv[2]);   
  Lbox         = atoi(argv[3]);  
  NpartSim     = atoi(argv[4]);  
  nfiles       = atoi(argv[5]);
  samp_size    = atoi(argv[6]); // Size of random subsample like 400000 or 800000 etc
  n_subsamples = atoi(argv[7]); // How many random subsample is to be taken


 // Change here the directory name where your snapshot files are
  snapfname = (char *)malloc(sizeof(char)*200);
  sprintf(snapfname,"/mnt/data1/sandeep/Kalinga_run_Analysis/"
          "%dMpc_%d/kalinga_snapdir_seed_1690811/%s",Lbox, NpartSim, argv[1]);

  cout<< snapfname<<"\n"; 

  // Gadget2 readsnap Class initiation

  Gadget2readsnap Snapshot;
  Snapshot.snapid      = snapid;
  Snapshot.filenumber = nfiles;
  Snapshot.fname      =  snapfname;
  Snapshot.read_snapshot_header_single();
  Snapshot.print_snapshot_header();
  Snapshot.initialise();
 
  int ifile;
  if(nfiles == 1)
     Snapshot.read_pos_mass(nfiles);
   else if(nfiles > 1)
     for(ifile = 0; ifile < nfiles; ifile++)
       Snapshot.read_pos_mass(ifile);
   else {
     printf("nfiles=%d can only be >= 1 ... QUITTING \n", nfiles);
     return(-1);
   }

   ndm = header.npartTotal[1];

   printf("ndm=%d \n",ndm);
   if( ndm != ndm_check ) {
     printf("MISMATCH ... ndm_check=%d ... QUITTING \n", ndm_check);
     return(-1);
   }//
        
   // Assigning the position and velocity array for random subsample

   r = new double [samp_size*ndim];
   v = new double [samp_size*ndim];

   for(i=0;i<samp_size;i++)
     for(ii=0;ii<ndim;ii++){
           r[ii+ndim*i] = 0.;
           v[ii+ndim*i] = 0.;
     }
    
   //------------------------------------------------------------------
       
   FILE *param_file;
   
   //check if wanted to do rsd scaling 

   cout<<"Do you want RSD mapping if yes type 1\n";
   cin>> RSD_key;
   RSD_key = (int) RSD_key;
   
   if (RSD_key==1)
     do_rsd_scaling();
     

   // I keep RSD and Real space subsample in a different folders under different name
   // Change here for the directory and filename where you want to keep subsamples.
   // The subsamp_param*.dat is the file contains the parameter used in creating this 
   // subsample file and it reside into the same directory as subsamples.

   if (RSD_key==1){
        sprintf(snapfname,"/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
           "sub_sample_data_%dMpc_%d/%d_sample/RSD/snapshot_%d_data"
           "/subsamp_param_%d.dat",Lbox, NpartSim, samp_size, snapid, snapid);
    }
    else{
        sprintf(snapfname,"/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
           "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data"
           "/subsamp_param_%d.dat",Lbox, NpartSim, samp_size, snapid, snapid);
   }
   
   cout<< snapfname<<"\n";
   double scale_factor = (double) 1./(1.+header.redshift);
   double HubbleParam  = (double) header.HubbleParam;
   
   param_file = fopen(snapfname, "w");

   box = ((double)header.BoxSize/1000.);   // In Gadget 2 header Box size is written in the units of kpc h^-1
                                           // One can use Lbox directly here so if you want you can change it here
   boxhalf = box/2.0;

   cout <<"box= "<< box <<" boxhalf= " << boxhalf <<"\n";

   fprintf(param_file,"%d %d %d %d %d %lf %lf %lf %lf\n", 
            ndim, samp_size, n_subsamples,ndm,snapid,scale_factor, 
            HubbleParam, box, boxhalf);

   fclose(param_file);

//  Setting mersine twister Psuedo Random number gen

   const int range_from = 0;
   const int range_to = header.npartTotal[1];
   seed_seq                            seed{2593697};
   mt19937_64                          gen(seed);
   uniform_real_distribution<double>  dist(range_from, range_to);
   FILE *subsam;

//--------------------------------------------------------------------------------

   for(k=0;k<n_subsamples;k++){
   
     if (RSD_key==1){
       sprintf(snapfname,"/mnt/data1/sandeep/Kalinga_run_Analysis/"
       "kalinga_velocity_correlation/sub_sample_data_%dMpc_%d/%d_sample/"
       "RSD/snapshot_%d_data/KMDPL2_SubSam_%d_%d_%d_phy.bin",Lbox, NpartSim, 
       samp_size, snapid, snapid, samp_size, k );
     }
     else{
       sprintf(snapfname,"/mnt/data1/sandeep/Kalinga_run_Analysis/"
       "kalinga_velocity_correlation/sub_sample_data_%dMpc_%d/%d_sample/"
       "REAL/snapshot_%d_data/KMDPL2_SubSam_%d_%d_%d_phy.bin",Lbox, NpartSim, 
       samp_size, snapid, snapid, samp_size, k );
    }
    
    subsam = fopen(snapfname, "w");
    cout << "While making sub sample Gadget2 positions are converted Kpc h^-1 comoving to Mpc h^-1 "<< "\n";
    cout << "While making sub sample Gadget2 velocities are converted to comoving velocity "<< "\n";


    for(i=0;i<samp_size;i++){
      j = (int)dist(gen);
      for(ii=0;ii<ndim;ii++){
        r[ii+ndim*i] = (double) posdm[ii+ndim*j]/1000.;     // Converting comoving positions into Mpc h^-1 
		v[ii+ndim*i] = (double) veldm[ii+ndim*j]/sqrt(scale_factor); // sqrt(a) v_G is the peculiar velocities 
                                                                     // in physical v_G/sqrt(a) Peculiar velocities in comoving
      }//for ii...all ndim  directions
    }//for i...samp_size
    
    snapid = (double) snapid;
    fwrite(&snapid,sizeof(double),1,subsam);
    fwrite(r,sizeof(double),samp_size*ndim,subsam);
    fwrite(v,sizeof(double),samp_size*ndim,subsam);
    fwrite(&snapid,sizeof(double),1,subsam);
    fclose(subsam);

  }//for k
  
  delete r;
  delete v;
  //dm
  free(posdm);
  free(veldm);
  free(mdm);
  free(snapfname);
  return(0);
    
}//main

//------------------------------------------------------------------------------


double H_z(double z_epoch, double small_h, double Omega_matter, 
           double Omega_not, double Omega_Lambda){
           
    double E_z, tmp, tmp1;
    
    tmp = (1. - Omega_not) * (1. + z_epoch)*(1. + z_epoch);
    tmp1 = Omega_matter * (1. + z_epoch);
    E_z = sqrt(tmp1+tmp +Omega_Lambda);
    tmp = (small_h * 100. * E_z);
    return tmp; //km s^-1 Mpc^-1
}



void Gadget2readsnap::read_snapshot_header_single(){

    int header_size, header_size_check;
    char filename[400];
    FILE *snapfile;

    if(filenumber == 1)
      sprintf(filename,"%s_%03d",fname,snapid);
    else  
      sprintf(filename,"%s_%03d.%d",fname,snapid,0);
    snapfile = fopen(filename,"r");
//-------Read the header from a snapshot file ----------------
    fread(&header_size,sizeof(int),1,snapfile);
    printf("\n\nsizeof(header)=%d\n",header_size);

    fread(&header,sizeof(header),1,snapfile);
    printf("Read header from %s\n",filename);

    fread(&header_size_check,sizeof(int),1,snapfile);
    if(header_size_check !=header_size)
      exit(-1);
    printf("CHECKED: sizeof(header)=%d \n\n",header_size_check);

    if(header.num_files != filenumber){
      printf("MISMATCH ... header.num_files=%d nfiles=%d ...TRY AGAIN ...QUITTING \n ",header.num_files, filenumber);
      exit(-1);
    }
    fclose(snapfile);
}

void Gadget2readsnap::print_snapshot_header(){

  int i;
  for(i=0;i<6;i++)
    All.TotNpart[i]=0;
  for(i = 0;i<6;i++){
    All.TotNpart[i] += header.npartTotal[i];
    All.TotNpart[i] += (((long long) header.npartTotalHighWord[i]) << 32);
  }//for...i
  for(i=0;i<6;i++){
    printf("mass[%d]=%2.14f\n",i,header.mass[i]);
    printf("npart[%d]=%d \n",i,header.npart[i]);
    printf("TotNpart[%d]=%lld\n\n",i,All.TotNpart[i]);
  }//
  printf("Scale Fac=%2.14f Redshift=%2.14f \n",
     header.time,header.redshift);
  printf("BoxSize=%2.2fh-1kpc number snapshot files=%d\n",
     header.BoxSize,header.num_files);
  printf("Omega0=%2.5f OmegaLambda=%2.5f h=%2.5f\n",
     header.Omega0,header.OmegaLambda,header.HubbleParam);
  printf("flag_sfr=%d flag_feedback=%d flag_cooling=%d \n",
     header.flag_sfr,header.flag_feedback,header.flag_cooling);
  printf("flag_doubleprecision=%d \n",header.flag_doubleprecision);	
  printf("sizeof(unsigned long long)=%lu \n",sizeof(unsigned long long));
  printf("sizeof(long double)=%lu \n\n",sizeof(long double));

}//print_header()


void Gadget2readsnap::initialise(){
    
    ndim=3;
    posdm = (float *)malloc(sizeof(float)*ndim*All.TotNpart[1]);
    veldm = (float *)malloc(sizeof(float)*ndim*All.TotNpart[1]);
    mdm = (float *)malloc(sizeof(float)*All.TotNpart[1]);
    long long i;
    for(i=0;i <  All.TotNpart[1];i++)
      mdm[i] = header.mass[1];
    cout << "Initialised DM mass array" << "\n";
    idm  = 0;
    ndm_check = 0;


}//void initialise()
  

void Gadget2readsnap::read_pos_mass(int fnumber){

    int i;
    char filename[300];
    FILE *snapfile;

    int offset;
    int header_size,header_size_check;
    int nblock_start,nblock_end,nblock_check;

    struct io_header this_header;

    if(header.num_files == 1)
      sprintf(filename,"%s_%03d",fname,snapid);
    else
      sprintf(filename,"%s_%03d.%d",fname,snapid,fnumber);

    snapfile = fopen(filename,"r");
    printf("\n\nREADING FILE: %s \n",filename);

   //-------Read the header from a snapshot file ----------------
    fread(&header_size,sizeof(int),1,snapfile);
    fread(&this_header,sizeof(header),1,snapfile);
    fread(&header_size_check,sizeof(int),1,snapfile);
    if(header_size_check !=header_size)
      exit(-1);

    //------Positions--------
    printf("\nPositions: \n");
    offset = 0;
    for(i=0;i<6;i++){
      offset += this_header.npart[i];
      printf("offset=%d \n",offset);
    }
    offset*=ndim;

    nblock_check = (offset*sizeof(float));
    printf("DM Particles: %d\n",this_header.npart[1]);
    
    fread(&nblock_start,sizeof(int),1,snapfile);
    fread(&posdm[0+ndim*idm],sizeof(float), this_header.npart[1]*ndim, snapfile);
    fread(&nblock_end, sizeof(int),1, snapfile);

    printf("POS: filenr=%d nblock_start=%d nlock_end=%d nblock_check=%d \n",
    filenumber,nblock_start,nblock_end,nblock_check);
    if(nblock_start != nblock_end){
      printf("blocks do not match !!!... QUITTING\n");
      exit(-1);
    }
    
    //------velocities--------

    fread(&nblock_start,sizeof(int),1,snapfile);
    fread(&veldm[0+ndim*idm],sizeof(float),this_header.npart[1]*ndim,snapfile);
    fread(&nblock_end,sizeof(int),1,snapfile);
    printf("vel: filenr=%d nblock_start=%d nlock_end=%d nblock_check=%d \n",
    filenumber,nblock_start,nblock_end,nblock_check);
    if(nblock_start != nblock_end){
      printf("blocks do not match !!!... QUITTING\n");
      exit(-1);
    }

    fclose(snapfile);
    idm += this_header.npart[1];
    ndm_check = idm;

}//read_properties()


void  do_rsd_scaling(){

   float  conv_gadget_units, boost_factor, Omega0, Hz;
   unsigned int nn;
   float temp_pos=0.0, BOX_SIZE;
   Omega0 = header.Omega0+header.OmegaLambda;
   Hz = H_z(header.redshift, header.HubbleParam, header.Omega0, Omega0, header.OmegaLambda);
 
   conv_gadget_units = 1000.* header.HubbleParam;
   boost_factor = sqrt(1.+ header.redshift)*conv_gadget_units/Hz;
            
   printf("redshfhift: %0.4e\n",header.redshift);
   printf("Hubble parameter H: %f\n", Hz);
   printf("factor: %f \n", boost_factor);
   cout<< "Redshift mapping in z direction\n";
   printf("Ndim:%d \n",ndim);
   BOX_SIZE=(float)header.BoxSize;
   
   printf("Box:%f \n",BOX_SIZE);
   
   for(nn=0; nn<header.npartTotal[1]; nn++){
     temp_pos = posdm[2+ndim*nn]+boost_factor*veldm[2+ndim*nn];
     //periodic box cond
     
     if(temp_pos> BOX_SIZE){
      temp_pos = temp_pos-BOX_SIZE;
     }
     if(temp_pos< 0){
      temp_pos = temp_pos+BOX_SIZE;
     }
     posdm[2+ndim*nn] = temp_pos;
   }
   cout<< " Redshift mapping done\n";
}
