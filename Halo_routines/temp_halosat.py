
################################################################
# Copyright (c) 2019, Sandeep Rana This is my first python3code#
# This is a general class to compute halo properties           #
# Halos catalog is generated using Velociraptor structure      #
# finder see Elahi et al. 2019                                 #
# https://ui.adsabs.harvard.edu/abs/2019PASA...36...21E        #
# get from git clone https://github.com/pelahi/VELOCIraptor-STF#
# Apart from basic velociraptor routines I have also tried to  # 
# Include data.tables fast table reading and analysis module   #
# it is still optional and in testing phase.                    #
################################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


#load other useful packages
from __future__ import print_function
import numpy as np
import pandas as pd
import argparse
import time
#for general python stuff
import sys,os,string,time,re,struct
import math,operator
#for useful scipy stuff
import itertools
#for useful mathematical tools
from sklearn.neighbors import NearestNeighbors
import scipy.spatial as spatial

# Correlation function uitilities
from Corrfunc.theory.xi import xi as xi_corr
from Corrfunc.theory.DD import DD
from Corrfunc.utils import convert_3d_counts_to_cf

# Importing astropy and hmf
from hmf import MassFunction
from hmf import cosmo
from astropy.cosmology import LambdaCDM
from astropy.io import ascii


# Fast tabular data reading module
import datatable as dt


#to load specific functions defined in another python file
basecodedir='/mnt/data1/sandeep/velociraptor_halo_catalog/VELOCIraptor_Python_Tools'
sys.path.append(basecodedir)
import velociraptor_python_tools as vpt
#import velociraptor_python_tools_cython as vpt
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
import Cosmo_Growth_Fac_raw as CGF_raw
import readsnap

# plotting libs
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec



#set some plotting parameters
rcParams['figure.figsize'] = (8,6)
rcParams['font.size'] = 18
rc('text', usetex=True)
rcParams['mathtext.fontset'] = 'custom'
rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
rcParams['xtick.direction']='in'
rcParams['ytick.direction']='in'
#plt.style.use("classic")


__author__     = "Sandeep Rana"
__credits__    = ["Pascal Elahi"]
__license__    = "GPL GPLv3 <https://www.gnu.org/licenses/gpl.txt>"
__version__    = "1.0.0"
__maintainer__ = "Sandeep Rana"
__copyright__  = "2020, Sandeep Rana"
__email__      = "sandeepranaiiser@gmail.com"


#--------- class for getting halo properties form the halo caltalogs ------


class Halo_Props:
    
    def __init__(self, haloCat_dirc_name, Lbox, NpartSim, 
                 snapID, ASCIIFORMAT=True, HDFFORMAT=False, 
                 BINARFORMAT=False):
        """

        """

        if os.path.exists(halo_dirc_name):
            self.haloCat_dirc_name = haloCat_dirc_name
        else:
            raise ValueError("Halo Catlog directory not found:")
    
        self.Lbox     = Lbox     
        self.Lboxhalf = Lbox/2.
        self.NpartSim = NpartSim

        #define file formats
        if not ASCIIFORMAT:
            if not HDFFORMAT:
                self.FileType =1
            else:
                self.FileType =2
        else:
            self.FileType=0
        
	# set base filename of halo catalog

        self.basefname = (haloCat_dirc_name+"/halo_data_sn_%d/raptor"\
                    	  "_HC_%dMpc_%d_sn_%03d"%(snapID, Lbox, NpartSim, snapID))

	#Reading my simulation header
	dir_name = "/mnt/data1/sandeep/Kalinga_run_Analysis/%dMpc_%d"% (Lbox, NpartSim)
        inp_file = dir_name + "/kalinga_snapdir_seed_1690811/snapshot_%03d.0"%snapID
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=False)

        #Setting Multidark cosmology
        self.H0          =  snapshot.HubbleParam*100.
        self.OmegaMatter =  snapshot.Omega0
        self.OmegaLambda =  snapshot.OmegaLambda
	self.zepoch      =  snapshot.redshift
	self.scale_fac   =  1./(1.+snapshot.redshift)
        self.OmegaBaryon =  0.048206
	self.sigma_8     =  0.8228 
	self.ns		 =  0.96
	self.snapshot    =  snapshot
        new_model   = LambdaCDM(H0= self.HO, Om0 = OmegaMatter, Tcmb0=2.73, Ob0= OmegaBaryon, Ode0= OmegaLambda)
        self.cosmo  = cosmo.Cosmology(new_model)

       
   #-------------------------------------------------------------------------------

    def _set_HMF_analytical(self, logMhalo_min, logMhalo_max, NBINS, Return=False):
        
        """
        """
        
        print("Please Enter the model that you want to use to compute HMF\n")
        print("Check for more available HM fitting functions (Default is ST): \n")
        print("https://hmf.readthedocs.io/en/latest/_autosummary/hmf.mass_"\
	      "function.fitting_functions.html#module-hmf.mass_function.fitting_functions.\n")
	
	Fit_model = raw_input("")
	if not Fit_model:
	    Fit_model='SMT'

        xlim              = [logMhalo_min, logMhalo_max]
        self.deltaM       = (xlim[1]-xlim[0])/float(NBINS)
        self.logMbins     = np.arange(xlim[0],xlim[1]+0.5*deltaM, deltaM)
        self.logMbins_avg = np.arange(xlim[0]+0.5*deltaM, xlim[1],deltaM)

        # Multidark 2 cosmology hmf model
        h = MassFunction(z=self.zepoch, Mmin=logMhalo_min, Mmax=logMhalo_max, dlog10m=deltaM, 
			hmf_model=Fit_model, cosmo_model=new_model, sigma_8=self.sigma_8, n=self.ns)

        self.HMF = h
	if not Return:
	    return h

#-------------------------------------------------------------------------------

    def _read_halo_properties(self, FastRead=True):
	
        if not FastRead:
            print("This is legacy code comes with Velociraptor as tools folder"\
                  ".It will take some time to read the properties data as it using basic 
                  pythonic way to read and write\n")
            print("-"*20)
	    start = time.process_time()
            self.halopropdata, self.numhalos = vpt.ReadPropertyFile(self.basefname, self.FileType)
            self.mass_unit_conv = halopropdata['UnitInfo']['Mass_unit_to_solarmass']*halopropdata['SimulationInfo']['h_val'] # 1e10 Msun/h
            self.len_unit_conv  = halopropdata['UnitInfo']['Length_unit_to_kpc']*halopropdata['SimulationInfo']['h_val'] # kpc h^{-1}
            self.vel_unit_conv  = halopropdata['UnitInfo']['Velocity_unit_to_kms']
 	    end = time.process_time()
	    print("Totla time taken for reading: {:%f}".format((end - start)))
            print("Total number of halos:{ :%d}".format(self.numhalos))	

            M_tot           = halopropdata['Mass_tot'][0]*self.mass_unit_conv # Msun/h  units
            DM_part_mass    = self.snapshot.massarr[1]*1e10
            No_part_in_halo = M_tot/DM_part_mass
            if np.int32(No_part_in_halo) != (halopropdata['npart'][0]):
                raise ValueError('number of particle and halo mass does not match')
        else:
            name =  self.basefname+"_properties.jay"
            if os.path.exists(name):
                self.halopropdata = dt.fread(name)
            else:
                self.halopropdata = dt.fread(self.basefname+".properties", skip_to_line=3)



            self.numhalo      = np.int32((dt.fread(self.basefname+".properties", max_nrows=2))[1,0])
            sim_info          = dt.fread(self.basefname+".siminfo")
            sim_info_names    = np.ndarray.flatten(sim_info['C0'].to_numpy())
            sim_info_values    = np.ndarray.flatten(sim_info['C2'].to_numpy())
            del sim_info
            units_info          = dt.fread(self.basefname+".units")
            units_info_names    = np.ndarray.flatten(units_info['C0'].to_numpy())
            units_info_values    = np.ndarray.flatten(units_info['C2'].to_numpy())
            del units_info
            
            indx_mass     = (units_info_names=='Mass_unit_to_solarmass')
            indx_length   = (units_info_names=='Length_unit_to_kpc')
            indx_velocity = (units_info_names=='Velocity_unit_to_kms')
            indx_hval     = (sim_info_names=='h_val')
            
            self.mass_unit_conv = units_info_values[indx_mass]*sim_info_values[indx_hval] # 1e10 Msun/h
            self.len_unit_conv  = units_info_values[indx_length]*sim_info_values[indx_hval] # kpc h^{-1}
            self.vel_unit_conv  = units_info_values[indx_velocity]
            
            M_tot           = self.halopropdata['Mass_tot(27)'][0, 0] * self.mass_unit_conv # Msun/h  units
            DM_part_mass    = self.snapshot.massarr[1]*1e10
            No_part_in_halo = M_tot/DM_part_mass
            
            if np.int32(No_part_in_halo) != (halopropdata['npart(6)'][0]):
                raise ValueError('number of particle and halo mass does not match')


#-------------------------------------------------------------------------------
     
    def _compute_HMF_from_sim(self, save=False, plot_compare=True, FastRead=True):
       
        if not FastRead:
            #desired mass field
            massfield=['Mass_200crit','Mass_tot', 'Mass_200mean', 'Mvir', 'M_FOF']
            massbindata=dict()
        
            #make histograms while converting units to Msun and kpc
            for key in massfield:
                massfac = np.log10(self.mass_unit_conv)
                massbindata[key], blah = np.histogram(np.log10(self.halopropdata[key]) + massfac, self.logMbins)
                massbindata[key] = np.float64(massbindata[key])/np.float64(self.Lbox)**3

        else:
            massfield=['Mass_200crit(30)','Mass_tot(27)', 'Mass_200mean(29)', 'Mvir(8)', 'M_FOF(28)']
            massbindata=dict()
        
            #make histograms while converting units to Msun and kpc
            for key in massfield:
                massfac = np.log10(self.mass_unit_conv)
                temp = np.ndarray.flatten(self.halopropdata[key].to_numpy()) 
                massbindata[key], blah = np.histogram(np.log10(temp) + massfac, self.logMbins)
                massbindata[key] = np.float64(massbindata[key])/np.float64(self.Lbox)**3


	if not save:
	    return massbindata, massfield, self.logMbins_avg
	else:

            inputfname= self.haloCat_dirc_name+"/halo_data_sn_%d/MF_data_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim)
	    if os.path.exists(inputfname):

		print("MF Directory exist writing MF file\n")
                fname_MF = (inputfname+"KMDPL2_Raptor_allMass_fun_BoxSize_"\
                        "%dMpc_NpartSim_%d_snapID_%d.txt"%(self.Lbox, self.NpartSim, self.snapID))
	        np.savetxt(fname_MF, zip(np.power(10.0,xbins), massbindata[massfield[0]], 
	               massbindata[massfield[1]], massbindata[massfield[2]], 
	               massbindata[massfield[3]],massbindata[massfield[4]] ), delimiter='\t', fmt="%0.6e", 
	               header='Mass_200crit\tMass_tot\tMass_200mean\tMvir\tM_FOF')
            else:

		print("MF Directory doesn't exist creating a new one\n")
	        os.mkdir(inputfname)
                fname_MF = (inputfname+"KMDPL2_Raptor_allMass_fun_BoxSize_"\
                        "%dMpc_NpartSim_%d_snapID_%d.txt"%(self.Lbox, self.NpartSim, self.snapID))
	        np.savetxt(fname_MF, zip(np.power(10.0,xbins), massbindata[massfield[0]], 
	               massbindata[massfield[1]], massbindata[massfield[2]], 
	               massbindata[massfield[3]],massbindata[massfield[4]] ), delimiter='\t', fmt="%0.6e", 
	               header='Mass_200crit\tMass_tot\tMass_200mean\tMvir\tM_FOF')

	# To plot Mass function along with analytical function
	if plot_compare:

            vlabel = [r'$\rm Mass_{200crit}$',r'$\rm Mass_{tot}$', 
		      r'$\rm Mass_{200mean} $', r'$\rm M_{vir}$', 
		      r'$\rm M_{FOF}$']


	    plt.figure(1, figsize=(12, 10))

	    gs = gridspec.GridSpec(2, 1, hspace=0.0, height_ratios=[3,1])
	    ax1 = plt.subplot(gs[0])
	    NUM_COLORS = 4
	    cm = plt.get_cmap('hsv')
	    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
	    mark = ['d', 'o', 's', '^', '>']

	    ax1.plot(self.HMF.m, self.HMF.dndlog10m, linestyle='-', linewidth=2, label=self.Fit_model)
	    for i in range(len(massfield)):
		ax1.plot(np.power(10.0, self.logMbins_avg), massbindata[massfield[i]]/self.deltaM,
		linestyle='-', linewidth=1.8, mew=1.5, marker=mark[i], ms=8,label=vlabels[i])

	    ax1.axvline(x=1e12, linestyle='--', linewidth=2, color='m', 
			label=r"$10^{10} \rm{M}_\odot$ $\rm{h}^{-1}$")
			
	    ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{M}$ ($\rm{h}^{3}$ $\rm{Mpc}^{-3}$)', fontsize=20)
	    ax1.set_xscale("log")
	    ax1.set_yscale("log")
	    ax1.set_xlim(10**9, 10**15.5)
	    ax1.minorticks_on()
	    plt.setp(ax1.get_xticklabels(),visible=False)
	    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
	    ax1.tick_params(axis='both', which='minor', length=5,
			      width=2, labelsize=16)
	    ax1.tick_params(axis='both', which='major', length=8,
			       width=2, labelsize=16)
	    ax1.spines['bottom'].set_linewidth(1.8)
	    ax1.spines['left'].set_linewidth(1.8)
	    ax1.spines['top'].set_linewidth(1.8)
	    ax1.spines['right'].set_linewidth(1.8)
	    plt.tight_layout()


	    ax2 = plt.subplot(gs[1],  sharex=ax1)

	    NUM_COLORS = 4
	    cm = plt.get_cmap('hsv')
	    ax2.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
	    for i in range(len(massfield)):
	    
		temp  = massbindata[massfield[i]]/self.deltaM
		ratio  = np.abs(temp-self.HMF.dndlog10m)/self.HMF.dndlog10m
		
		ax2.plot(np.power(10.0, self.logMbins_avg), ratio*100, linestyle='-', 
			 linewidth=1.8, mew=1.5, marker=mark[i], ms=8, 
			 label=vlabels[i])

	    ax2.set_xlabel(r'$\rm{M}$ [$\rm{M}_\odot$ $\rm{h}^{-1}$]', fontsize=20)
	    ax2.set_ylabel(r'$\Delta (\%)$', fontsize=20)
	    #ax1.set_xscale("log")
	    ax2.set_yscale("log")
	    ax1.set_xlim(10**9, 10**15.5)
	    ax2.set_ylim(0.373, 341)
	    ax2.minorticks_on()
	    ax2.legend(frameon=False, ncol=1, fontsize=16, loc=3)
	    ax2.tick_params(axis='both', which='minor', length=5,
			      width=2, labelsize=16)
	    ax2.tick_params(axis='both', which='major', length=8,
			       width=2, labelsize=16)
	    ax2.spines['bottom'].set_linewidth(1.8)
	    ax2.spines['left'].set_linewidth(1.8)
	    ax2.spines['top'].set_linewidth(1.8)
	    ax2.spines['right'].set_linewidth(1.8)
	    plt.tight_layout()

	    inputfname = (self.haloCat_dirc_name+"/halo_data_sn_%d/MF_plots"\
			  "_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim)

	    if os.path.exists(inputfname):
		print("MF plot Directory exist saving file\n")

	        plot_name = (inputfname+"KMDPL2_Raptor_CompareMass_fun_BoxSize_"\
		  	     "%dMpc_NpartSim_%d_snapID_%d.png"%(Lbox, NpartSim, snapID))
	        plt.savefig(plot_name, dpi=300)

	    else:
		print("MF plots Directory doesn't exist creating a new one\n")
	        os.mkdir(inputfname)
		plot_name = (inputfname+"KMDPL2_Raptor_CompareMass_fun_BoxSize_"\
		  	     "%dMpc_NpartSim_%d_snapID_%d.png"%(Lbox, NpartSim, snapID))
	        plt.savefig(plot_name, dpi=300)

	    plt.close()


#-------------------------------------------------------------------------------

    def _compute_VF_from_sim(self, logV_min, logV_max, NBINS, save=False, 
                            plot_compare=True, FastRead=True):
    
        """
    	"""

	#Vmax DistributioNBINS=50
	xlim=[logV_min, logV_max]
	deltaV=(xlim[1]-xlim[0])/np.float64(NBINS)
	xedges=np.arange(xlim[0],xlim[1]+0.5*deltaV,deltaV)
	xbins=np.arange(xlim[0]+0.5*deltaV,xlim[1],deltaV)
        
        if not FastRead:
            #desired velocity field
            fields=['Vmax','sigV']
            bindata=dict()
            #make histograms while converting units to km/s
            for key in fields:
                fac=np.log10(self.Velocity_unit_to_kms)
                bindata[key], blah = np.histogram(np.log10(self.halopropdata[key])+fac, xedges)
                bindata[key]       = np.float64(bindata[key])/np.float64(self.Lbox)**3
        else:
        
            #desired velocity field
            fields=['Vmax(40)','sigV(41)']
            bindata=dict()
            #make histograms while converting units to km/s
            for key in fields:
                fac=np.log10(self.Velocity_unit_to_kms)
                temp = np.ndarray.flatten(self.halopropdata[key].to_numpy()) 
                bindata[key], blah = np.histogram(np.log10(temp)+fac, xedges)
                bindata[key]       = np.float64(bindata[key])/np.float64(self.Lbox)**3



	if not save:
	    return bindata, fields, xbins
	else:

            inputfname= self.haloCat_dirc_name+"/halo_data_sn_%d/MF_data_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim)

	    fname_MF = (inputfname+"KMDPL2_Raptor_allVmax_fun_BoxSize_"\
		    "%dMpc_NpartSim_%d_snapID_%d.txt"%(self.Lbox, self.NpartSim, self.snapID))
	    np.savetxt(fname_MF, zip(np.power(10.0,xbins), bindata[field[0]], 
		       bindata[field[1]]), delimiter='\t', fmt="%0.6e", 
               	       header='Vmax\tsigV')

	if plot_compare:
    	    
	    vlabels=[r'$\rm{V}_{\rm{max}}$',r'$\sigma_{\rm{v}}$']
    	    plt.figure(1, figsize=(8, 6))
	    gs = gridspec.GridSpec(1, 1)
	    ax1 = plt.subplot(gs[0])
	    NUM_COLORS = len(fields)
	    cm = plt.get_cmap('hsv')
	    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
	    mark = ['s', 'o', '^']

    	    for i in range(len(fields)):
                ax1.plot(np.power(10.0, xbins), bindata[fields[i]]/deltaV,
                linestyle='-', linewidth=1.8, mew=1.5, marker=mark[i], ms=8,label=vlabels[i])

            ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{V}$ ($\rm{h}^{3}$ $\rm{Mpc}^{-3}$)', fontsize=20)
	    ax1.set_xlabel(r'$\rm{V}$ ($\rm{km}$ $\rm{s}^{-1}$)', fontsize=20)
	    ax1.set_xscale("log")
	    ax1.set_yscale("log")
	    ax1.set_xlim(10, 10**3.6)
	    ax1.minorticks_on()
	    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
	    ax1.tick_params(axis='both', which='minor', length=5,
			      width=2, labelsize=16)
	    ax1.tick_params(axis='both', which='major', length=8,
			       width=2, labelsize=16)
	    ax1.spines['bottom'].set_linewidth(1.8)
	    ax1.spines['left'].set_linewidth(1.8)
	    ax1.spines['top'].set_linewidth(1.8)
	    ax1.spines['right'].set_linewidth(1.8)
	    plt.tight_layout()
	
    	   inputfname='../halo_data_%dMpc_%d/MF_plots_%dMpc_%d/'%(Lbox, NpartSim, Lbox, NpartSim)
    	   plot_name =(inputfname+"KMDPL2_Raptor_Vmax_dist_BoxSize_"\
           	     "%dMpc_NpartSim_%d_snapID_%d.png"%(Lbox, NpartSim, snapID))
    	   plt.savefig(plot_name, dpi=300, bbox_inches='tight')
    	   plt.close()


#-------------------------------------------------------------------------------

    def _halo_corr(self, logM_min, logM_max, massdef='Mass_tot(27)'):
      

        nbins   = 50
        nthreads= 25
        rmin = 4*(self.Lbox/self.NpartSim/25.)
        rmax = self.Lbox/5.0
        

        if not massdef:
            massdef = 'Mvir(8)'
        else:
            massdef ='Mass_tot(27)'

        rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
        xi1     = np.zeros(nbins, dtype=np.float64)
        xi      = np.zeros(nbins, dtype=np.float64)
        xi_bar  = np.zeros(nbins, dtype=np.float64)
        ravg    = np.zeros(nbins, dtype=np.float64)
        r_min   = np.zeros(nbins, dtype=np.float64)
        r_max   = np.zeros(nbins, dtype=np.float64)
        npairs  = np.zeros(nbins, dtype=np.float64)
        err     = np.zeros(nbins, dtype=np.float64)
        Avpart     = np.zeros(nbins, dtype=np.float64)
        

        X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 

        log_halo_mass = np.log10( np.ndarray.flatten(self.halopropdata[massdef].to_numpy()) )
        keep = (log_halo_mass>=logMhalo_min )*(log_halo_mass< logMhalo_max)
        
        X = X[keep]
        Y = Y[keep]
        Z = Z[keep]


        weights = np.ones_like(X)   
        numden = np.float64(1.*len(X)/self.Lbox**3)
        
        xi_counts = xi_corr(self.Lbox, nthreads, rbins, X, Y, Z, weights=weights, 
                            weight_type='pair_product', verbose=True, output_ravg=True)

        del X; del Y; del Z
        
        count=0
        sum1=0
        sum2=0
        for r in xi_counts: 
            
            vol = (4.0*np.pi/3.0)*( r['rmax']**3.0 - r['rmin']**3.0 )
            avpart= np.float64(len(X)) * vol * numden;
            
            xi1[count]     = (np.float64(r['npairs'])/avpart) - 1.
            sum2          += r['npairs']
            sum1          += avpart
            xi_bar[count]  = (sum2/sum1) - 1.0

            xi[count]      = r['xi']
            npairs[count]  = r['npairs']
            ravg[count]    = r['ravg']
            r_min[count]   = r['rmin']
            r_max[count]   = r['rmax']
            err[count]     = (1.+r['xi'])/np.sqrt(r['npairs'])
            Avpart[count]  = avpart
            count         += 1
            
        dirc_name = "./corr_func_results"
        if not os.path.exists(dirc_name):
            os.mkdir("./corr_func_results")

        outfile = (dirc_name+"/logbin_KMDPL2_Corr_"\
                   "%dMpc%d_SnapID_%d_MhaloMin_%0.1f"\
                   "_MhaloMax_%0.1f_Rmax_%0.1fMpc_Nbr_%d.txt"%(
                   self.Lbox, self.NpartSim, self.snapID, logM_min, 
                   logM_max, rmax, nbins))
                   
        np.savetxt(outfile, zip(r_min, r_max, ravg, xi, npairs, Avpart, xi1, xi_bar, err), 
                   fmt="%2.14f", delimiter='\t')
     
 
#-------------------------------------------------------------------------------

    def _halo_Dq_corr(self, logM_min, logM_max, massdef='Mass_tot(27)'):

	"""
	"""
    	rmin = 10.0
    	print("Lbox{: }, Npart_Sim{: f}".format(self.Lbox, self.NpartSim)
    	rmax = Lbox/5.0
    	print("rmin{: f}, rmax{: f}".format(rmin, rmax))
	nbins   = 50
    	rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
	NMc     = 500000
    	autocorr = 0
	
	if not massdef:
            massdef = 'Mvir(8)'
        else:
            massdef ='Mass_tot(27)'

	X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 

        log_halo_mass = np.log10( np.ndarray.flatten(self.halopropdata[massdef].to_numpy()) )
        keep = (log_halo_mass>=logMhalo_min )*(log_halo_mass< logMhalo_max)
        
        X1 = X[keep]
        Y1 = Y[keep]
        Z1 = Z[keep]
	index = np.random.randint(0, samp_size, size=NMc)
    	X2 = np.float64(X[index])
    	Y2 = np.float64(Y[index])
    	Z2 = np.float64(Z[index])

        results = DD(autocorr, nthreads, rbins, X1, Y1, Z1,
                 X2=X2, Y2=Y2, Z2=Z2, verbose=True,
                 boxsize=np.float64(Lbox), output_ravg=True)
	del X,Y,Z
	del X1,Y1,Z1
	del X2,Y2,Z2
        count=0
	for r in results:
	    vol = (4.0*np.pi/3.0) * ( r['rmax']**3.0 - r['rmin']**3.0 )
	    avpart= np.float64(samp_size) * vol * numden;
	    npairs[count]  = r['npairs']
	    ravg[count]    = r['ravg']
	    r_min[count]   = r['rmin']
	    r_max[count]   = r['rmax']
	    Avpart[count]  = avpart
	    count+=1

	Cqd = npairs/np.float64(samp_size)/np.float64(NMc)
	Dqd =  np.diff(np.log10(Cqd))
	Dqd = Dqd/np.diff(np.log10(rbins[:-1]))

        dirc_name = "./corrfunc_fractal_results"
        if not os.path.exists(dirc_name):
            os.mkdir("./corrfunc_halo_fractal_results")

        outfile = (dirc_name+"/logbin_CorrInt_Lbox_%d_"\
         	   "NparSim_%d_snapID_%d_MhaloMin_%0.1f"\
                   "_MhaloMax_%0.1f_Rmax_%0.1fMpc_Nbr_%d"\
                   "_NMc_%d.dat"%(self.Lbox, self.NpartSim, 
                   self.snapID, logMhalo_min, logMhalo_max rmax,
                   nbins, NMc))

    	np.savetxt(outfile, zip(r_min, r_max, ravg, Avpart, npairs, Cqd, Dqd),
               fmt="%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f\t%2.14f",
               header='r_min\tr_max\travg\tAvpart\tnpairs\tCqd2\tDqd2',delimiter='\t')

#-------------------------------------------------------------------------------

    def _make_halo_subsamp(self, nsamples, samp_size, logMhalo_min, logMhalo_max, massdef=True):
        
        """
        """

        # some checks
        if samp_size>self.numhalos:
            raise ValueError("sub sample size can't be more than total number of halos")


        if not massdef:
            halo_mass_typ = 'Mvir(8)'
        else:
            halo_mass_typ ='Mass_tot(27)'

	X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 
        
	VX  = np.ndarray.flatten(self.halopropdata['VXc(18)'].to_numpy()) 
        VY  = np.ndarray.flatten(self.halopropdata['VYc(19)'].to_numpy()) 
        VZ  = np.ndarray.flatten(self.halopropdata['VZc(20)'].to_numpy()) 

        log_halo_mass = np.log10(np.ndarray.flatten(self.halopropdata[halo_mass_typ].to_numpy()) )
        keep = (log_halo_mass>=logMhalo_min )*(log_halo_mass< logMhalo_max)
        

        X = X[keep]
        Y = Y[keep]
        Z = Z[keep]

        VX = VX[keep]
        VY = VY[keep]
        VZ = VZ[keep]
        
        dirc_name = "./halo_sub_sample"
        if not os.path.exists(dirc_name):
            os.mkdir("./halo_sub_sample")

        for j in xrange(0, n_sample):
        
            count=0
            halo_pos=np.zeros((samp_Size, 3), dtype=np.float64)
            halo_vel=np.zeros((samp_Size, 3), dtype=np.float64)
            indices = np.random.randint(self.numhalos, size=samp_size)
            
            halo_pos[:, 0] = Xc[indices]
            halo_pos[:, 1] = Yc[indices]
            halo_pos[:, 2] = Zc[indices]
            
            halo_vel[:, 0] = VXc[indices]
            halo_vel[:, 1] = VYc[indices]
            halo_vel[:, 2] = VZc[indices]


            halo_pos = np.ndarray.flatten(halo_pos)  
            halo_vel = np.ndarray.flatten(halo_vel)  
            
            temp  = np.array([self.snapID], dtype=np.float64)
            temp1 = np.array([self.Lbox], dtype=np.float64)
            file_data = np.concatenate((temp, temp1, halo_pos, halo_vel, temp1, temp))    
            snapfname = (dirc_name+"KMDPL2_%dMpc_%d_snapID_%d_SampSize_%d_"\
                         "MhaloMin_%0.1f_MhaloMax_%0.1f_real_%d.bin"%(self.Lbox, self.NpartSim, 
                         self.snapID, samp_size, logMhalo_min, logMhalo_max, j))
            print snapfname
            file_data.tofile(snapfname, format="%lf")

#-------------------------------------------------------------------------------
 
    def _conv_ascii_to_jay(self):
#        vpt.ConvertASCIIPropertyFileToHDF(self.basefname, iseparatesubfiles=0, iverbose=0)
        name = self.basefname+"_properties.jay"
        if os.path.exists(name):
            raise ValueError("Halo catalog in jay extension already exists")
        else:
            self.halopropdata.to_jay(name)
        
#-------------------------------------------------------------------------------

    def _free_data(self):
        del self.halopropdata

#-------------------------------------------------------------------------------


    def _test_position(self):
        """
        """

        X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 
        
        plt.figure(1, figsize=(8, 6))
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0])
        ax1.scatter(X, Y, lw=0, s=1)
        ax1.set_ylabel(r'$\rm Y$)', fontsize=20)
        ax1.set_xlabel(r'$\rm X$)', fontsize=20)
        ax1.set_xlim(0, self.Lbox)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        plt.savefig("temp_xy.png")
        plt.close()

        plt.figure(1, figsize=(8, 6))
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0])

        ax1.scatter(X, Z, lw=0, s=1)
        ax1.set_ylabel(r'$\rm Z$)', fontsize=20)
        ax1.set_xlabel(r'$\rm X$)', fontsize=20)
        ax1.set_xlim(0, self.Lbox)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        plt.savefig("temp_xz.png")
        plt.close()


        plt.figure(1, figsize=(8, 6))
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0])

        ax1.scatter(Y, Z, lw=0, s=1)
        ax1.set_ylabel(r'$\rm Z$)', fontsize=20)
        ax1.set_xlabel(r'$\rm Y$)', fontsize=20)
        ax1.set_xlim(0, self.Lbox)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        plt.savefig("temp_yz.png")
        plt.close()

   
 


