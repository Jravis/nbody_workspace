import numpy as np
import Gen_HaloStats as hs
import argparse




def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Number of particle in sim i.e 512 is 512^3 particles')
    parser.add_argument("snapID", type=int, 
    help='snapshot ID')




    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    snapID   = args.snapID

    #define formats
    ASCIIFORMAT=0
    HDFFORMAT=2
    BINARFORMAT=1
 
    dirc_name = ("/mnt/data1/sandeep/Kalinga_run_Analysis"\
                "/Kalinga_halo_cat/halo_data_%dMpc_%d"%(Lbox, NpartSim))
    halo_props = hs.Halo_Props(dirc_name, Lbox, NpartSim, snapID)
    halo_props._set_HMF_analytical(9.0, 16, 50, False)    
    halo_props._read_halo_properties()
    halo_props._compute_HMF_from_sim()
    #halo_props._compute_VF_from_sim(1, 3.6, 50)
    #halo_props._halo_corr(11.5, 16.0)
    #halo_props._halo_Dq_corr(11.5, 16.0)
    #halo_props._conv_ascii_to_jay()
    
if __name__ == "__main__":
    main()




