
################################################################
# Copyright (c) 2019, Sandeep Rana This is my first python3code#
# This is a general class to compute halo properties           #
# Halos catalog is generated using Velociraptor structure      #
# finder see Elahi et al. 2019                                 #
# https://ui.adsabs.harvard.edu/abs/2019PASA...36...21E        #
# get from git clone https://github.com/pelahi/VELOCIraptor-STF#
# Apart from basic velociraptor routines I have also tried to  # 
# Include data.tables fast table reading and analysis module   #
# it is still optional and in testing phase.                    #
################################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


#load other useful packages
from __future__ import print_function
import numpy as np
import pandas as pd
import argparse
import time
#for general python stuff
import sys,os,string,time,re,struct
import math,operator
#for useful scipy stuff
import itertools
#for useful mathematical tools
from sklearn.neighbors import NearestNeighbors
import scipy.spatial as spatial

# Correlation function uitilities
from Corrfunc.theory.xi import xi as xi_corr
from Corrfunc.theory.DD import DD
from Corrfunc.utils import convert_3d_counts_to_cf

# Importing astropy and hmf
from hmf import MassFunction
from hmf import cosmo
from astropy.cosmology import LambdaCDM
from astropy.io import ascii


# Fast tabular data reading module
import datatable as dt


#to load specific functions defined in another python file
basecodedir='/mnt/data1/sandeep/velociraptor_halo_catalog/VELOCIraptor_Python_Tools'
sys.path.append(basecodedir)
import velociraptor_python_tools as vpt
#import velociraptor_python_tools_cython as vpt
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
#import Cosmo_Growth_Fac_raw as CGF_raw
import readsnap

# plotting libs
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec



#set some plotting parameters
mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
#plt.style.use("classic")


__author__     = "Sandeep Rana"
__credits__    = ["Pascal Elahi"]
__license__    = "GPL GPLv3 <https://www.gnu.org/licenses/gpl.txt>"
__version__    = "1.0.0"
__maintainer__ = "Sandeep Rana"
__copyright__  = "2020, Sandeep Rana"
__email__      = "sandeepranaiiser@gmail.com"


#--------- class for getting halo properties form the halo caltalogs ------


class Halo_Props:
    
    def __init__(self, haloCat_dirc_name, Lbox, NpartSim, 
                 snapID, ASCIIFORMAT=True, HDFFORMAT=False, 
                 BINARFORMAT=False):
        """

        """

        if os.path.exists(haloCat_dirc_name):
            self.haloCat_dirc_name = haloCat_dirc_name
        else:
            raise ValueError("Halo Catlog directory not found:")

        self.Lbox     = Lbox     
        self.Lboxhalf = Lbox/2.
        self.NpartSim = NpartSim
        self.snapID   = snapID
        
        #define file formats
        if not ASCIIFORMAT:
            if not HDFFORMAT:
                self.FileType =1
            else:
                self.FileType =2
        else:
            self.FileType=0
        
# set base filename of halo catalog

        self.basefname = (haloCat_dirc_name+"/halo_data_sn_%d/raptor"\
        "_HC_%dMpc_%d_sn_%03d"%(snapID, Lbox, NpartSim, snapID))

#Reading my simulation header
        dir_name = ("/mnt/data1/sandeep/Kalinga_run_Analysis/%dMpc_%d"%(Lbox, NpartSim))
        inp_file = dir_name + "/kalinga_snapdir_seed_1690811/snapshot_%03d.0"%snapID
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=False)

        #Setting Multidark cosmology
        self.H0          =  snapshot.HubbleParam*100.
        self.OmegaMatter =  snapshot.Omega0
        self.OmegaLambda =  snapshot.OmegaLambda
        self.zepoch      =  snapshot.redshift
        self.scale_fac   =  1./(1.+snapshot.redshift)
        self.OmegaBaryon =  0.048206
        self.sigma_8     =  0.8228 
        self.ns		     =  0.96
        self.snapshot    =  snapshot
        new_model        = LambdaCDM(H0= snapshot.HubbleParam*100., Om0 = snapshot.Omega0, 
                                     Tcmb0=2.73, Ob0= self.OmegaBaryon, Ode0= snapshot.OmegaLambda)
        self.cosmo  = cosmo.Cosmology(new_model)
        self.new_model = new_model

   
   #-------------------------------------------------------------------------------

    def _set_HMF_analytical(self, logMhalo_min, logMhalo_max, NBINS, Return=True):
        
        """
        """
        
        print("Please Enter the model that you want to use to compute HMF\n")
        print("Check for more available HM fitting functions (Default is ST): \n")
        print("https://hmf.readthedocs.io/en/latest/_autosummary/hmf.mass_"\
        "function.fitting_functions.html#module-hmf.mass_function.fitting_functions.\n")

        Fit_model = input("")
        if not Fit_model:
            Fit_model='SMT'
        print("Using:", Fit_model)
        xlim              = [logMhalo_min, logMhalo_max]
        deltaM            = (xlim[1]-xlim[0])/float(NBINS)
        self.logMbins     = np.arange(xlim[0],xlim[1]+0.5*deltaM, deltaM)
        self.logMbins_avg = np.arange(xlim[0]+0.5*deltaM, xlim[1], deltaM)
        self.deltaM       = deltaM
        self.Fit_model    = Fit_model
        # Multidark 2 cosmology hmf model
        h = MassFunction(z=self.zepoch, Mmin=logMhalo_min, Mmax=logMhalo_max, dlog10m=self.deltaM, 
            hmf_model=Fit_model, cosmo_model=self.new_model, sigma_8=self.sigma_8, n=self.ns)

        self.HMF = h
        if Return:
            return h

#-------------------------------------------------------------------------------

    def _get_MDPL2_MF(self):
        """
        """
        print("Computing halo mass and velocity function from Rockstar catalog of MDPL2\n")
        name =  "../MDPL_MF_data"

        if os.path.exists(name):

            self.MDPL2_mrange  = np.genfromtxt(name+"/MDPL2_MF_rockstar.txt", usecols=0) # mass range
            self.MDPL2_MF      = np.genfromtxt(name+"/MDPL2_MF_rockstar.txt", usecols=1) # Mass function of Mtot 
            self.MDPL2_vrange  = np.genfromtxt(name+"/MDPL2_VF_rockstar.txt", usecols=0) # Mass function of Mtot 
            self.MDPL2_Vmax    = np.genfromtxt(name+"/MDPL2_VF_rockstar.txt", usecols=1) # Mass function of Mtot 
            self.MDPL2_sigmav  = np.genfromtxt(name+"/MDPL2_VF_rockstar.txt", usecols=2) # Mass function of Mtot 
            
        else:

            name = "/mnt/data1/sandeep/multidark2_data/halo_cat/halo_data/Rockstar_MDPL2_halo_properties.csv"
            start = time.process_time()
            MDPLpropdata = dt.fread(name)
            end = time.process_time()
            print("Total time taken for reading: {: f}".format((end - start)))

            #desired mass field
            massfield=['Mvir',  'M200c', 'M200b']
            massbindata=dict()
            
            Lbox_MDPL2 = 1000 #h^-1 Mpc
            #make histograms while converting units to Msun and kpc
            for key in massfield:
                massbindata[key], blah = np.histogram(np.log10(MDPLpropdata[key].to_numpy()), self.logMbins)
                massbindata[key] = np.float64(massbindata[key])/np.float64(Lbox_MDPL2)**3
            
            NBINS = 50
            xlim=[1, 3.6]
            deltaV=(xlim[1]-xlim[0])/np.float64(NBINS)
            vedges=np.arange(xlim[0],xlim[1]+0.5*deltaV,deltaV)
            vbins=np.arange(xlim[0]+0.5*deltaV,xlim[1],deltaV)
        
            #desired velocity field
            fields=['Vmax', 'Vrms']
            bindata=dict()
            #make histograms while converting units to km/s
            for key in fields:
                bindata[key], blah = np.histogram(np.log10(MDPLpropdata[key].to_numpy()), vedges)
                bindata[key]       = np.float64(bindata[key])/np.float64(Lbox_MDPL2)**3


            os.mkdir("../MDPL_MF_data")
            

            fname_MF = "../MDPL_MF_data/MDPL2_MF_rockstar.txt"
            table = [(10.0**self.logMbins_avg), massbindata[massfield[0]], 
            massbindata[massfield[1]], massbindata[massfield[2]]]
            
            names = ['Mbins_avg', 'Mvir', 'Mass_200crit', 'Mass_200b']        
            Formats = {'Mbins_avg':'%0.6e', 'Mvir':'%0.6e',  'Mass_200crit':'%0.6e',
            'Mass_200b':'%0.6e'}
            ascii.write(table, fname_MF, names=names, delimiter='\t', formats=Formats, overwrite=True)


            fname_MF = "../MDPL_MF_data/MDPL2_VF_rockstar.txt"
            table = [(10.0**vbins), bindata[fields[0]], bindata[fields[1]]]
            names   = ['vbins_avg', 'Vmax', 'Vrms']        
            Formats = {'vbinss_avg':'%0.6e', 'Vmax':'%0.6e',  'Vrms':'%0.6e'}
            ascii.write(table, fname_MF, names=names, delimiter='\t', formats=Formats, overwrite=True)


            
            self.MDPL2_mrange  = 10**self.logMbins_avg
            self.MDPL2_MF      = massbindata[massfield[0]]
            self.MDPL2_vrange  = 10**vbins
            self.MDPL2_Vmax    = bindata[fields[0]]
            self.MDPL2_sigmav  = bindata[fields[1]]



#-------------------------------------------------------------------------------

    def _read_halo_properties(self, FastRead=True):
	
        if not FastRead:
            print("This is legacy code comes with Velociraptor as tools folder"\
            ".It will take some time to read the properties data as it using basic"\
            "pythonic way to read and write\n")
            print("-"*20)
            start = time.process_time()
            self.halopropdata, self.numhalos = vpt.ReadPropertyFile(self.basefname, self.FileType)
            self.mass_unit_conv = halopropdata['UnitInfo']['Mass_unit_to_solarmass']*halopropdata['SimulationInfo']['h_val'] # 1e10 Msun/h
            self.len_unit_conv  = halopropdata['UnitInfo']['Length_unit_to_kpc']*halopropdata['SimulationInfo']['h_val'] # kpc h^{-1}
            self.vel_unit_conv  = halopropdata['UnitInfo']['Velocity_unit_to_kms']
            end = time.process_time()
            print("Total time taken for reading: {: f}".format((end - start)))
            print("Total number of halos:{: d}".format(self.numhalos))	

            M_tot           = halopropdata['Mass_tot'][0]*self.mass_unit_conv # Msun/h  units
            DM_part_mass    = self.snapshot.massarr[1]*1e10
            No_part_in_halo = M_tot/DM_part_mass
            if np.int32(No_part_in_halo) != (halopropdata['npart'][0]):
                raise ValueError('number of particle and halo mass does not match')
        else:
            name =  self.basefname+"_properties.jay"
            if os.path.exists(name):
                self.halopropdata = dt.fread(name)
            else:
                self.halopropdata = dt.fread(self.basefname+".properties", skip_to_line=3)

            self.numhalo      = np.int32((dt.fread(self.basefname+".properties", max_nrows=2))[1,0])
            sim_info          = dt.fread(self.basefname+".siminfo")
            sim_info_names    = np.ndarray.flatten(sim_info['C0'].to_numpy())
            sim_info_values    = np.ndarray.flatten(sim_info['C2'].to_numpy())
            del sim_info
            units_info          = dt.fread(self.basefname+".units")
            units_info_names    = np.ndarray.flatten(units_info['C0'].to_numpy())
            units_info_values    = np.ndarray.flatten(units_info['C2'].to_numpy())
            del units_info
            
            indx_mass     = (units_info_names=='Mass_unit_to_solarmass')
            indx_length   = (units_info_names=='Length_unit_to_kpc')
            indx_velocity = (units_info_names=='Velocity_unit_to_kms')
            indx_hval     = (sim_info_names=='h_val')
            
            self.mass_unit_conv = units_info_values[indx_mass]*sim_info_values[indx_hval] # 1e10 Msun/h
            self.len_unit_conv  = units_info_values[indx_length]*sim_info_values[indx_hval] # kpc h^{-1}
            self.vel_unit_conv  = units_info_values[indx_velocity]
            

            M_tot           = self.halopropdata['Mass_tot(27)'][0, 0] * self.mass_unit_conv # Msun/h  units
            DM_part_mass    = self.snapshot.massarr[1]*self.mass_unit_conv
            No_part_in_halo = M_tot/DM_part_mass
            if np.int32(np.around(No_part_in_halo)) != ((self.halopropdata['npart(6)'][0,0])):
                raise ValueError('number of particle and halo mass does not match')


#-------------------------------------------------------------------------------
 
    def _compute_HMF_from_sim(self, save=True, plot_compare=True, FastRead=True):
        
        if not FastRead:
            #desired mass field
            massfield=['Mass_200crit','Mass_tot', 'Mass_200mean', 'Mvir', 'Mass_FOF']
            massbindata=dict()
        
            #make histograms while converting units to Msun and kpc
            for key in massfield:
                massfac = np.log10(self.mass_unit_conv)
                massbindata[key], blah = np.histogram(np.log10(self.halopropdata[key]) + massfac, self.logMbins)
                massbindata[key] = np.float64(massbindata[key])/np.float64(self.Lbox)**3
        else:
            massfield=['Mass_200crit(30)','Mass_tot(27)', 'Mass_200mean(29)', 'Mvir(8)', 'Mass_FOF(28)']
            massbindata=dict()
        
            #make histograms while converting units to Msun and kpc
            for key in massfield:
                massfac = np.log10(self.mass_unit_conv)
                temp = np.ndarray.flatten(self.halopropdata[key].to_numpy()) 
                massbindata[key], blah = np.histogram(np.log10(temp) + massfac, self.logMbins)
                massbindata[key] = np.float64(massbindata[key])/np.float64(self.Lbox)**3
	    
        if not save:
	        return massbindata, massfield, self.logMbins_avg
        else:      
            inputfname= (self.haloCat_dirc_name+"/halo_data_sn_%d/MF_data"\
            "_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim))
	        
            if not os.path.exists(inputfname):
                print("MF Directory doesn't exist creating a new one\n")
                os.mkdir(inputfname)

            fname_MF = (inputfname+"KMDPL2_Raptor_allMass_fun_BoxSize_"\
            "%dMpc_NpartSim_%d_snapID_%d.txt"%(self.Lbox, self.NpartSim, self.snapID))
            
            table = [(10.0**self.logMbins_avg), massbindata[massfield[0]], 
            massbindata[massfield[1]], massbindata[massfield[2]], 
            massbindata[massfield[3]],massbindata[massfield[4]]]        
            
            names = ['Mbins_avg', 'Mass_200crit', 'Mass_tot', 'Mass_200mean', 'Mvir', 'Mass_FOF']        
            Formats = {'Mbins_avg':'%0.6e', 'Mass_200crit':'%0.6e', 'Mass_tot':'%0.6e', 
            'Mass_200mean':'%0.6e', 'Mvir':'%0.6e', 'Mass_FOF':'%0.6e'}
            ascii.write(table, fname_MF, names=names, delimiter='\t', formats=Formats, overwrite=True)

	    # To plot Mass function along with analytical function
        if plot_compare:
            vlabels = [r'$\rm Mass_{200crit}$',r'$\rm Mass_{tot}$', 
            r'$\rm Mass_{200mean} $', r'$\rm M_{vir}$',  r'$\rm M_{FOF}$']
            
            plt.figure(1, figsize=(12, 10))

            gs = gridspec.GridSpec(2, 1, hspace=0.0, height_ratios=[3,1])
            ax1 = plt.subplot(gs[0])
            NUM_COLORS = 6
            cm = plt.get_cmap('hsv')
            ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#            ax1.set_prop_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
            mark = ['d', 'o', 's', '^', '>']
            
            model_name = str(self.Fit_model)
            ax1.plot(self.HMF.m, self.HMF.dndlog10m, linestyle='-', color='k', linewidth=2, label=model_name)
            for i in range(len(massfield)):
                ax1.plot(np.power(10.0, self.logMbins_avg), massbindata[massfield[i]]/self.deltaM,
                linestyle='-', linewidth=1.8, mew=1.5, marker=mark[i], ms=6,label=vlabels[i])
            
            ax1.plot(np.power(10.0, self.logMbins_avg), self.MDPL2_MF/self.deltaM,
                linestyle='-', linewidth=1.8, mew=1.5, marker=mark[i], ms=6,label=r"$\rm M_{vir}^{MDPL2}$")


            ax1.axvline(x=1e12, linestyle='--', linewidth=2, color='m', 
                label=r"$10^{12} \rm{M}_\odot$ $\rm{h}^{-1}$")
            ax1.axvline(x=1e11, linestyle='--', linewidth=2, color='peru', 
                label=r"$10^{11} \rm{M}_\odot$ $\rm{h}^{-1}$")
            ax1.axvline(x=1e10, linestyle='--', linewidth=2, color='teal', 
                label=r"$10^{10} \rm{M}_\odot$ $\rm{h}^{-1}$")
                
            ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{M}$ ($\rm{h}^{3}$ $\rm{Mpc}^{-3}$)', fontsize=20)
            ax1.set_xscale("log")
            ax1.set_yscale("log")
            ax1.set_xlim(10**9, 10**16)
            ax1.minorticks_on()
            plt.setp(ax1.get_xticklabels(),visible=False)
            ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
            ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
            ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
            ax1.spines['bottom'].set_linewidth(1.8)
            ax1.spines['left'].set_linewidth(1.8)
            ax1.spines['top'].set_linewidth(1.8)
            ax1.spines['right'].set_linewidth(1.8)
            plt.tight_layout()


            ax2 = plt.subplot(gs[1],  sharex=ax1)

            NUM_COLORS = 5
            cm = plt.get_cmap('hsv')
            ax2.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
            for i in range(len(massfield)):
            
                temp   = massbindata[massfield[i]]/self.deltaM
            #    ratio  = np.abs(temp-self.HMF.dndlog10m)/self.HMF.dndlog10m
                ratio  = temp/self.HMF.dndlog10m
                ax2.plot(np.power(10.0, self.logMbins_avg), ratio, linestyle='-', 
                linewidth=1.8, mew=1.5, marker=mark[i], ms=8, label=vlabels[i])
                 
            temp   = self.MDPL2_MF/self.deltaM
            ratio  = temp/self.HMF.dndlog10m
            ax2.plot(np.power(10.0, self.logMbins_avg), ratio, linestyle='-', 
            linewidth=1.8, mew=1.5, marker=mark[i], ms=8, label=r'$\rm M_{vir}^{MDPL2}$')

            ax2.axhline(y=1., linestyle='--', linewidth=2, color='k') 
 
            ax2.set_xlabel(r'$\rm{M}$ [$\rm{M}_\odot$ $\rm{h}^{-1}$]', fontsize=20)
            #ax2.set_ylabel(r'$\Delta (\%)$', fontsize=20)
            ax2.set_ylabel(r'$\rm HMF_{sim} / HMF_{anal}$', fontsize=20)
            #ax1.set_xscale("log")
            #ax2.set_yscale("log")
            ax1.set_xlim(10**9, 10**15.5)
            ax2.set_ylim(-0.1, 1.1)
            ax2.minorticks_on()
            #ax2.legend(frameon=False, ncol=1, fontsize=16, loc=3)
            ax2.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
            ax2.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
            ax2.spines['bottom'].set_linewidth(1.8)
            ax2.spines['left'].set_linewidth(1.8)
            ax2.spines['top'].set_linewidth(1.8)
            ax2.spines['right'].set_linewidth(1.8)
            plt.tight_layout()

            inputfname = (self.haloCat_dirc_name+"/halo_data_sn_%d/MF_plots"\
            "_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim))
            if not os.path.exists(inputfname): 
                print("MF plots Directory doesn't exist creating a new one\n")
                os.mkdir(inputfname)
                

            plot_name = (inputfname+"KMDPL2_Raptor_CompareMass_fun_BoxSize_"\
            "%dMpc_NpartSim_%d_snapID_%d.png"%(self.Lbox, self.NpartSim, self.snapID))
            plt.savefig(plot_name, dpi=300)

            plt.close()
        


#-------------------------------------------------------------------------------

    def _compute_VF_from_sim(self, logV_min, logV_max, NBINS, save=True, 
                            plot_compare=True, FastRead=True):
    
        """
        """

        #Vmax DistributioNBINS=50
        xlim=[logV_min, logV_max]
        deltaV=(xlim[1]-xlim[0])/np.float64(NBINS)
        xedges=np.arange(xlim[0],xlim[1]+0.5*deltaV,deltaV)
        xbins=np.arange(xlim[0]+0.5*deltaV,xlim[1],deltaV)
        
        if not FastRead:
            #desired velocity field
            fields=['Vmax','sigV']
            bindata=dict()
            #make histograms while converting units to km/s
            for key in fields:
                fac=np.log10(self.vel_unit_conv)
                bindata[key], blah = np.histogram(np.log10(self.halopropdata[key])+fac, xedges)
                bindata[key]       = np.float64(bindata[key])/np.float64(self.Lbox)**3
        else:
            #desired velocity field
            fields=['Vmax(40)','sigV(41)']
            bindata=dict()
            #make histograms while converting units to km/s
            for key in fields:
                fac                = np.log10(self.vel_unit_conv)
                temp               = np.ndarray.flatten(self.halopropdata[key].to_numpy()) 
                bindata[key], blah = np.histogram(np.log10(temp)+fac, xedges)
                bindata[key]       = np.float64(bindata[key])/np.float64(self.Lbox)**3


        if not save:
            return bindata, fields, xbins
        else:
            inputfname= (self.haloCat_dirc_name+"/halo_data_sn_%d/"\
            "MF_data_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim))
            if not os.path.exists(inputfname):
                print("MF Directory doesn't exist creating a new one\n")
                os.mkdir(inputfname)
                

            fname_MF = (inputfname+"KMDPL2_Raptor_allVmax_fun_BoxSize_"\
            "%dMpc_NpartSim_%d_snapID_%d.txt"%(self.Lbox, self.NpartSim, self.snapID))
            

            table = [(10.0**xbins), bindata[fields[0]], bindata[fields[1]]]
            names = ['Vmax_bins', 'Vmax', 'sigV']       
            Formats = {'Vmax_bins':'%0.6e', 'Vmax':'%0.6e', 'sigV':'%0.6e'} 
            ascii.write(table, fname_MF, names=names, delimiter='\t', formats=Formats, overwrite=True)


        if plot_compare:
    	    
            vlabels=[r'$\rm{V}_{\rm{max}}$',r'$\sigma_{\rm{v}}$']
            plt.figure(1, figsize=(8, 6))
            gs = gridspec.GridSpec(1, 1)
            ax1 = plt.subplot(gs[0])
            NUM_COLORS = 4 #len(fields)
            cm = plt.get_cmap('hsv')
            ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
            mark = ['s', 'o', '^', 'd']

            for i in range(len(fields)):
                ax1.plot(np.power(10.0, xbins), bindata[fields[i]]/deltaV,
                linestyle='-', linewidth=1.8, mew=1.5, marker=mark[i], ms=8,label=vlabels[i])

            ax1.plot(np.power(10.0, xbins), self.MDPL2_Vmax/deltaV, linestyle='-', linewidth=1.8, 
            mew=1.5, marker=mark[i], ms=8,label=r'$\rm V_{max}^{MDPL2}$')

            ax1.plot(np.power(10.0, xbins), self.MDPL2_sigmav/deltaV, linestyle='-', linewidth=1.8, 
            mew=1.5, marker=mark[i], ms=8,label=r'$\rm \sigma_{v}^{MDPL2}$')

            ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{V}$ ($\rm{h}^{3}$ $\rm{Mpc}^{-3}$)', fontsize=20)  
            ax1.set_xlabel(r'$\rm{V}$ ($\rm{km}$ $\rm{s}^{-1}$)', fontsize=20)
            ax1.set_xscale("log")
            ax1.set_yscale("log")
            ax1.set_xlim(10, 10**3.6)
            ax1.minorticks_on()
            ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
            ax1.tick_params(axis='both', which='minor', length=5, width=2, labelsize=16)
            ax1.tick_params(axis='both', which='major', length=8, width=2, labelsize=16)
            ax1.spines['bottom'].set_linewidth(1.8)
            ax1.spines['left'].set_linewidth(1.8)
            ax1.spines['top'].set_linewidth(1.8)
            ax1.spines['right'].set_linewidth(1.8)
            plt.tight_layout()
	
            inputfname = (self.haloCat_dirc_name+"/halo_data_sn_%d/MF_plots"\
            "_%dMpc_%d/"%(self.snapID, self.Lbox, self.NpartSim))
            if not os.path.exists(inputfname): 
                print("MF plots Directory doesn't exist creating a new one\n")
                os.mkdir(inputfname)

            plot_name = (inputfname+"KMDPL2_Raptor_Vmax_dis_BoxSize_"\
            "%dMpc_NpartSim_%d_snapID_%d.png"%(self.Lbox, self.NpartSim, self.snapID))
            plt.savefig(plot_name, dpi=300)
            plt.close()

#-------------------------------------------------------------------------------

    def _halo_corr(self, logMhalo_min, logMhalo_max, massdef='Mass_tot(27)', comoving=True):
    
        """
        """

        nbins   = 50
        nthreads= 25
        rmin = 4*(self.Lbox/self.NpartSim/25.)
        rmax = self.Lbox/5.0


        if not massdef:
            massdef = 'Mvir(8)'
        else:
            massdef ='Mass_tot(27)'

        rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
        xi1     = np.zeros(nbins, dtype=np.float64)
        xi      = np.zeros(nbins, dtype=np.float64)
        xi_bar  = np.zeros(nbins, dtype=np.float64)
        ravg    = np.zeros(nbins, dtype=np.float64)
        r_min   = np.zeros(nbins, dtype=np.float64)
        r_max   = np.zeros(nbins, dtype=np.float64)
        npairs  = np.zeros(nbins, dtype=np.float64)
        err     = np.zeros(nbins, dtype=np.float64)
        Avpart     = np.zeros(nbins, dtype=np.float64)


        X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 

        massfac = np.log10(self.mass_unit_conv)
        log_halo_mass = np.log10( np.ndarray.flatten(self.halopropdata[massdef].to_numpy()) )
        log_halo_mass += massfac
        keep = (log_halo_mass>=logMhalo_min )*(log_halo_mass< logMhalo_max)

        X = X[keep]
        Y = Y[keep]
        Z = Z[keep]
        if not comoving:
            X = (X/self.scale_fac)*(self.H0/100.0)
            Y = (Y/self.scale_fac)*(self.H0/100.0)
            Z = (Z/self.scale_fac)*(self.H0/100.0)

        weights1 = np.ones_like(X)   
        numden = np.float64(1.*len(X)/self.Lbox**3)

        xi_counts = xi_corr(self.Lbox, nthreads, rbins, X, Y, Z, weights=weights1, 
        weight_type='pair_product', verbose=True, output_ravg=True)


        count=0
        sum1=0
        sum2=0
        for r in xi_counts: 
            
            vol = (4.0*np.pi/3.0)*( r['rmax']**3.0 - r['rmin']**3.0 )
            avpart= np.float64(len(X)) * vol * numden;
            
            xi1[count]     = (np.float64(r['npairs'])/avpart) - 1.
            sum2          += r['npairs']
            sum1          += avpart
            xi_bar[count]  = (sum2/sum1) - 1.0

            xi[count]      = r['xi']
            npairs[count]  = r['npairs']
            ravg[count]    = r['ravg']
            r_min[count]   = r['rmin']
            r_max[count]   = r['rmax']
            err[count]     = (1.+r['xi'])/np.sqrt(r['npairs'])
            Avpart[count]  = avpart
            count         += 1
            
        del X; del Y; del Z
        dirc_name= (self.haloCat_dirc_name+"/halo_data_sn_%d/"\
        "corr_func_results/"%(self.snapID))

        if not os.path.exists(dirc_name):
            os.mkdir(dirc_name)

        outfile = (dirc_name+"logbin_KMDPL2_Corr_"\
                   "%dMpc%d_SnapID_%d_MhaloMin_%0.1f"\
                   "_MhaloMax_%0.1f_Rmax_%0.1fMpc_Nbr_%d.txt"%(
                   self.Lbox, self.NpartSim, self.snapID, logMhalo_min, 
                   logMhalo_max, rmax, nbins))
                   
                   
        table = [r_min, r_max, ravg, xi, npairs, Avpart, xi1, xi_bar, err]            
        names = ['r_min', 'r_max', 'ravg', 'xi', 'npairs', 'Avpart', 'xi1', 'xi_bar', 'err']        
        Formats = {'r_min':'%2.14f', 'r_max':'%2.14f', 'ravg':'%2.14f', 'xi':'%2.14f', 
        'npairs':'%2.14f', 'Avpart':'%2.14f', 'xi1':'%2.14f', 'xi_bar':'%2.14f', 'err':'%2.14f'}
        ascii.write(table, outfile, names=names, delimiter='\t', formats=Formats, overwrite=True)
        
#-------------------------------------------------------------------------------

    def _halo_Dq_corr(self, logMhalo_min, logMhalo_max, massdef='Mass_tot(27)', comoving=True):

        """
	    """
        nthreads=10
        rmin = 8.0
        print("Lbox{: }, Npart_Sim{: f}".format(self.Lbox, self.NpartSim))
        rmax = self.Lbox/5.0
        print("rmin{: f}, rmax{: f}".format(rmin, rmax))
        nbins    = 20
        rbins    = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
        autocorr = 0
        
        
        ravg    = np.zeros(nbins, dtype=np.float64)
        r_min   = np.zeros(nbins, dtype=np.float64)
        r_max   = np.zeros(nbins, dtype=np.float64)
        npairs  = np.zeros(nbins, dtype=np.float64)
        Avpart  = np.zeros(nbins, dtype=np.float64)

        if not massdef:
            massdef = 'Mvir(8)'
        else:
            massdef ='Mass_tot(27)'

        X  = np.ndarray.flatten(self.halopropdata['Xc(9)'].to_numpy()) 
        Y  = np.ndarray.flatten(self.halopropdata['Yc(10)'].to_numpy()) 
        Z  = np.ndarray.flatten(self.halopropdata['Zc(11)'].to_numpy()) 

        massfac = np.log10(self.mass_unit_conv)
        log_halo_mass = np.log10( np.ndarray.flatten(self.halopropdata[massdef].to_numpy()) )
        log_halo_mass += massfac

        keep = (log_halo_mass>=logMhalo_min )*(log_halo_mass< logMhalo_max)
        
        X1 = X[keep]
        Y1 = Y[keep]
        Z1 = Z[keep]
        if not comoving:
            X1 = (X1/self.scale_fac) * (self.H0/100.0)
            Y1 = (Y1/self.scale_fac) * (self.H0/100.0)
            Z1 = (Z1/self.scale_fac) * (self.H0/100.0)

        
        print("Total number of halos in the mass range {: d}".format(len(X1)))
        NMc = np.int32(input(""))
        
        index = np.random.randint(0, len(X1), size=NMc)
        X2 = np.float64(X[index])
        Y2 = np.float64(Y[index])
        Z2 = np.float64(Z[index])

        weights1 = np.ones_like(X1)   
        numden = np.float64(1.*len(X1)/self.Lbox**3)


        results = DD(autocorr, nthreads, rbins, X1, Y1, Z1, 
                     X2=X2, Y2=Y2, Z2=Z2, verbose=True, 
                     boxsize=np.float64(self.Lbox), output_ravg=True)

        count=0

        for r in results:

            vol = (4.0*np.pi/3.0) * ( r['rmax']**3.0 - r['rmin']**3.0 )
            avpart= np.float64(len(X1)) * vol * numden;
            npairs[count]  = r['npairs']
            ravg[count]    = r['ravg']
            r_min[count]   = r['rmin']
            r_max[count]   = r['rmax']
            Avpart[count]  = avpart
            count+=1

        Cqd = npairs/np.float64(len(X1))/np.float64(NMc)
        Dqd =  np.diff(np.log10(Cqd))
        Dqd = Dqd/np.diff(np.log10(rbins[:-1]))
        
        del X,Y,Z
        del X1,Y1,Z1
        del X2,Y2,Z2

        dirc_name= (self.haloCat_dirc_name+"/halo_data_sn_%d/"\
                    "corrfunc_fractal_results/"%(self.snapID))

        if not os.path.exists(dirc_name):
            os.mkdir(dirc_name)



        outfile = (dirc_name+"/logbin_CorrInt_Lbox_%d_"\
         	       "NparSim_%d_snapID_%d_MhaloMin_%0.1f"\
                   "_MhaloMax_%0.1f_Rmax_%0.1fMpc_Nbr_%d"\
                   "_NMc_%d.dat"%(self.Lbox, self.NpartSim, 
                   self.snapID, logMhalo_min, logMhalo_max, rmax,
                   nbins, NMc))

        table = [r_min[:-1], r_max[:-1], ravg[:-1], Avpart[:-1], npairs[:-1], Cqd[:-1], Dqd]            
        names = ['r_min', 'r_max', 'ravg', 'Avpart', 'npairs', 'Cqd', 'Dqd']        
        Formats = {'r_min':'%2.14f', 'r_max':'%2.14f', 'ravg':'%2.14f', 'Avpart':'%2.14f', 
        'npairs':'%2.14f', 'Cqd':'%2.14f', 'Dqd':'%2.14f'}
        ascii.write(table, outfile, names=names, delimiter='\t', formats=Formats, overwrite=True)
        

#-----------------------------------------------------------------------------------------------------


    def _conv_ascii_to_jay(self):
#        vpt.ConvertASCIIPropertyFileToHDF(self.basefname, iseparatesubfiles=0, iverbose=0)
        name = self.basefname+"_properties.jay"
        if os.path.exists(name):
            raise ValueError("Halo catalog in jay extension already exists")
        else:
            self.halopropdata.to_jay(name)
        
#-------------------------------------------------------------------------------

    def _free_data(self):
        del self.halopropdata

#-------------------------------------------------------------------------------


