#load other useful packages
import numpy as np
import astropy as ap
import pandas as pd
import sys,os,string,time,re,struct
import argparse
from astropy.cosmology import LambdaCDM

#to load specific functions defined in another python file
basecodedir='/mnt/data1/sandeep/velociraptor_halo_catalog/VELOCIraptor_Python_Tools'
sys.path.append(basecodedir)
import velociraptor_python_tools as vpt
#import velociraptor_python_tools_cython as vpt
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
import Cosmo_Growth_Fac_raw as CGF_raw
import readsnap


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize 1024 h^-1 Mpc')
    parser.add_argument("NpartSim", type=int, 
    help='1024^3')
    parser.add_argument("Samp_Size", type=int, 
    help='400000 sub sample size')
    parser.add_argument("n_sample", type=int, 
    help='50 or 100')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 16 for kalinga multidark2')
#
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

    Lbox      = args.Lbox
    NpartSim = args.NpartSim
    Samp_Size = args.Samp_Size
    n_sample  = args.n_sample
    snapID    = args.snapID
    
    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)
    
    inp_file = dir_name +"snapshot_%03d.0"%snapID

    print "+++++++++++++++++ Gadget header ++++++++++++++++++++++++++\n"
    
    snapshot        = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    
    Lbox      = snapshot.BoxSize/1000.
    L_boxhalf = Lbox/2.
    scale_fac = 1./(1.+snapshot.redshift)
    kpc_to_Mpc = 1./1000.
    vel_conv_factor = 1./np.sqrt(scale_fac)
    cosmo  = LambdaCDM(H0= 100.*snapshot.HubbleParam, 
                       Om0 = snapshot.Omega0,
                       Ode0= snapshot.OmegaLambda,
                       Tcmb0=0.)
    
    H_z = cosmo.H(snapshot.redshift).value
     
    #define formats
    ASCIIFORMAT=0
    HDFFORMAT=2
    BINARFORMAT=1
    #base filename 
    inputfname=("../halo_data_%dMpc_%d/halo_data_sn_%d/raptor"\
                "_HC_%dMpc_%d_sn_%03d"%(Lbox, NpartSim, snapID, Lbox, 
                NpartSim, snapID))

    halopropdata,numhalos = vpt.ReadPropertyFile(inputfname, ASCIIFORMAT)
    h = halopropdata['SimulationInfo']['h_val']
    Period = halopropdata['SimulationInfo']['Period']*h # Lbox in h^-1Mpc
    
    np.random.seed(1023947)

    snapfname = ("/mnt/data1/sandeep/Kalinga_run_Analysis/Kalinga_halo_cat"\
                 "/halo_data_%dMpc_%d/halo_data_sn_%d/halo_subsample/"\
                 "%d/RSD/KMDPL2_Halo_subsamp_param_%d.dat"%(Lbox, NpartSim, 
                 snapID, Samp_Size, snapID))
               
    np.savetxt(snapfname, zip([3], [Samp_Size], [n_sample], 
               [numhalos], [snapID], [scale_fac], 
               [snapshot.HubbleParam*100], [Lbox], [L_boxhalf], [Period]), 
               fmt="%d %d %d %d %d %lf %lf %lf %lf %lf") 

    print "h value:", h 
    print "Period value:", Period 
    
    Xc =  np.asarray(halopropdata['Xc']*h, dtype=np.float64)
    Yc =  np.asarray(halopropdata['Yc']*h, dtype=np.float64)
    Zc =  np.asarray(halopropdata['Zc']*h, dtype=np.float64)

    VXc =  np.asarray(halopropdata['VXc'], dtype=np.float64)
    VYc =  np.asarray(halopropdata['VYc'], dtype=np.float64)
    VZc =  np.asarray(halopropdata['VZc'], dtype=np.float64)
    
    
    #--- since gadget2 internal position units are in kpc h^-1
    boost_factor = snapshot.HubbleParam/H_z # In comoving units scaling is \vec{V}_{comoving}.\hat{z}/H(z)
    #Since velociraptor units are in comoving velocities

    #To get comoving Peculiar velocities from Gadget 
    #velocities multiply by this factor


    print "redshfhift: %0.4e Hubble parameter H: %f  factor: %f"%(snapshot.redshift, H_z, boost_factor) 
    Zc += boost_factor*VZc
    Zc  = Zc%Period

    for j in xrange(0, n_sample):
        
        count=0
        halo_pos=np.zeros((Samp_Size, 3), dtype=np.float64)
        halo_vel=np.zeros((Samp_Size, 3), dtype=np.float64)
        indices = np.random.randint(numhalos, size=Samp_Size)
        
        halo_pos[:, 0] = Xc[indices]
        halo_pos[:, 1] = Yc[indices]
        halo_pos[:, 2] = Zc[indices]
        
        halo_vel[:, 0] = VXc[indices]
        halo_vel[:, 1] = VYc[indices]
        halo_vel[:, 2] = VZc[indices]


        halo_pos = np.ndarray.flatten(halo_pos)  
        halo_vel = np.ndarray.flatten(halo_vel)  
        
        temp  = np.array([snapID], dtype=np.float64)
        temp1 = np.array([Period], dtype=np.float64)
        file_data = np.concatenate((temp, temp1, halo_pos, halo_vel, temp1, temp))    
        snapfname = ("/mnt/data1/sandeep/Kalinga_run_Analysis/Kalinga_halo_cat"\
                     "/halo_data_%dMpc_%d/halo_data_sn_%d/halo_subsample/"\
                     "%d/RSD/KMDPL2_SubSam_%d_%d_%d_halo.bin"%(Lbox, NpartSim, 
                     snapID, Samp_Size, snapID, Samp_Size, j))
        print snapfname
        file_data.tofile(snapfname, format="%lf")


if __name__ == "__main__":
    main()


