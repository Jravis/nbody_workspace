/*
    This file is a part of the my_fast_corr
    Copyright (C) 2015-- Sandeep Rana (sandeepranaiiser@gmail.com)
    License: MIT LICENSE. See LICENSE file under the top-level
    It's a class to create mesh, cell list and compute various pairs
    for cosmological calculations.
*/



#ifndef RECTANGULARMESH_H
#define RECTANGULARMESH_H

#pragma once 
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<cmath>
#include<string>
#include <algorithm>
#include <bits/stdc++.h>
#include <sstream> // for ostringstream
#include <exception>
#include <ctime>
#include "ttqdm.h"
#pragma GCC optimize ("Ofast")
#pragma GCC target ("avx,avx2,fma")

using namespace std;  

struct str{
    double value;
    int index;
};

int cmp(const void *a, const void *b)
{

    struct str *a1 = (struct str *)a;
    struct str *a2 = (struct str *)b;
    if ((*a1).value < (*a2).value)
        return -1;
    else if ((*a1).value > (*a2).value)
        return 1;
    else
        return 0;
}

//----------------------------------------------------------------------------------

class RectangularMesh{

//private:
    /*
    Parameters
    ----------
    x1in, y1in, z1in : arrays
        Length-*Npts* arrays containing the spatial position of the *Npts* points.
    xperiod, yperiod, zperiod : double 
        Length scale defining the periodic boundary conditions in each dimension.
        In virtually all realistic cases, these are all equal.
    approx_xcell_size, approx_ycell_size, approx_zcell_size : double
        approximate cell sizes into which the simulation box will be divided.
        These are only approximate because in each dimension,
        the actual cell size must be evenly divide the box size.
    */


    public:

        /* Class initiation for mesh data and routines*/

        int Npts, Nbr;
        bool PBC = true;
        int xperiod, yperiod, zperiod;
        int approx_xcell_size;
        int approx_ycell_size;
        int approx_zcell_size;
        int num_xdivs, num_ydivs, num_zdivs, ncells;
        
        double rmax, rmin;
        double *xin, *yin, *zin; 
        double xcell_size, ycell_size, zcell_size;

        void set_num_divs(){
            num_xdivs = max((int) (lround(xperiod/approx_xcell_size)), 1);
            num_ydivs = max((int) (lround(yperiod/approx_ycell_size)), 1);
            num_zdivs = max((int) (lround(zperiod/approx_zcell_size)), 1);
            ncells = num_xdivs*num_ydivs*num_zdivs;
        }

        void set_cell_size(){
            xcell_size = (double) xperiod / (double) num_xdivs;
            ycell_size = (double) yperiod / (double) num_ydivs;
            zcell_size = (double) zperiod / (double) num_zdivs;
        }
        
        int digitized_position(double p, double cell_size, int num_divs);  

        /* Make cell list */

        int *head, *lscl; 
        void ini_cell_list(){ 
            head = new int [ncells];
            lscl = new int [Npts]; 
            memset(head, -1, ncells*sizeof(int));
            memset(lscl, 0, Npts*sizeof(int));
        }
        //double *sortx, *sorty, *sortz; 
        void make_cellList(); 
        
               
        //Setting bins and computing bin volume also

        bool LogBin = true; 
        double *rbins;
        void binning(){
            double temp=0., bin_fac=0.;
            rbins = new double [Nbr];
            if (LogBin){
                bin_fac = (log10(rmax)-log10(rmin))/(double)(Nbr);
                for(int l=0; l < Nbr+1; l++){
                    temp    = log10(rmin) + l * bin_fac;
                    rbins[l] =  (pow(10., temp));
                }
            }
            else{
                bin_fac = (rmax-rmin)/(double)(Nbr);
                for(int l=0; l < Nbr+1; l++){
                    temp    = (rmin) + l * bin_fac;
                    rbins[l] =  temp;
                }
            }

        }//binning()
        
        int Nbmu;
        void Gen_Count_Pairs_rmu(double *xpos,double *ypos, double *zpos,long int *npairs_r);

//------------------------------------------
        
        int *rand_ind, Nsphere;
        void get_random_indx(){
        
            rand_ind = new int [Nsphere]; 
            const int range_from = 0;
            const int range_to   = Npts; 
            seed_seq                            seed{777777};
            mt19937_64                          gen(seed);
            uniform_real_distribution<double>  dist(range_from, range_to);
            
            for(int nn=0; nn<Nsphere; nn++)
                rand_ind[nn] = (int) dist(gen)  ;
        }
};

#endif
//----------------------------------------------------------------------------------

/* All class routines are here */


int RectangularMesh::digitized_position(double p, double cell_size, int num_divs)
{
    int ip;
    ip = (int) floor(p/cell_size);
    
    if(ip>=num_divs){
        //cout<<"Position index is greater than the cell index"<<"\n";
        //exit(0);
        ip=num_divs-1; 
    }
    //if(ip<0)
    //    ip = num_xdivs+ip;
    return ip;
}

//--------------------------------------------------------------------------------------

void RectangularMesh::make_cellList()
{
    cout<<"\n";
    cout<<"-------------------------Starting cell List---------------------------------"<<"\n";
    int ix, iy, iz, icell;
    
    for(int nn=0; nn<Npts; nn++){

        ix = digitized_position(xin[nn], xcell_size, num_xdivs);
        iy = digitized_position(yin[nn], ycell_size, num_ydivs);
        iz = digitized_position(zin[nn], zcell_size, num_zdivs); 
        icell = ix*num_ydivs*num_zdivs+iy*num_zdivs+iz;
        lscl[nn] = head[icell];
        head[icell] = nn;
    }//for nn
    cout<<"\n";
    cout<<"------------------------- done with cell list---------------------------------"<<"\n";
    /*
    cout<<"sorting positions such that z position is in increasing"<<"\n";
    struct str *objects = new str [Npts];
    for (int i = 0; i < Npts; i++){
        objects[i].value = zin[i];
        objects[i].index = i;
    }
    //sort objects array according to value maybe using qsort
    qsort(objects, Npts, sizeof(objects[0]), cmp);
    
    sortx = new double [Npts];
    sorty = new double [Npts];
    sortz = new double [Npts];
    
    for (int i = 0; i < Npts; i++){
        sortx[i] = xin[objects[i].index];
        sorty[i] = yin[objects[i].index];
        sortz[i] = zin[objects[i].index];
    }
    delete xin;
    delete yin;
    delete zin;
    */
}

//--------------------------------------------------------------------------------------

void RectangularMesh::Gen_Count_Pairs_rmu(double *xpos, double *ypos, double *zpos, long int *npairs_rmu)
{
    
    cout<<"\n";
    cout<<"------------------------- Counting pairs  ------------------------------"<<"\n";
    
    //* making small data copy here for paralle computation

    double delta_rr;
    double Rmin = rmin;
    double Rmax = rmax;
    int nbr = Nbr;
    int nb_mu = Nbmu;
    int Lx = xperiod, Ly=yperiod, Lz=zperiod;
    int nmesh_x=num_xdivs, nmesh_y=num_ydivs, nmesh_z=num_zdivs, Ncells=ncells;
    int *head_omp, *lscl_omp; 

    if(LogBin)
        delta_rr = log10(rmax/rmin)/(double) Nbr;
    else
        delta_rr = (rmax-rmin)/(double) Nbr;
    double mu_max = 1.0;
    double mu_min = 0.0;
    double delta_mu = (mu_max-mu_min)/(double) Nbmu;
    cout<<"\n";

    head_omp = new int [ncells];
    lscl_omp = new int [Npts]; 
    
    for(int ht=0; ht<ncells;ht++)
        head_omp[ht] = head[ht];
    
    for(int ht=0; ht<Npts;ht++)
        lscl_omp[ht] = lscl[ht];

    cout<<"------------------------- starting omp parallel ------------------------------"<<"\n";

#pragma omp parallel default(none)  \
    shared(head_omp, lscl_omp, xpos, ypos, zpos, Rmin, Rmax, Ncells,\
            Lx, Ly, Lz, nmesh_x, nmesh_y, mu_max, mu_min,\
            nmesh_z, delta_rr, nbr, nb_mu, npairs_rmu, delta_mu)
 {
    
    //tqdm bar;

    long int pairs_rmu_private[nbr*nb_mu]; //Histogram filled by each thread for log(r)
    for(int ht= 0 ; ht<nbr*nb_mu; ht++)
        pairs_rmu_private[ht]=0;


    int64_t bin_refine_factor = 1;
                
#pragma omp for nowait schedule(dynamic)
    for(int64_t nn=0; nn<Ncells; nn++){
        //bar.progress(nn, Ncells);
        const int iz = nn%nmesh_z;
        const int ix = nn/(nmesh_y*nmesh_z);
        const int iy = (nn - iz - ix*nmesh_z*nmesh_y)/nmesh_z;

        //assert( ((iz + nmesh_z*iy + nmesh_z*nmesh_y*ix) == nn) && "Index reconstruction is wrong");

        for(int iix=-bin_refine_factor;iix<=bin_refine_factor;iix++){
            int iiix;
            double off_xwrap=0.0;
            if(ix + iix >= nmesh_x) {
                 off_xwrap = -Lx;
             } else if (ix + iix < 0) {
                 off_xwrap = Lx;
             }
             iiix=(ix+iix+nmesh_x)%nmesh_x;

            for(int iiy=-bin_refine_factor;iiy<=bin_refine_factor;iiy++){
                int iiiy;
                double off_ywrap = 0.0;
                if(iy + iiy >= nmesh_y) {
                    off_ywrap = -Ly;
                } else if (iy + iiy < 0) {
                    off_ywrap = Ly;
                }
                iiiy=(iy+iiy+nmesh_y)%nmesh_y;
                for(int iiz=-bin_refine_factor;iiz<=bin_refine_factor;iiz++){
                    int iiiz;
                    double off_zwrap = 0.0;
                    if(iz + iiz >= nmesh_z) {
                         off_zwrap = -Lz;
                     } else if (iz + iiz < 0) {
                         off_zwrap = Lz;
                     }
                    iiiz=(iz+iiz+nmesh_z)%nmesh_z;
                    
       //             assert(iiix >= 0 && iiix < nmesh_x && iiiy >= 0 && iiiy < nmesh_y && iiiz >= 0 
        //                   && iiiz < nmesh_z && "Checking that the second pointer is in     range");
                     
                     /* traversing Cell list part */
                     int64_t icell = iiix*nmesh_y*nmesh_z + iiiy*nmesh_z + iiiz;  
                     int i, j, ir, icth; //nnei
                     double dx, dy, dz, rsqr; 
                     double mu;
                     
                     i = head_omp[nn];
                     while(i!=-1){
                        j = head_omp[icell];
                        while(j!=-1){
                            //if (i<j){
                                dz = (zpos[i]+off_zwrap)-zpos[j];
                                dy = (ypos[i]+off_ywrap)-ypos[j];
                                dx = (xpos[i]+off_xwrap)-xpos[j];

                                rsqr =  dx*dx + dy*dy + dz*dz;

                                //************************log(r)-mu*********************
                                if(sqrt(rsqr) >= Rmax || sqrt(rsqr) < Rmin){
                                        j = lscl_omp[j];
                                        continue;
                                }//rmax
                                mu  = fabs(dz)/sqrt(rsqr);
                                ir   = (int)(log10(sqrt(rsqr)/Rmin)/delta_rr);
                                if(mu <= mu_max && mu >= mu_min){
                                    icth = (int)((mu-mu_min)/delta_mu);
                                    pairs_rmu_private[ir+nbr*icth] +=1;
                                }//mu
                            //}// if i<j
                            j = lscl_omp[j];
                        }//while j
                        i = lscl_omp[i];
                    }// while j
                }//iiz loop over bin_refine_factor
            }//iiy loop over bin_refine_factor
        }//iix loop over bin_refine_factor
    }//for cell_ind

#pragma omp critical
    {
        const uint64_t int_fac = 1;
        for( int hr= 0 ; hr<nbr; hr++)//Check bound
        for( int hu= 0 ; hu<nb_mu; hu++){//Check bound
            npairs_rmu[hr+nbr*hu]+= (int_fac*pairs_rmu_private[hr+nbr*hu]); //Add private histograms to shared one
        }

    }//end critical
}// end pragma omp parallel
    cout<<"\n";
    cout<<"------------------------- done omp parallel ------------------------------"<<"\n";

    delete head_omp;
    delete lscl_omp;
}// Count_Pairs_Corr




