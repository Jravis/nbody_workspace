#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<cmath>
#include<string>
#include <algorithm>
#include <bits/stdc++.h>
#include <sstream> // for ostringstream
#include <exception>
#include <ctime>
//#include "tqdm.cpp/include/tqdm/tqdm.h"
#include "ttqdm.h"
#include "Gen_Mesh_cls.h"
#pragma GCC optimize ("Ofast")
#pragma GCC target ("avx,avx2,fma")


using namespace std;  

//--------------- Declare global varables------------------------


char *filename1;
char *filename2;
double *pos, *vel, Lbox, rmax, rmin, Lbox_half, scale_factor, HubbleParam;
double *xpos, *ypos, *zpos;
int nsub, samp_size, ndim=3, ndm, snapid, npart_sim;

//----------------------------------------------------------------------------------

int main(int argc, char **argv)
{



    if(argc != 7){
        cout<< "\n./a.out   [snapid] [Nbr]  [sub_samp_size] [npartsim] [boxsize] [Nbmu] "<<"\n";
        cout<< "\n./a.out     17      50       400000           512       1024    50 "<<"\n";
        return(-1);
    }//if

    
    //-------------------------------Setting up data---------------------------

    snapid = atoi(argv[1]);
    samp_size = atoi(argv[3]);
    npart_sim = atoi(argv[4]);
    Lbox   = (double) atoi(argv[5]);
    rmax   = Lbox/5.;

    pos = new double [samp_size*ndim];
    xpos = new double [samp_size];
    ypos = new double [samp_size];
    zpos = new double [samp_size];
    vel  = new double [samp_size*ndim];
    memset(pos, 0, samp_size*ndim*sizeof(double));
    
    
    filename1  = (char *) malloc(500*sizeof(char));
    filename2  = (char *) malloc(500*sizeof(char));
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/subsamp_param_%s.dat", 
           (int) Lbox, npart_sim, samp_size, snapid, argv[1]);
           

    cout << "\n";
    cout << "---------------------------- Setting up input data ---------------------------\n";
    cout << "\n";
    cout << "Opening file to read params: "<< filename1 << "\n";
  
    FILE  *param_file=NULL;
    param_file = fopen(filename1, "r");
  
    fscanf(param_file,"%d %d %d %d %d %lf %lf %lf %lf\n", 
            &ndim, &samp_size, &nsub, &ndm, &snapid, &scale_factor, 
            &HubbleParam, &Lbox, &Lbox_half);
    fclose(param_file);
 
    double softening_length;
    softening_length = (Lbox*1000.)/npart_sim;
    softening_length *= (1./25.);

    rmin = 2.*(softening_length/1000.);
    double numden=(double)(samp_size)/(Lbox*Lbox*Lbox);


    cout << "\n";
    cout << "-----------------------------------\n";
    cout<<" "<<"samp_size       : "<< samp_size<<"\n" ;
    cout<<" "<<"snapid          : "<< snapid <<"\n" ;
    cout<<" "<<"ndm             : "<< ndm <<"\n" ;
    cout<<" "<<"Lbox            : "<< Lbox <<"\n"; 
    cout<<" "<<"scalefac        : "<< scale_factor <<"\n"; 
    cout<<" "<<"h               : "<< HubbleParam <<"\n"; 
    cout<<" "<<"rmin(h^-1 Mpc)  : "<< rmin <<"\n"; 
    cout<<" "<<"rmax(h^-1 Mpc)  : "<< rmax <<"\n"; 
    cout<<" "<<"numden          : "<< numden <<"\n"; 
    cout<<" "<<"softening length in kpc h^-1: " << softening_length << "\n";
    cout << "------------------------------------\n";
    cout << "\n";


    //-------------------------- Reading Positions ---------------------------

    cout << "---------------------------- Reading positions ---------------------------\n";
    cout << "\n";

    //sprintf(filename1, "KMDPL2_SubSam_%d_%d_%d_comov.bin",(int) snapid,(int) samp_size,0);


    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/KMDPL2_SubSam_%d_%d_%d_phy.bin", 
            (int) Lbox, npart_sim, samp_size, snapid, snapid, samp_size, 0);

    cout<<"Opening sample file for reading: " << filename1<<"\n"; 

    FILE *snapfile = NULL;
    int nblock_start, nblock_end;

    snapfile = fopen(filename1, "r");

    cout << "\n";
    fread(&nblock_start,sizeof(int),1,snapfile);
    fread(pos,sizeof(double), samp_size*ndim, snapfile);
    fread(vel,sizeof(double), samp_size*ndim, snapfile);
    fread(&nblock_end, sizeof(int),1, snapfile);

    if( nblock_start != nblock_end ) {
       printf("MISMATCH ... snapid=%d ... QUITTING \n", nblock_start);
       return(-1);
    }//if
    fclose(snapfile);

    for(int ii =0; ii<samp_size;ii++){
        xpos[ii] = pos[0+ndim*ii];
        ypos[ii] = pos[1+ndim*ii];
        zpos[ii] = pos[2+ndim*ii];
    }
    delete pos; 
    delete vel;

    //--------------------------Class Initiation---------------------------

    RectangularMesh my_Mesh;
    
    my_Mesh.Npts = samp_size;
    my_Mesh.Nbr  = atoi(argv[2]);
    my_Mesh.Nbmu  = atoi(argv[6]);
    my_Mesh.rmax = rmax; 
    my_Mesh.rmin = rmin; 
    
    my_Mesh.xin  = xpos;
    my_Mesh.yin  = ypos;
    my_Mesh.zin  = zpos;
    
    my_Mesh.xperiod = (int) Lbox;
    my_Mesh.yperiod = (int) Lbox;
    my_Mesh.zperiod = (int) Lbox;
    
    my_Mesh.approx_xcell_size = (int) lround(rmax);
    my_Mesh.approx_ycell_size = (int) lround(rmax);
    my_Mesh.approx_zcell_size = (int) lround(rmax);
    my_Mesh.set_num_divs();
    my_Mesh.set_cell_size();
    
    cout << "---------------------------- Mesh specification ---------------------------\n";
    cout << "\n";


    cout<<" "<<"Total number of points: " << my_Mesh.Npts<<"\n";
    cout<<" "<< "Lx: " << my_Mesh.xperiod <<"\n";
    cout<<" "<< "Ly: " << my_Mesh.yperiod <<"\n";
    cout<<" "<< "Lz: " << my_Mesh.zperiod <<"\n";

    cout<<"-------------------------------------------"<<"\n";

    cout<<" "<<"Total number of cells: " << my_Mesh.ncells<<"\n";
    cout<<" "<< "no of x divs: " << my_Mesh.num_xdivs <<"\n";
    cout<<" "<< "no of y divs: " << my_Mesh.num_ydivs <<"\n";
    cout<<" "<< "no of z divs: " << my_Mesh.num_zdivs <<"\n";

    cout<<"-------------------------------------------"<<"\n";

    cout<<" "<< "x dim cell size: " << my_Mesh.xcell_size <<"\n";
    cout<<" "<< "y dim cell size: " << my_Mesh.ycell_size <<"\n";
    cout<<" "<< "z dim cell size: " << my_Mesh.zcell_size <<"\n";

    cout<<"-------------------------------------------"<<"\n";


    /* This section is for various correaltion computation*/

    cout << "\n";
    
    cout << "--------------- Computing npairs and multipole of correlation function in r-mu bins ---------------------\n";
    cout << "\n";
    my_Mesh.ini_cell_list();
    my_Mesh.make_cellList();
    my_Mesh.binning();


    double vol_rmu[my_Mesh.Nbr*my_Mesh.Nbmu]       ; // nbin1=Nbr(radial), nbin2=Nb_mu(bin in mu)
    //Computing effective volume of the cylinder in (r, mu) coordinate system
    double dmu = 1./ (double) my_Mesh.Nbmu; // as mu is in [0,1]
    double mu_bin[my_Mesh.Nbmu]; 
    for(int l=0; l < my_Mesh.Nbmu+1; l++)
        mu_bin[l] =  l * dmu;
    


    //Computing effective volume of the cylinder in (r, mu) coordinate system
    for(int ii=0; ii < my_Mesh.Nbr; ii++){
        for(int jj=0; jj < my_Mesh.Nbmu; jj++){

    //        dmu = mu_bin[j+1]-mu_bin[j] ; // mu_bins diff

            vol_rmu[ii + my_Mesh.Nbr*jj] = (2 * M_PI/3.0) * dmu * (pow(my_Mesh.rbins[ii+1],3.0) - pow(my_Mesh.rbins[ii],3.0));
            vol_rmu[ii + my_Mesh.Nbr*jj] = 2 * vol_rmu[ii+my_Mesh.Nbr*jj]; // The factor of 2 is for 
        } 
    }

    //--------------------------------------------------------------

    time_t start_t;
    time_t end_t;

    time(&start_t);
    long int *pairs_rmu = new long int [my_Mesh.Nbr*my_Mesh.Nbmu];
    for(int ii =0; ii<my_Mesh.Nbr*my_Mesh.Nbmu; ii++)
        pairs_rmu[ii]=0;
    
    my_Mesh.Gen_Count_Pairs_rmu(xpos, ypos, zpos, pairs_rmu);
    time(&end_t);

    cout<<"\n";
    cout<< "It took npair counting: " << (end_t-start_t)/60. << " minutes for nlist \n";
    cout<<endl;

    
    cout<<"\n";
    cout << "---------------------------- make CF ---------------------------\n";
    cout<<"\n";

    /*
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_1Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat",(int)Lbox, (int)npart_sim,(int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size, rmax, (int)Nbr, (int)Nb_mu, (int)k);
    */      

    sprintf(filename1, "logbin_1Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat", snapid, samp_size, rmax, my_Mesh.Nbr, my_Mesh.Nbmu, 0);
    cout<<filename1<<"\n"; 

    /* 
    sprintf(filename2, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_2Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat",(int)Lbox, (int)npart_sim,(int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size, rmax, (int)Nbr, (int)Nb_mu, (int)k);
    */

    sprintf(filename2, "logbin_2Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat", snapid, samp_size, rmax, my_Mesh.Nbr, my_Mesh.Nbmu, 0);
    cout<<filename2<<"\n"; 
     
    snapfile = fopen(filename1, "w");
    fprintf(snapfile,"#rbin  rbin_mid  pairs  avpairs  Xi_0  Xi_2"  
            "  X_4  Xi_par  Xi_perp  Xi_bar  err_Xi\n");
            
    FILE *corr_rmu_file = NULL;
    corr_rmu_file = fopen(filename2, "w");
    fprintf(corr_rmu_file,"#rbin  mubin  rmu_avpairs  rmu_pairs  xi_rmu\n");



    double xi_0   = 0.0, xi_2 = 0.0, xi_4 = 0.0; // multipoles of correlation function
    double xi_rperp= 0.0, xi_rpar = 0.0; // clustering wedge
    double P2=0.0, P4=0.0, rbin_mid=0; 
    double  sum1=0.0, sum2=0.0, sum3 =0.0,sum4=0.0;
    double xibar, xi_rmu, avpart;    
    tqdm bar;
    for(int i=0; i< my_Mesh.Nbr; i++) {

        bar.progress(i, my_Mesh.Nbr);
        sum1   = 0.0, sum2 = 0.0;
        xi_0   = 0.0, xi_2 = 0.0, xi_4 = 0.0; 
        xi_rperp= 0.0, xi_rpar = 0.0; 

        for(int j=0; j< my_Mesh.Nbmu; j++) {
            
            dmu    =  mu_bin[j+1]-mu_bin[j];

            avpart = ((double) samp_size) * vol_rmu[i+my_Mesh.Nbr*j] * numden;
            xi_rmu = ((double) pairs_rmu[i+my_Mesh.Nbr*j]/avpart) - 1;

            if((mu_bin[j]<0.5) && (mu_bin[j]>=0)) // For clustering Wedge see Kayzin et al. 2012
                xi_rperp += xi_rmu/dmu; //if

            if((mu_bin[j]<1) && (mu_bin[j]>=0.5))
                xi_rpar += xi_rmu/dmu; //if
        
            //  Legendre polynomial \ell =2 and \ell=4 
        
            P2 = (3. * mu_bin[j]*mu_bin[j] - 1.)*0.5;
            P4 = (35. * pow(mu_bin[j], 4) - 30. * pow(mu_bin[i], 2.)+ 3)/8.;
            
            sum1 += (double) pairs_rmu[i+my_Mesh.Nbr*j];     //Total number of pairs in a rbin 
            sum2 += avpart;                         //Total average number of pairs in a rbin 
        
            xi_0 += xi_rmu * dmu * 0.5  ;    // monopole  since intergral instead of -1 to 1 it's 0 to 1
            xi_2 += xi_rmu * dmu * P2 * (2*2. + 1.)/2. ;   //quadrapole
            xi_4 += xi_rmu * dmu * P4 * (2*4. + 1.)/2. ;   //hexadecapole

            fprintf(corr_rmu_file,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
                    my_Mesh.rbins[i], mu_bin[j], avpart, (double) pairs_rmu[i+my_Mesh.Nbr*j], 
                    xi_rmu);
        }//for j mu bin
      
        sum3     += sum1;
        sum4     += sum2;
        
        xibar    = (sum3/sum4) - 1.0;
        rbin_mid  = (my_Mesh.rbins[i]+my_Mesh.rbins[i+1])*0.5;

        fprintf(snapfile,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f"  
              "  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
              my_Mesh.rbins[i], rbin_mid, sum1, sum2, 2.*xi_0, 2.*xi_2, 2.*xi_4, 
              xi_rpar, xi_rperp, xibar, (1. + 2.*xi_0)/sqrt(sum1) );
    }//for i rbin
    

    return 0;
}

