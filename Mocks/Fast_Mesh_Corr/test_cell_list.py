import numpy as np
import rectangular_mesh as my_mesh

import matplotlib.pyplot as plt

def digitized_position(p, cell_size, num_divs):
    """ Function returns a discretized spatial position of input point(s).
    """
    ip = np.floor(p // cell_size).astype(int)
    return np.where(ip >= num_divs, num_divs-1, ip)




np.random.seed(71)
Lbox = 1000.0
rmax = Lbox/2.

x = np.random.uniform(0, Lbox, 10000)
y = np.random.uniform(0, Lbox, 10000)
z = np.random.uniform(0, Lbox, 10000)
#plt.scatter(x, y, s=1, lw=0)
#plt.show()

mesh = my_mesh.RectangularMesh(x, y, z, Lbox, Lbox, Lbox, rmax, rmax, rmax, Periodic = False)
#print mesh.ncells, mesh.num_xdivs

mesh.make_cellList()
#print len(mesh.head)
#print len(mesh.head)
#print"\n"
#print mesh.lscl

"""
ncell_range = np.arange(mesh.num_xdivs)
import itertools

for ix, iy, iz in itertools.product(ncell_range, ncell_range, ncell_range):
    ic = mesh.cell_id_from_cell_tuple(ix, iy, iz)
    #print ic, ix, iy, iz
    for ii in xrange(ic-1, ic+1):
        for jj in xrange(ic-1, ic+1):
            for kk in xrange(ic-1, ic+1):
               # print ii, jj, kk
                
                if ii < 0:     
                    rshift_ii = -1.0*Lbox
                elif ii>=mesh.num_xdivs:
                    rshift_ii = Lbox
                else:
                    rshift_ii = 0
                
                if jj < 0:     
                    rshift_jj = -1.0*Lbox
                elif jj>=mesh.num_ydivs:
                    rshift_jj = Lbox
                else:
                    rshift_jj = 0
                
                if kk < 0:     
                    rshift_kk = -1.0*Lbox
                elif kk>=mesh.num_zdivs:
                    rshift_kk = Lbox
                else:
                    rshift_kk = 0
 
                idx = ((ii + mesh.num_xdivs)%mesh.num_xdivs)
                idy = ((jj + mesh.num_ydivs)%mesh.num_ydivs)
                idz = ((kk + mesh.num_zdivs)%mesh.num_zdivs)
                
                cl = mesh.cell_id_from_cell_tuple(idx, idy, idz)

                i = mesh.head[ic]
                while i!=-1:
                    j = mesh.head[cl]
                    while j!=-1:
                        if i<j:
                            rx = x[i]-(x[j]+rshift_ii)
                            ry = y[i]-(y[j]+rshift_jj)
                            rz = z[i]-(z[j]+rshift_kk)
                            rij = np.sqrt(rx**2+ry**2+rz**2)
                            if rij<rmax:
                                print i, j, rij, rmax
                        j=mesh.lscl[j]
                    i=mesh.lscl[i]
                    



""" 

#Usually the linked list (LIST, HEAD) is used to generate a Verlet list

nlistbeg=0
nList_point = np.zeros(mesh.npts, dtype=np.int32)
nList_pair  = []

for i in xrange(mesh.npts):

    ix = digitized_position(x[i], mesh.xcell_size, mesh.num_xdivs)
    iy = digitized_position(y[i], mesh.ycell_size, mesh.num_ydivs)
    iz = digitized_position(z[i], mesh.zcell_size, mesh.num_zdivs)
    nnei=0
    for ii in xrange(ix-1, ix+2):
        for jj in xrange(iy-1, iy+2):
            for kk in xrange(iz-1, iz+2):
                if ii >= mesh.num_xdivs:     
                    idx = ii-mesh.num_xdivs
                elif ii< 0:
                    idx = ii+mesh.num_xdivs
                else:
                    idx = ii 

                if   jj >= mesh.num_ydivs:     
                     idy = jj-mesh.num_ydivs
                elif jj< 0:
                     idy = jj+mesh.num_ydivs
                else:
                     idy = jj 
                    
                if kk >= mesh.num_zdivs:     
                    idz = kk-mesh.num_zdivs
                elif kk< 0:
                    idz = kk+mesh.num_xdivs
                else:
                    idz = kk
                icell = mesh.cell_id_from_cell_tuple(idx, idy, idz)
                j = mesh.head[icell]
                while j!=-1:
                    if i<j:
                        rx = x[i]-(x[j])
                        ry = y[i]-(y[j])
                        rz = z[i]-(z[j])
                        rij = np.sqrt(rx**2+ry**2+rz**2)
                        #rr.append(rij)
                        if rij<=rmax:
                            nnei+=1
                            nList_pair.append(j)
                    j=mesh.lscl[j]
    
#    print nlistbeg, i
    #nList[nlistbeg]=nnei
    nList_point[i]=nnei
    #nlistbeg = nlistbeg+nnei+1
#    print nlistbeg, i

"""
#print nList_point
nList_pair = np.asarray(nList_pair)
ini = 0
fin = 0
#print nList_pair

for i in xrange(mesh.npts):
    fin = ini+nList_point[i]
    print nList_point[i],nList_pair[ini:fin] 
    ini+=nList_point[i]
"""       
