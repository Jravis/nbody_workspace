#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# used Alvaro Orsi base code                                    #
#################################################################



import matplotlib

from matplotlib.transforms import Affine2D

import mpl_toolkits.axisartist.floating_axes as floating_axes

import numpy as np

import mpl_toolkits.axisartist.angle_helper as angle_helper

from matplotlib.projections import PolarAxes

from mpl_toolkits.axisartist.grid_finder import (FixedLocator, MaxNLocator,

                                                 DictFormatter)

import matplotlib.pyplot as plt

from matplotlib import pyplot, transforms




font = {'family' : 'STIXGeneral',

        'size'   : 22}

matplotlib.rc('font', **font)


from astropy.cosmology import FlatLambdaCDM
OmegaMatter = 0.3
H_0 = 0.7*100.
cosmo = FlatLambdaCDM(H0=H_0, Om=OmegaMatter



def setup_axes2(fig, rect,tmin, tmax,zmin,zmax):

  """

  With custom locator and formatter.

  Note that the extreme values are swapped.

  """


  tr = PolarAxes.PolarTransform()

  pi = np.pi


  angle_ticks = [(tmin, '%.2f' % tmin), (0,r'$0$'), (tmax, '%.2f' % tmax)]



  grid_locator1 = FixedLocator([v for v, s in angle_ticks])

  tick_formatter1 = DictFormatter(dict(angle_ticks))



  grid_locator2 = MaxNLocator(4)



  grid_helper = floating_axes.GridHelperCurveLinear(

      tr, extremes=(tmax, tmin, zmax, zmin),

      grid_locator1=grid_locator1,

      grid_locator2=grid_locator2,

      tick_formatter1=tick_formatter1,

      tick_formatter2=None)



  ax1 = floating_axes.FloatingSubplot(fig, rect, grid_helper=grid_helper)

  fig.add_subplot(ax1)



  # create a parasite axes whose transData in RA, cz

  aux_ax = ax1.get_aux_axes(tr)



  aux_ax.patch = ax1.patch  # for aux_ax to have a clip path as in ax

  ax1.patch.zorder = 0.95  # but this has a side effect that the patch is

  # drawn twice, and possibly over some other

  # artists. So, we decrease the zorder a bit to

  # prevent this.



  return ax1, aux_ax



fig = plt.figure(1,figsize=(15,15))



ax, aux_ax = setup_axes2(fig, 111,tmin,tmax,zmin,zmax)

aux_ax.plot(tgal,zgal,'.',color='k',markersize=3)
ax.axis["left"].set_axis_direction("top")
ax.axis["left"].toggle(ticklabels=True, label=True)

ax.axis["left"].major_ticklabels.set_axis_direction("top")
ax.axis["left"].label.set_axis_direction("top")



ax.axis["right"].major_ticklabels.set_color("white")
ax.axis["right"].set_axis_direction("bottom")
ax.axis["right"].toggle(ticklabels=True, label=True)





ax.axis['right'].label.set_visible(True)
ax.axis['right'].label.set_text(r'${\rm Mpc/}h$')

ax.axis['left'].label.set_visible(True)
ax.axis['left'].label.set_text('redshift')
