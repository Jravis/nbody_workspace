#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# used Alvaro Orsi base code                                    #
#################################################################



import matplotlib

from matplotlib.transforms import Affine2D

import mpl_toolkits.axisartist.floating_axes as floating_axes

import numpy as np

import mpl_toolkits.axisartist.angle_helper as angle_helper

from matplotlib.projections import PolarAxes

from mpl_toolkits.axisartist.grid_finder import (FixedLocator, MaxNLocator,

                                                 DictFormatter)

import matplotlib.pyplot as plt

from matplotlib import pyplot, transforms

from astropy.cosmology import FlatLambdaCDM
OmegaMatter = 0.3
H_0 = 0.7*100.
cosmo = FlatLambdaCDM(H0=H_0, Om0=OmegaMatter)

font = {'family' : 'STIXGeneral',

        'size'   : 18}

matplotlib.rc('font', **font)

#plt.style.use("classic")


#---------------------------------------------------------------------

def setup_axes3(fig, rect, ra_min, ra_max, czmin, czmax):

    """

    Sometimes, things like axis_direction need to be adjusted.

    """

    # rotate a bit for better orientation
    tr_rotate = Affine2D().translate(-90, 0)

    # scale degree to radians

    tr_scale = Affine2D().scale(np.pi/180., 1.)
    tr = tr_rotate + tr_scale + PolarAxes.PolarTransform()

    grid_locator1 = angle_helper.LocatorHMS(4)
    tick_formatter1 = angle_helper.FormatterHMS()
    grid_locator2 = MaxNLocator(3)

    # Specify theta limits in degrees

    ra0, ra1 = 7.5*15, 16.7*15
    #ra0, ra1 = ra_min, ra_max

    # Specify radial limits
    #cz0, cz1 = 0, 15500
    cz0, cz1 = 0, czmax

    grid_helper = floating_axes.GridHelperCurveLinear(

        tr, extremes=(ra0, ra1, cz0, cz1),
        grid_locator1=grid_locator1,
        grid_locator2=grid_locator2,
        tick_formatter1=tick_formatter1,
        tick_formatter2=None)



    ax1 = floating_axes.FloatingSubplot(fig, rect, grid_helper=grid_helper)

    fig.add_subplot(ax1)



    # adjust axis

    ax1.axis["left"].set_axis_direction("bottom")
    ax1.axis["right"].set_axis_direction("top")



    ax1.axis["bottom"].set_visible(False)

    ax1.axis["top"].set_axis_direction("bottom")

    ax1.axis["top"].toggle(ticklabels=True, label=True)

    ax1.axis["top"].major_ticklabels.set_axis_direction("top")

    ax1.axis["top"].label.set_axis_direction("top")


    ax1.axis["left"].label.set_text(r"cz [km s$^{-1}$]")

    ax1.axis["top"].label.set_text(r"$\alpha_{1950}$")



    # create a parasite axes whose transData in RA, cz

    aux_ax = ax1.get_aux_axes(tr)

    aux_ax.patch = ax1.patch  # for aux_ax to have a clip path as in ax

    ax1.patch.zorder = 0.9  # but this has a side effect that the patch is

    # drawn twice, and possibly over some other

    # artists. So, we decrease the zorder a bit to

    # prevent this.

    return ax1, aux_ax



def main():

  from astropy import units as u
  from astropy.coordinates import SkyCoord


  ra_blue    = np.genfromtxt("./data/blue.dat", usecols=1)
  dec_blue   = np.genfromtxt("./data/blue.dat", usecols=2)
  z_cmb_blue = np.genfromtxt("./data/blue.dat", usecols=3)
  
  ra_red    = np.genfromtxt("./data/red.dat", usecols=1)
  dec_red   = np.genfromtxt("./data/red.dat", usecols=2)
  z_cmb_red = np.genfromtxt("./data/red.dat", usecols=3)

  from astropy import constants as const

  sdss_ra   = np.genfromtxt("./data/phospec_data.dat", usecols=0)
  sdss_dec  = np.genfromtxt("./data/phospec_data.dat", usecols=1)
  sdss_z    = np.genfromtxt("./data/phospec_data.dat", usecols=2)


  X = np.genfromtxt("ICs_SliceWidth_0Mpc_Lbox_50Mpc_Npart_050.dat", usecols=0)[1:]
  Y = np.genfromtxt("ICs_SliceWidth_0Mpc_Lbox_50Mpc_Npart_050.dat", usecols=1)[1:]
  Z = np.genfromtxt("ICs_SliceWidth_0Mpc_Lbox_50Mpc_Npart_050.dat", usecols=2)[1:]


  x = np.arange(0, 50)
  y = np.arange(0, 50)
  z = np.arange(0, 50)
  print x

  from mpl_toolkits.mplot3d import Axes3D  
  from matplotlib.collections import LineCollection



  #xx, yy, zz = np.meshgrid(x, y, z)#, sparse=True)
  xx, yy = np.meshgrid(x, y)#, sparse=True)

  fig = plt.figure(figsize=(8, 6))
  plt.scatter(xx, yy, s=1, color='peru')
  plt.scatter(X, Y, s=4, color='r')
  segs1 = np.stack((xx,yy), axis=2)
  segs2 = segs1.transpose(1,0,2)
  plt.gca().add_collection(LineCollection(segs1))
  plt.gca().add_collection(LineCollection(segs2))

  plt.show()
  


  #c = SkyCoord(ra=ra*u.degree, dec=dec*u.degree) 
  
  fig = plt.figure(1, figsize=(8, 6))

  #fig.subplots_adjust(wspace=0.3, left=0.05, right=0.95)

  ax2, aux_ax2 = setup_axes3(fig, 111, np.min(ra_blue), 
                             np.max(ra_blue), np.min(z_cmb_blue), 
                             np.max(z_cmb_blue))

  aux_ax2.scatter(ra_blue, z_cmb_blue, s=1, lw=1, c='b', label=r'$\rm{ALFA-blue}$')
#  plt.legend(frameon=False, ncol=1, fontsize=14, loc=1, bbox_to_anchor=(0.8, 1.1) )
  plt.tight_layout()
  plt.savefig("ALFABlue_NGC_footprint.png", dpi=600)
  


#---------------------------------------------------------------------------------
  fig = plt.figure(2, figsize=(8, 6))

  #fig.subplots_adjust(wspace=0.3, left=0.05, right=0.95)
  ax2, aux_ax2 = setup_axes3(fig, 111, np.min(ra_red), 
                             np.max(ra_red), np.min(z_cmb_red), 
                             np.max(z_cmb_red))

  aux_ax2.scatter(ra_red, z_cmb_red, s=1, lw=1, c='r', label=r'$\rm{ALFA-red}$')
#  plt.legend(frameon=False, ncol=1, fontsize=14, loc=1, bbox_to_anchor=(0.8, 1.1) )
  plt.tight_layout()
  plt.savefig("ALFARed_NGC_footprint.png", dpi=600)
  
#---------------------------------------------------------------------------------

  fig = plt.figure(3, figsize=(8, 6))
  ax2, aux_ax2 = setup_axes3(fig, 111, np.min(sdss_ra), 
                             np.max(sdss_ra), np.min(const.c.value*sdss_z*0.001), 
                             np.max(const.c.value*sdss_z*0.001))

  aux_ax2.scatter(sdss_ra, const.c.value*sdss_z*0.001 , s=1, lw=1, c='c', label=r'$\rm{SDSS}$')
#  plt.legend(frameon=False, ncol=1, fontsize=14, loc=1, bbox_to_anchor=(0.8, 1.1) )
  plt.tight_layout()
  plt.savefig("SDSS_NGC_footprint.png", dpi=600)


  plt.show()
  

if __name__ == "__main__":
    main()


