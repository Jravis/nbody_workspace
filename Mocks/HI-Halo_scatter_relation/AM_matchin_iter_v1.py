
#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
                                                    #
# This routine is to compute the effect of          #
# scatter into HI-halo abundace matching relation   #
# Following Behroozi 2010 and Ren 2019.             #
                                                    #
#####################################################

from __future__ import print_function, division
import numpy as np
import numexpr as ne
from scipy import optimize
from scipy import integrate
from scipy import stats
import sys
import time
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from scipy import interpolate
from hmf import MassFunction
from hmf import cosmo
from astropy.cosmology import LambdaCDM
import argparse
from numba import njit, jit

import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from astropy.visualization import hist

#mpl.rcParams['font.family'] = 'sans-serif'
#mpl.rcParams['font.sans-serif'] = ['Tahoma']
#mpl.rcParams['text.usetex'] = True

#set some plotting parameters
mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
#plt.style.use("classic")
#plt.style.use("ggplot")
plt.style.use("seaborn-white")





__author__ = "Sandeep Rana"

__credits__ = [""]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"



#---------------- Global Values ---------------------------

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206

new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)



def h_c(c):
    return 100./c
    

#HIPASS parameter

alpha_Hipass = -1.37            # \pm 0.03 \pm 0.05
#log_M_star_Hipass = 9.80       #-2*np.log10(h_c(75)) # \pm 0.03 \pm 0.03 h_75^-1
log_M_star_Hipass = 9.86-2*np.log10(h_c(70)) # \pm 0.03 \pm 0.03 h_75^-1
#phi_star_Hipass = 6.0*0.001    #*h_c(70)**3 #\pm 0.8 \pm 0.6 h_75^3 Mpc^-3
phi_star_Hipass = 5.0*0.001*h_c(70)**3 #\pm 1 h_70^3 Mpc^-3

#ALFA parameter 2DSWL

alpha_ALFA = -1.33              # \pm 0.02 
log_M_star_ALFA = 9.96-2*np.log10(h_c(70)) # \pm 0.2 h_70^-2
phi_star_ALFA = 4.8*0.001*h_c(70)**3 #\pm 0.8 \pm 0.6 h_70^3 Mpc^-3 dex^-1

#----------------------------------------------------------

def HI_Mass_func(MHI, ALFA=True):

    if not ALFA:
        MHI_hat = MHI - log_M_star_Hipass
        MHI_hat_ratio = 10**MHI/10**log_M_star_Hipass

        return ( np.log10(phi_star_Hipass * np.log(10.)) + 
                 (1+alpha_Hipass)*MHI_hat - MHI_hat_ratio * np.log10(np.exp(1)))
    else:
        MHI_hat = MHI - log_M_star_ALFA
        MHI_hat_ratio = 10**MHI/10**log_M_star_ALFA
        
        return ( np.log10(phi_star_ALFA * np.log(10.)) + 
                 (1+alpha_ALFA)*MHI_hat - MHI_hat_ratio * np.log10(np.exp(1)))

#------------------------------------------------------------------------

def lognormal_dist(logMHI, Mhalo, func, scatter):
       
    """
    Conditional luminostiy function in the 
    form of log normal distribution.
    """
    #scatter = 0.3
    temp = logMHI - func(Mhalo)
    return (1./np.sqrt(2*np.pi*scatter**2)) * np.exp(-temp**2/2./scatter**2)
     
#------------------------------------------------------------------------


def _is_reversed(x, y):
    """
    Does the tabulated function y(x) decrease for increasing x?
    """
    sorted_inds = np.argsort(x)
    x = x[sorted_inds]
    y = y[sorted_inds]
    
    return  np.all(np.diff(y)<0)
 

def get_cumulative(f, bins, reverse=True):
    
    """
    calculate the cumulative abundance function by 
    integrating the differential abundance 
    function.

    Parameters
    ----------
    Returns
    ----------
    """
    
    #determine the normalization.
    # Initial point
    if reverse==True:
        N0 = np.float64(f(bins[-1])) * (bins[-2]-bins[-1])
    else:
        N0 = float(f(bins[0]))*(bins[1]-bins[0])

    #if the function is monotonically decreasing, integrate in the opposite direction
    if reverse==True:
        N_cum = integrate.cumtrapz(f(bins[::-1]), bins[::-1], initial=N0)
        N_cum = N_cum[::-1]*(-1.0)
    else:
        N_cum = integrate.cumtrapz(f(bins), bins, initial=N0)

    #interpolate result to get a functional form
    N_cum_func = _spline(bins, N_cum, k=1)
    
    return N_cum_func


#------------------------------------------------------------------------


def AM_nonparam_iter(G, dn_dG, H, dn_dH, spread, analytic=True, P_x=None,
		     H_min=None, H_max=None, nH=100, tol_err=0.001):

    """
    Abundance match galaxy property 'G' to halo property 'H'. 
    
    Iteratively deconvolve <G(H)> from dn/dH.
    
    Tabulated abundances of galaxies and haloes are required.
    
    Determines the mean of P(G_gal | H_halo), given the centered distribution, 
    i.e. all other moments.
    
    In detail P should be of the form: P_x(y, mu_xy(y)), where y 
    is a property of the halo and mu_xy(y) is the mean of the distribution 
    as a function of y. This function simply solves for the form of mu_xy.
    
    Parameters
    ==========
    dn_dx: array_like
        galaxy abundance as a function of property 'x'
    
    x: array_like
    
    dn_dy: array_like
        halo abundance as a function of property 'y'
    
    y: array_like
    
    P_x: function
        The centered probability distribution of P(x_gal | y_halo).  
	If this is None, traditional abundance matching is 
	done with no scatter.
    
    y_min: float
        minimum value to determine the form of P(x|y) for
    
    y_max: float
        maximum value to determine the form of P(x|y) for
    
    ny: int
        number of points used to sample the first moment in the range [y_min,y_max]]
    
    tol_err: float, optional
         stop the calculation when <x(y)> changes by less than tol_err
    
    Returns
    =======
    mu : function 
         first moment of P(x| y)
    """

    #process input parameters
    G = np.array(G, dtype=np.float64) # In log10
    dn_dG = np.array(dn_dG, dtype=np.float64)

    if H_min==None:
	    H_min = np.amin(H)
    if H_max==None:
	    H_min = np.amax(H)
    if H_min > H_max:
	    raise ValueError("H_min must be less than H_max!")


    if not analytic:
    	H = np.array(H, dtype=np.float64) # log mass
    	dn_dH = np.array(dn_dH, dtype=np.float64) # dndlog10m
    #check halo abundance range inputs
    else:
    	H = np.array(H, dtype=np.float64)
    	deltax = (H_max- H_min)/float(len(H))
    	h_sampl = MassFunction(z=0.0,Mmin=H_min, Mmax=H_max, dlog10m=deltax, 
	    	hmf_model="SMT", cosmo_model=new_model, sigma_8=0.8228, n=0.96)
    	dn_dH  = (h_sampl.dndlog10m * 0.6777**3)


    #my_cosmo = cosmo.Cosmology(new_model)
    #define y samples used when integrating dn_dH*P(g|H)dH. 
    #This needs to be finely spaced enough to get accurate 
    #integrals of P(G|H)dH. This can get very narrow for small
    #scatter and/or for steep G(H) relations.

    NBINS=1000
    deltax = (H_max- H_min)/float(NBINS)
    H_fine = np.arange(H_min, H_max+0.5*deltax, deltax)


    #define y samples of x(y). This will be the number of parameters to minimize when 
    #solving for x(y).  More parameters will slow the code down!
    deltax = (H_max- H_min)/float(nH)
    H_sampling = np.arange(H_min, H_max+0.5*deltax,deltax)


    #step 0: preliminary work
    #Put in order so the independent variable is monotonically increasing

    #Galaxies
    inds = np.argsort(G)
    G = G[inds]
    dn_dG = dn_dG[inds]
    #Halos
    inds = np.argsort(H)
    H = H[inds]
    dn_dH = dn_dH[inds]

#-------------------------------------------------------------------

    #check direction of increasing number density.  Usually this is reversed.
    #This affects the sign of integrations, and how interpolations are implemented.
    
    reverse_G = _is_reversed(G, dn_dG)
    reverse_H = _is_reversed(H, dn_dH)

    print("Galaxy property decreases as dn_dG increases: ", reverse_G)
    print("Halo mass decreases as dn_dH increases: ", reverse_H)

    #check numerical values of dn_dx and dn_dy
    #trim down ranges if they return numbers they are too small


    keep = (dn_dG > 10.0**(-20.0))

    if not np.all(keep):
        print("Triming G-range to keep number densities above 1e-20")
        G = G[keep]
        dn_dG = dn_dG[keep]

    keep = (dn_dH>10.0**(-20.0))
    if not np.all(keep):
        print("Triming H-range to keep number densities above 1e-20")
        H = H[keep]
        dn_dH = dn_dH[keep]


    #convert tabulated abundance functions into function objects using interpolation
    #interpolation.

    #galaxy abundance function
    ln_dn_dG_func = _spline(G, np.log10(dn_dG), k=1)
    dn_dG_func    = _spline(G, dn_dG, k=1)

    #halo abundance function
    ln_dn_dH_func = _spline(H, np.log10(dn_dH), k=1)
    dn_dH_func    = _spline(H, dn_dH, k=1)

    #calculate the cumulative abundance functions
    N_cum_halo_func = get_cumulative(dn_dH_func, H, reverse = reverse_H)
    N_cum_gal_func  = get_cumulative(dn_dG_func, G, reverse = reverse_G)

    #galaxy cumulative abundance function number density range must span that of the halo 
    #abundance function for abundance matching to be possible.
    N_min = min(N_cum_halo_func(H_min), N_cum_halo_func(H_max)) # This is to taken into account for 
    N_max = max(N_cum_halo_func(H_min), N_cum_halo_func(H_max)) # Galaxy luminosity 

    print("maximum halo cumulative abundnace: {0}".format(N_max))
    print("mimimum halo cumulative abundnace: {0}".format(N_min))
    print("maximum galaxy cumulative abundnace: {0}".format(np.amax(N_cum_gal_func(G))))
    print("mimimum galaxy cumulative abundnace: {0}".format(np.amin(N_cum_gal_func(G))))

#--------------------------------------------------------------------------------------------

    # Now to do abundance matching we need to calculate the inverse cumulative abundances

    if reverse_G==True:
        N_cum_gal_inv  = _spline(N_cum_gal_func(G)[::-1], G[::-1], k=1)
    else:
        N_cum_gal_inv  = _spline(N_cum_gal_func(G), G, k=1)

    if reverse_H==True:    
        N_cum_halo_inv = _spline(N_cum_halo_func(H)[::-1], H[::-1], k=1)
    else:
        N_cum_halo_inv = _spline(N_cum_halo_func(H), H, k=1)


    #calculate pivot point below this point, the galaxy abundance function will be 
    #incomplete given the halo value range under consideration.

    G_pivot = N_cum_gal_inv(N_cum_halo_func(H_min))
    print("minimum G value in range: {0}".format(G_pivot))

    #discard the galaxy abundance function below this point.
    keep = (G>G_pivot)
    G = G[keep]


    ##########################################################
    #step 1: solve for first moment in the absence of scatter.  This is abundance matching
    #with no scatter.
    G_H_ini  = N_cum_gal_inv(N_cum_halo_func(H_sampling))
    G_H_ini = _spline(H_sampling, G_H_ini, k=1)
    """    
    plt.plot(H, G_H_ini(H), 'r-', linewidth=1.8)
    plt.xlim(9, 16)
    plt.ylim(6, 12)
    plt.show()
    """
    if P_x==None:
        return G_H_ini
        
    ##########################################################
    #step 2: get second estimate of the first moment by integrating to get the galaxy
    #abundance function of haloes and solve for N_halo(x) = N_halo(y) to get a second
    #estimate of x(y)
    
    #do integral
    dn_halo_dx = np.zeros(len(G), dtype=np.float64)
    for i in xrange(len(G)):
        lnPx      = lognormal_dist(G[i], H, G_H_ini, spread)
        integrand = lnPx*dn_dH_func(H)
        dn_halo_dx[i] = integrate.simps(integrand, H)
    
    #get galaxy abundance function function (number of haloes as a function of x)
    dn_halo_dx = _spline(G, dn_halo_dx, k=1) 
    
    #get new cumulative function
    N_cum_halo_G = get_cumulative(dn_halo_dx, G, reverse = reverse_G)
    """ 
    plt.plot(G, np.log10(N_cum_halo_G(G)), 'r-', linewidth=1.8)
    plt.plot(G, np.log10(N_cum_gal_func(G)), 'b-', linewidth=1.8)
    plt.xlim(6, 12)
    plt.show()
    """


    #get inverse cumulative mass function in our case solving convolution 
    #integral gives us new HI mass function 

    if reverse_G==True:
        N_cum_halo_G_inv = _spline(N_cum_halo_G(G)[::-1], G[::-1], k=1)
    else:
        N_cum_halo_G_inv = _spline(N_cum_halo_G(G), G, k=1)


    # Now after abundance match get the transformation between G and G_trans
    G_trans = N_cum_gal_inv(N_cum_halo_G(G))
    G_trans = _spline(G, G_trans, k=1)
    
    #get x2(y).  This is our second estimate.
    G_H_upd =  G_trans(G_H_ini(H_sampling))
    G_H_upd =  _spline(H_sampling, G_H_upd, k=1)

    """
    fig = plt.figure(figsize=(8, 6))
    plt.plot(H_sampling,G_H_ini(H_sampling),'-',color='red')
    plt.plot(H_sampling,G_H_upd(H_sampling),'.',color='blue',ms=2)
    plt.xlim(9, 16)
    plt.ylim(6, 12)
    plt.xlabel(r'$M_{\rm vir}$')
    plt.ylabel(r'$M_{*}$')
    plt.show()
    """

    
    G_H_upd_i = G_H_upd #previous iteration of the x(y)

    stop = False
    iteration = 0
    max_iterations=1000
    while stop==False: 

	#do integral
    	dn_halo_dx = np.zeros(len(G), dtype=np.float64)
	for i in xrange(len(G)):
	    lnPx      = lognormal_dist(G[i], H, G_H_upd_i, spread)
	    integrand = lnPx*dn_dH_func(H)
	    dn_halo_dx[i] = integrate.simps(integrand, H)
	
	#get galaxy abundance function function (number of haloes as a function of x)
	dn_halo_dx = _spline(G, dn_halo_dx, k=1) 
	
	#get new cumulative function
	N_cum_halo_G = get_cumulative(dn_halo_dx, G, reverse = reverse_G)
	
	if reverse_G==True:
            N_cum_halo_G_inv = _spline(N_cum_halo_G(G)[::-1], G[::-1], k=1)
    	else:
            N_cum_halo_G_inv = _spline(N_cum_halo_G(G), G, k=1)


        # Now after abundance match get the transformation between G and G_trans
        G_trans = N_cum_gal_inv(N_cum_halo_G(G))
        G_trans = _spline(G, G_trans, k=1)
    
        #This is our second estimate.
	G_H_upd_ii = G_H_upd_i
        G_H_upd_i =  G_trans(G_H_upd_i(H_sampling))
        G_H_upd_i =  _spline(H_sampling, G_H_upd_i, k=1)

	err = np.amax(np.fabs(G_H_upd_i(H_sampling) - G_H_upd_ii(H_sampling))/G_H_upd_i(H_sampling))
        
        if err<=tol_err:
            break
        iteration = iteration+1
        if iteration==max_iterations:
            print("specified tolerance not reached!")
            break
    """ 
    plt.plot(G, np.log10(N_cum_halo_G(G)), 'r-', linewidth=1.8)
    plt.plot(G, np.log10(N_cum_gal_func(G)), 'b-', linewidth=1.8)
    plt.xlim(6, 12)
    plt.show()

    plt.plot(H_sampling, G_H_ini(H_sampling),'-',color='red')
    plt.plot(H_sampling, G_H_upd_i(H_sampling),'.',color='blue',ms=2)
    plt.xlim(9, 16)
    plt.ylim(6, 12)
    plt.xlabel(r'$M_{\rm vir}$')
    plt.ylabel(r'$M_{*}$')
    plt.show()
    """

    return G_H_upd_i, G_H_ini

#-------------------------------------------------------------------------




def main():
    
    
    NBINS = 1000
    xlim  = [6, 11]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    G  = np.arange(xlim[0],xlim[1]+0.5*delta_mf,delta_mf)
    dn_dG   = 10**HI_Mass_func(G)         # Alfa-alfa HI mass function

    NBINS = 1000
    xlim  = [6, 16]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H  = np.arange(xlim[0],xlim[1]+0.5*delta_mf,delta_mf)
    dn_dH = np.zeros(NBINS)

    HI_halo_func1, HI_halo_func_ini = AM_nonparam_iter(G, dn_dG, H,
                                    dn_dH, 0.3, analytic=True, P_x=True, 
        		            H_min=6, H_max=16, nH=NBINS, tol_err=0.001)
                                    
    HI_halo_func2, HI_halo_func_ini = AM_nonparam_iter(G, dn_dG, H, 
                                    dn_dH, 0.1, analytic=True, P_x=True,
        		            H_min=6, H_max=16, nH=NBINS, tol_err=0.001)
     

#---------------------------------------------------------------------------                                    

    Mock_halo = np.genfromtxt("mock_halo_small.txt")
    Mock_HI_scatt1   = HI_halo_func1(Mock_halo)
    Mock_HI_scatt2   = HI_halo_func2(Mock_halo)
    Mock_HI_ini   = HI_halo_func_ini(Mock_halo)
    
    ind= (Mock_HI_scatt1<11)*(Mock_HI_scatt1>=6)
    Mock_HI_scatt1 = Mock_HI_scatt1[ind]
    Mock_halo1 = Mock_halo[ind]
   
    ind= (Mock_HI_scatt2<11)*(Mock_HI_scatt2>=6)
    Mock_HI_scatt2 = Mock_HI_scatt2[ind]
    Mock_halo2 = Mock_halo[ind]

    ind= (Mock_HI_ini<11)*(Mock_HI_ini>=6)
    Mock_HI_ini = Mock_HI_ini[ind]
    Mock_halo3 = Mock_halo[ind]
 
 

    delta_mf=(xlim[1]-xlim[0])/float(len(H))
    h_sampl = MassFunction(z=0.0,Mmin=6, Mmax=16, dlog10m=delta_mf, 
	    	hmf_model="SMT", cosmo_model=new_model, sigma_8=0.8228, n=0.96)
    dn_dH  = (h_sampl.dndlog10m * 0.6777**3)

    H = np.array(H, dtype=np.float64)
    
    #Halos
    inds = np.argsort(H)
    H = H[inds]
    dn_dH = dn_dH[inds]
    reverse_H = _is_reversed(H, dn_dH)
    
    keep = (dn_dH>10.0**(-20.0))
    if not np.all(keep):
        print("Triming H-range to keep number densities above 1e-20")
        H = H[keep]
        dn_dH = dn_dH[keep]

    ln_dn_dH_func = _spline(H, np.log10(dn_dH), k=1)
    dn_dH_func    = _spline(H, (dn_dH), k=1)
 
    #do integral
    dn_halo_dx1 = np.zeros(len(G), dtype=np.float64)
    dn_halo_dx2 = np.zeros(len(G), dtype=np.float64)
    

    for i in xrange(len(G)):

        lnPx      = lognormal_dist(G[i], H, HI_halo_func1, 0.3)
        integrand = lnPx*dn_dH_func(H)
        dn_halo_dx1[i] = integrate.simps(integrand, H)
    
        lnPx      = lognormal_dist(G[i], H, HI_halo_func2, 0.1)
        integrand = lnPx*dn_dH_func(H)
        dn_halo_dx2[i] = integrate.simps(integrand, H)
 


    plt.figure(1, figsize=(6, 6))
    plt.scatter(Mock_halo1, Mock_HI_scatt1, s=1, marker='o',color='blue', lw=0, label=r'$\Sigma =0.3$')
    plt.scatter(Mock_halo2, Mock_HI_scatt2, s=1, marker='s',color='g', lw=0, label=r'$\Sigma =0.1$')
    plt.scatter(Mock_halo3, Mock_HI_ini, s=1, marker='D',color='r', lw=0, label=r'$\Sigma =0.0$')
    plt.legend(loc=4)
    plt.xlim(9, 15)
    plt.ylim(6, 11)
    plt.xlabel(r'$M_{\rm h}$')
    plt.ylabel(r'$M_{HI}$')



    plt.figure(2, figsize=(6, 6))
    gs = gridspec.GridSpec(1, 1)#, hspace=0.0) #, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    hist(Mock_HI_scatt1, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.3$')
    hist(Mock_HI_scatt2, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.1$')
    hist(Mock_HI_ini, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.0$')
    ax1.set_ylabel(r'$\log_{10 }\rm{N}$ ', fontsize=20)
    ax1.set_xlabel(r'$\log_{10}$ $\rm{M}_{HI} [\rm{M}_{\odot}]$', fontsize=20)
    #ax1.set_xscale("log")
    ax1.set_yscale("log")
#    ax1.set_ylim(10**-7, 10**1)
    ax1.set_xlim(6, 11)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1)
    ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    
    plt.figure(3, figsize=(6, 6))
    gs = gridspec.GridSpec(1, 1)#, hspace=0.0) #, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    ax1.plot(G, np.log10(dn_halo_dx1), linestyle='-', color='r', linewidth=2, label=r'$\Sigma=0.3$')
    ax1.plot(G, np.log10(dn_halo_dx2), linestyle='-', color='b', linewidth=2, label=r'$\Sigma=0.1$')
    ax1.plot(G, np.log10(dn_dG), linestyle='-', color='c', linewidth=2, label=r'$\rm{ALFA}$')

#    hist(Mock_halo1, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.3$')
#    hist(Mock_halo2, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.1$')
#    hist(Mock_halo3, bins='knuth', histtype='step', density=True, linewidth=1.8, alpha=0.8, label=r'$\Sigma=0.0$')
    ax1.set_ylabel(r'$\log_{10 }\rm{N}$ ', fontsize=20)
    ax1.set_xlabel(r'$\log_{10}$ $\rm{M}_{HI} [\rm{M}_{\odot}]$', fontsize=20)
    #ax1.set_xscale("log")
#    ax1.set_yscale("log")
#    ax1.set_ylim(10**-7, 10**1)
    ax1.set_xlim(6, 11)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1)
    ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()

    plt.show()
                     
if __name__ == "__main__":
    main()




