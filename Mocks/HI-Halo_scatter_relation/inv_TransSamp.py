#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
                                                    #
# This routine is to compute the effect of          #
# scatter into HI-halo abundace matching relation   #
# Following Behroozi 2010 and Ren 2019.             #
                                                    #
#####################################################

from __future__ import print_function, division
import numpy as np
import numexpr as ne
from scipy import optimize
from scipy import integrate
from scipy import stats
import sys
import time
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from scipy import interpolate
from hmf import MassFunction
from hmf import cosmo
from astropy.cosmology import LambdaCDM
import argparse
from numba import njit, jit

import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from astropy.visualization import hist

#mpl.rcParams['font.family'] = 'sans-serif'
#mpl.rcParams['font.sans-serif'] = ['Tahoma']
#mpl.rcParams['text.usetex'] = True

#set some plotting parameters
mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
#plt.style.use("classic")
#plt.style.use("ggplot")
plt.style.use("seaborn-white")





__author__ = "Sandeep Rana"

__credits__ = [""]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"


#---------------- Global Values ---------------------------
# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206

new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)


def _is_reversed(x, y):
    """
    Does the tabulated function y(x) decrease for increasing x?
    """
    sorted_inds = np.argsort(x)
    x = x[sorted_inds]
    y = y[sorted_inds]
    
    return  np.all(np.diff(y)<0)

def get_cumulative(f, bins, reverse=True):
    
    """
    calculate the cumulative abundance function by 
    integrating the differential abundance 
    function.

    Parameters
    ----------
    Returns
    ----------
    """
    
    #determine the normalization.
    # Initial point
    if reverse==True:
        N0 = np.float64(f(bins[-1])) * (bins[-2]-bins[-1])
    else:
        N0 = float(f(bins[0]))*(bins[1]-bins[0])

    #if the function is monotonically decreasing, integrate in the opposite direction
    if reverse==True:
        N_cum = integrate.cumtrapz(f(bins[::-1]), bins[::-1], initial=N0)
        N_cum = N_cum[::-1]*(-1.0)
    else:
        N_cum = integrate.cumtrapz(f(bins), bins, initial=N0)

    #interpolate result to get a functional form
    N_cum_func = _spline(bins, N_cum, k=1)
    
    return N_cum_func

#---------------------------------------------------------------------

def randomvariate(pdf,n, xmin, xmax, pmin, pmax):  

    """  
    Rejection method for random number generation  
    ===============================================  
    Uses the rejection method for generating random numbers derived from an arbitrary   
    probability distribution. For reference, see Bevington's book, page 84. Based on  
    rejection*.py.  
       
    Usage:  
    >>> randomvariate(P,N,xmin,xmax, Pmax, Pmin)  
     where  
     P : probability distribution function from which you want to generate random numbers  
     N : desired number of random values  
     xmin,xmax : range of random numbers desired  
     Pmax,Pmin : range of distribution funciton
	
    Returns:   
    the sequence (ran,ntrials) where  
    ran : array of shape N with the random variates that follow the input P  
    ntrials : number of trials the code needed to achieve N  
       
    Here is the algorithm:  
    - generate x' in the desired range  
    - generate y' between Pmin and Pmax (Pmax is the maximal value of your pdf)  
    - if y'<P(x') accept x', otherwise reject  
    - repeat until desired number is achieved  
    """

    #counters
    np.random.seed(23456) 
    Naccept=0
    Ntrial=0
    ran = []
    accept_px = []
    while Naccept<n:  
      x = np.random.uniform(xmin,xmax) # x'  
      y = np.random.uniform(pmin,pmax) # y'  

      if y<pdf(x):  
        ran.append(x)  
        accept_px.append(y)
        Naccept=Naccept+1  
      Ntrial=Ntrial+1  

    return np.asarray(ran), np.asarray(accept_px), Ntrial

#------------------------------------------------------------


def main():

    NBINS = 1000
    xlim  = [6, 16]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H  = np.arange(xlim[0],xlim[1]+0.5*delta_mf,delta_mf)
    delta_mf=(xlim[1]-xlim[0])/float(len(H))
    h_sampl = MassFunction(z=0.0,Mmin=6, Mmax=16, dlog10m=delta_mf, 
	    	hmf_model="SMT", cosmo_model=new_model, sigma_8=0.8228, n=0.96)
    dn_dH  = (h_sampl.dndlog10m * 0.6777**3)

    H = np.array(H, dtype=np.float64)
    
    #Halos
    inds = np.argsort(H)
    H = H[inds]
    dn_dH = dn_dH[inds]
    reverse_H = _is_reversed(H, dn_dH)
    
    keep = (dn_dH>10.0**(-20.0))
    if not np.all(keep):
        print("Triming H-range to keep number densities above 1e-20")
        H = H[keep]
        dn_dH = dn_dH[keep]

    ln_dn_dH_func = _spline(H, np.log10(dn_dH), k=1)
    dn_dH_func    = _spline(H, (dn_dH), k=1)

    pmin = np.min(ln_dn_dH_func(H))
    pmax = np.max(ln_dn_dH_func(H))
    print(pmin, pmax)
    Mock_halo, ppx, trials = randomvariate(ln_dn_dH_func,  500000, 9, 15.5, pmin, pmax)
    np.savetxt("mock_halo_small.txt", Mock_halo, fmt="%lf")
    
    plt.figure(1, figsize=(6, 6))
    gs = gridspec.GridSpec(1, 1)#, hspace=0.0) #, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    

    ax1.plot(H, np.log10(dn_dH), linestyle='-', color='r', linewidth=2, label=r'$\rm{ALFA}$')
    ax1.scatter(Mock_halo, ppx, s=1, lw=0, color='b')
    ax1.set_ylabel(r'$\log_{10 }\rm{\phi}$ [$\rm{Mpc}^{-3}$ $\rm{dex}^{-1}$]', fontsize=20)
    ax1.set_xlabel(r'$\log_{10}$ $\rm{M}_{h} [\rm{M}_{\odot}]$', fontsize=20)
    #ax1.set_xscale("log")
    #ax1.set_yscale("log")
    #ax1.set_ylim(10**-7, 10**1)
    ax1.set_xlim(6, 16)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1)
    ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    plt.show()



if __name__ == "__main__":
    main()

