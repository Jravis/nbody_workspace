/* This file is auto-generated from countpairs_kernels.c.src */
#ifndef DOUBLE_PREC
#define DOUBLE_PREC
#endif
// # -*- mode: c -*-
/* File: countpairs_kernels.c.src */
/*
  This file is a part of the Corrfunc package
  Copyright (C) 2015-- Manodeep Sinha (manodeep@gmail.com)
  License: MIT LICENSE. See LICENSE file under the top-level
  directory at https://github.com/manodeep/Corrfunc/
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include "function_precision.h"
#include "utils.h"
#include "weight_functions_double.h"

//-----------------------------------------------------------------------

static inline int countpairs_fallback_double(const int64_t N0, double *x0, double *y0, double *z0, 
                                             const weight_struct_double *weights0, double *vx0, 
                                             double *vy0, double *vz0, const int64_t N1, double *x1, 
                                             double *y1, double *z1, const weight_struct_double *weights1,
                                             double *vx1, double *vy1, double *vz1, const int same_cell,
                                             const double sqr_rpmax, const double sqr_rpmin, const int nbin, 
                                             const double *rupp_sqr, const double rpmax, const double off_xwrap, 
                                             const double off_ywrap, const double off_zwrap,
                                             double *src_rpavg, uint64_t *src_npairs,
                                             double *src_weightavg, double *src_vpavg, 
                                             double *src_vp_r_avg, double *src_vtavg,
                                             const weight_method_t weight_method) 
{

    /*----------------- FALLBACK CODE --------------------*/
  const int32_t need_rpavg     = src_rpavg     != NULL;
  const int32_t need_weightavg = src_weightavg != NULL;
  const int32_t need_vpavg     = src_vpavg     != NULL;
  const int32_t need_vp_r_avg  = src_vp_r_avg  != NULL;
  const int32_t need_vtavg     = src_vpavg     != NULL;

  uint64_t npairs[nbin];
  for(int i=0;i<nbin;i++) {
    npairs[i]=0;
  }
  double rpavg[nbin], weightavg[nbin];
  double vpavg[nbin], vp_r_avg[nbin];
  double vtavg[nbin];
  

  for(int i=0;i<nbin;i++) {
    if(need_rpavg) {
      rpavg[i]=0.;
    }
    if(need_weightavg){
      weightavg[i]=0.;
    }
    if(need_vpavg){
      vpavg[i]=0.;
    }
    if(need_vp_r_avg){
      vp_r_avg[i]=0.;
    }
    if(need_vtavg){
      vtavg[i]=0.;
    }
  }
  
  // A copy whose pointers we can advance
  weight_struct_double local_w0 = {.weights={NULL}, .num_weights=0}, 
                       local_w1 = {.weights={NULL}, .num_weights=0};
                       
  pair_struct_double pair = {.num_weights=0};
  weight_func_t_double weight_func = NULL;
  
  if(need_weightavg){
      // Same particle list, new copy of num_weights pointers into that list
      local_w0 = *weights0;
      local_w1 = *weights1;
      
      pair.num_weights = local_w0.num_weights;
      
      weight_func = get_weight_func_by_method_double(weight_method);
  }
  
  /* naive implementation that is guaranteed to compile */
  int64_t nleft=N1, n_off = 0;
  for(int64_t i=0;i<N0;i++) {

    const double xpos = *x0++ + off_xwrap;
    const double ypos = *y0++ + off_ywrap;
    const double zpos = *z0++ + off_zwrap;
    
    const double xvel = *vx0++;
    const double yvel = *vy0++;
    const double zvel = *vz0++;

    for(int w = 0; w < pair.num_weights; w++){
        pair.weights0[w].d = *local_w0.weights[w]++;
    }

    /* If in the same cell, unique pairs are guaranteed by not including the current particle */
    if(same_cell == 1) {
        z1++; n_off++;
        nleft--;
    } else {
        /* For a different cell, all pairs are unique pairs, since two cells are only opened for pairs once (accounted for in the assign_ngb_cells function)*/
        while(nleft > 0) {
            /*Particles are sorted on 'z', in increasing order */
            const double dz = *z1 - zpos;
            if(dz > -rpmax) break;
            z1++; n_off++;
            nleft--;
        }
        /*If no particle in the second cell satisfies distance constraints on 'dz' for the current 'i'th particle in first cell, 
          then there can be no more pairs from any particles in the first cell (since the first cell is also sorted in increasing order in 'z')
        */
        if(nleft == 0) {
            i=N0;/*noting intent for which loop is break'ing */
            break;
        }
    }
    double *localz1 = z1;
    double *localx1 = x1 + n_off;
    double *localy1 = y1 + n_off;

    double *localvz1 = vz1 ;
    double *localvx1 = vx1 ;
    double *localvy1 = vy1 ;


    for(int w = 0; w < pair.num_weights; w++){
        local_w1.weights[w] = weights1->weights[w] + n_off;
    }

    for(int64_t j=0;j<nleft;j++) {
      const double dx = *localx1++ - xpos;
      const double dy = *localy1++ - ypos;
      const double dz = *localz1++ - zpos;

      for(int w = 0; w < pair.num_weights; w++){
        pair.weights1[w].d = *local_w1.weights[w]++;
      }
            
      if(dz >= rpmax) break;
      
      const double r2 = dx*dx + dy*dy + dz*dz;
      if(r2 >= sqr_rpmax || r2 < sqr_rpmin) continue;
      
      const double dvx = *localvx1++ - xvel;
      const double dvy = *localvy1++ - yvel;
      const double dvz = *localvz1++ - zvel;

      if(need_weightavg){
        pair.dx.d = dx;
        pair.dy.d = dy;
        pair.dz.d = dz;
      }

      double r, pairweight;
      if(need_rpavg) {
        r = SQRT(r2);
      }
      if(need_weightavg){
        pairweight = weight_func(&pair);
      }
      
      if(need_vpavg){
        const double vp_r = (dvx*dx + dvy*dy + dvz*dz)/r2;
        const double vp = (dvx*dx + dvy*dy + dvz*dz)/SQRT(r2);
      
        const double sp = vp*vp;
        const double st = (dvx*dvx + dvy*dvy + dvz*dvz)-sp;
        const double vt = sqrt(st);
      }


      for(int kbin=nbin-1;kbin>=1;kbin--){
        if(r2 >= rupp_sqr[kbin-1]) {
          npairs[kbin]++;
          if(need_rpavg) {
            rpavg[kbin] += r;
          }
          if(need_weightavg){
            weightavg[kbin] += pairweight;
          }
          if(need_vpavg){
            vp_r_avg[kbin]+= vp_r;
            vpavg[kbin]   += vp;
            vtavg[kbin]   += vt;
          }
          break;
        }
      }//searching for kbin loop
    }
  }
  
  for(int i=0;i<nbin;i++) {
    src_npairs[i] += npairs[i];
    if(need_rpavg) {
      src_rpavg[i] += rpavg[i];
    }
    if(need_weightavg){
      src_weightavg[i] += weightavg[i];
    }
    if(need_vpavg){
      src_vpavg[i] += vp[i]
    }
    if(need_vp_r_avg){
      src_vp_r_avg[i] += vp_r[i]
    }
    if(need_vtavg){
      src_vtavg[i] += vt[i]
    }

  }
  /*----------------- FALLBACK CODE --------------------*/

  return EXIT_SUCCESS;
}
