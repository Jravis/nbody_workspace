/*#############################################################//
                                                               //  
Copyright (c) 2019, Sandeep Rana                               //
This is an open-mpi enabled brute force                        //
correlation code.                                              //
Including functionality for                                    //
2-D real-space for a set of arrays.                            //
The calculation is for auto-correlation,                       //
and with or without periodic boundaries.                       //
Following binning-schems are available                         //
log(r)-mu                                                      //
log(rperp)- log(rpar)                                          //
rperp-rpar.                                                    //
Z direction is by default los direction while                  //
the projected separation, rp is calculated                     //
in the X-Y plane.                                              //

This compute varitey of correaltion functions.                 //
Xi(r, mu)                                                      //
Xi_0, Xi_2 monopole and quadrapole                             //
Xi_0  = Xi(r) angle avg correaltion function                   //  
Xi(rpar, rperp)                                                //
Xi_par, Xi_perp from clustring wedges Kayzin et al 2012        //
                                                               //
This routine also computes pair-velocity correlation or        //
streaming velocity and velocity dispersion                     //
v_l along the line of sight(i.e along the r direction)         //
v_t transverse to the line of sight                            //
v_disp is the velocity dispersion                              //
                                                               //
################################################################*/





#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<math.h>
#include<string>
#include <algorithm> 
#include <bits/stdc++.h> 
#include <sstream> // for ostringstream
using namespace std;



//--------------- Declare global varables------------------------


char *filename1;
char *filename2;
char *filename3;
double *pos;
double *vel;
int nsub, sub_samp_size, ndim=3, ndm;


long int *pairs_rmu ;     // pairs as a function of (r, mu)
long int *pairs_rpp ;     // pairs as a function of (rpar, rperp)

//double   *pairs_vpp ;     // pairs as a function of (rpar, rperp)
//double   *pairs_vpp1;     // pairs as a function of (rpar, rperp)

double *pairvel_l  ;      // Along the line of sight pairwise velocity
double *pairvel_est  ;      // Along the line of sight pairwise velocity estimator 
double *CijSqr  ;      // Along the line of sight pairwise velocity estimator 

double *pairvel_est_los  ;      // Along the line of sight pairwise velocity estimator 
double *CijSqr_los  ;      // Along the line of sight pairwise velocity estimator 


double *pairvel_t  ;      // Along the transverse direction pairwise velocity
double *pairvdisp  ;      // Velocity dispersion 
double *pairvdisp_l;
double *pairvdisp_t;


int Nbr     ;             // Radial correlation bins angle avg bin
int Nb_mu   ;             // mu bins
int Nb_rpar ;             // along to line of sight bins
int Nb_rperp;             // Perpendicular to line of sight bins

double *rbin     ;
double *rp_bin   ;
double *rperp_bin;
double *mu_bin   ;

double Lbox, Lbox_half;
double scale_factor, HubbleParam;




//------------------------ Define subroutines --------------------------------------------


void init_pair_arrays();
void cleanup();
double* binning(double amin, double amax , 
               int nbin, double *bins, int bin_Key);
/*                  
void  corr_2D_box_bf(int np, int nbin, int nb_mu, int nb_rp, int nb_rperp, 
                     double *pos, double *vel, long int *p_rpp, long int *p_rmu, double *p_vpp,
                     double *p_vpp1, double *v_l_hh, double *v_t_hh, double *vdis_hh, 
                     double *vdis_l_hh, double *vdis_t_hh, 
                     double mu_min, double mu_max, double rmin, double rmax, 
                     double rp_min, double rp_max, double rperp_min, double rperp_max,
                     double l_box, double l_box_half, int bin_key, int bin_key1);
*/

void  corr_2D_box_bf(int np, int nbin, int nb_mu, int nb_rp, int nb_rperp, 
                     double *pos, double *vel, long int *p_rpp, long int *p_rmu,
                     double *v_l_hh, double *v_est_hh, double *cij_sqr, 
                     double *v_est_los_hh, double *cij_sqr_los,
                     double *v_t_hh, double *vdis_hh, 
                     double *vdis_l_hh, double *vdis_t_hh, double mu_min, 
                     double mu_max, double rmin, double rmax, double rp_min, 
                     double rp_max, double rperp_min, double rperp_max,
                     double l_box, double l_box_half, int bin_key, int bin_key1);

//-------------------------------------------------------------------------------

int main(int argc, char **argv){

  int    snapid, i, k, j;
  //int nblock_start, nblock_end;
  double nblock_start, nblock_end;
  double npart_sim, softening_length;
  double avpart,numden, bin_type_rmu, bin_type_rpp;

  double rmin, rmax;
  double Rp_min, Rp_max;
  double Rperp_min, Rperp_max;
  
    double xi_bar, xi_0, xi_2, xi_4;
  double sum1=0.0, sum2=0.0, sum3 = 0.0, sum4 = 0.0;
  double xi_rmu, xi_rpp, xi_rpar, xi_rperp;



  
  if(argc != 11){
    cout<< "\n./a.out   [snapid] [Nbr] [Nb_mu] [Nb_rpar] [Nb_rperp]  [sub_samp_size] [npartsim] [boxsize]]     [bin_type_rmu;]        [bin_type_rpp]"<<"\n";
    cout<< "\n./a.out     17     50     100      50        50           400000           512       1024     1 for logbin 0 linear  1 for logbin 0 linear"      <<"\n";
    return(-1);
  }//if


//-------------------------------------------------------------------------------------------------
// Setting up initial parameters

  snapid      = atoi(argv[1]);
  Nbr         = atoi(argv[2]);
  Nb_mu       = atoi(argv[3]);
  Nb_rpar     = atoi(argv[4]);
  Nb_rperp    = atoi(argv[5]);
  npart_sim   = (double) atof(argv[7]); 
  bin_type_rmu= atoi(argv[9]); 
  bin_type_rpp= atoi(argv[10]); 
  
  filename1  = (char *) malloc(500*sizeof(char));
  filename2  = (char *) malloc(500*sizeof(char));
  filename3  = (char *) malloc(500*sizeof(char));
  

  sprintf(filename1,"/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
           "sub_sample_data_%sMpc_%s/%s_sample/REAL/snapshot_%s_data/"
           "subsamp_param_%s.dat",argv[8], argv[7], argv[6], argv[1], argv[1]);

  cout << filename1 << "\n";
  
  FILE  *param_file=NULL;
  param_file = fopen(filename1, "r");
  
  fscanf(param_file,"%d %d %d %d %d %lf %lf %lf %lf\n", 
            &ndim, &sub_samp_size, &nsub, &ndm, &snapid, &scale_factor, 
            &HubbleParam, &Lbox, &Lbox_half);
  fclose(param_file);
 
  
  cout<<"sub_samp_size: "<<sub_samp_size<<"\n" ;
  cout<<"snapid: "<<snapid<<"\n" ;
  cout<<"ndm: "<<ndm<<"\n" ;
  cout<<"Lbox: "<<Lbox<<"\n"; 
  

  softening_length = (Lbox*1000.)/npart_sim;
  softening_length *= (1./25.);
  
  cout<<"softening length in kpc h^-1: " << softening_length << "\n";
  
  rmin      = 4.*(softening_length/1000.);
  Rp_min    = 0.0;  
  
  if (bin_type_rpp==1){
      Rperp_min = (softening_length/1000.);
  }
  else{
      Rperp_min = 0;
  }
  rmax      = Lbox/5.; 
  Rp_max    = 50.0; //Lbox/5.;
  Rperp_max = 50.0; //Lbox/5.;

  numden=(double)(sub_samp_size)/(double)(Lbox*Lbox*Lbox);
  printf("numden = %2.10f, nsub = %d ndim=%d Rmin=%0.3f Rmax=%0.3f Rperp_min=%0.3f Rperp_max=%0.3f\n",numden, nsub, ndim, rmin, rmax, Rperp_min, Rperp_max);
 


//------------------------------------------------------------------------------------------------------
//Setting bins and computing bin volume also

  rbin        = (double*) malloc((Nbr)*sizeof(double))     ;
  rp_bin      = (double*) malloc((Nb_rpar)*sizeof(double)) ;
  rperp_bin   = (double*) malloc((Nb_rperp)*sizeof(double));
  mu_bin      = (double*) malloc((Nb_mu)*sizeof(double))   ;
 
  pairs_rmu  = (long int*) malloc(Nbr*Nb_mu*sizeof(long int))       ;
  pairs_rpp  = (long int*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  
//  pairs_vpp  = (double*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
//  pairs_vpp1  = (double*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  
  pairvel_est  = (double*) malloc(Nbr*sizeof(double))   ; // Along the line of sight estimator
  CijSqr     = (double*) malloc(Nbr*sizeof(double))   ; // Along the line of sight estimator
  
  pairvel_est_los  = (double*) malloc(Nbr*sizeof(double))   ; // Along the line of sight estimator
  CijSqr_los     = (double*) malloc(Nbr*sizeof(double))   ; // Along the line of sight estimator

  pairvel_l  = (double*) malloc(Nbr*sizeof(double))     ; // Along the line of sight
  pairvel_t  = (double*) malloc(ndim*Nbr*sizeof(double)); // Along the transverse direction
  pairvdisp  = (double*) malloc(Nbr*sizeof(double))     ;
  pairvdisp_l= (double*) malloc(Nbr*sizeof(double))     ;
  pairvdisp_t= (double*) malloc(Nbr*sizeof(double))     ;
  
  binning(0, 1 , Nb_mu, mu_bin, 0)                                       ;
  binning(Rp_min   , Rp_max    , Nb_rpar , rp_bin   , 0)                 ;
  binning(rmin     , rmax      , Nbr     , rbin     , (int) bin_type_rmu);
  binning(Rperp_min, Rperp_max , Nb_rperp, rperp_bin, (int) bin_type_rpp);

  double vol_rmu[Nbr*Nb_mu]       ; // nbin1=Nbr(radial), nbin2=Nb_mu(bin in mu)
  double vol_rpp[Nb_rpar*Nb_rperp]; // nbin1=Nb_rp(rpar), nbin2=Nb_perp(bin in r_perp)
  double dmu;

 //Computing effective volume of the cylinder in (r, mu) coordinate system
  for(i=0; i < Nbr; i++){
    for(j=0; j < Nb_mu; j++){
        dmu = mu_bin[j+1]-mu_bin[j] ; // mu_bins diff
        vol_rmu[i + Nbr*j] = (2 * M_PI/3.0) * dmu * (pow(rbin[i+1],3.0) - pow(rbin[i],3.0));
        vol_rmu[i + Nbr*j] = 2 * vol_rmu[i+Nbr*j]; // The factor of 2 is for 
    } 
  }

 //Computing effective volume in (r_parallel, r_perpendicular) coordinate system

  double los_bin_diff=0.0; 
  for(i=0; i < Nb_rpar; i++){
    for(j=0; j <  Nb_rperp; j++){
        los_bin_diff = rp_bin[i+1]-rp_bin[i];
        vol_rpp[i+Nb_rpar*j] = M_PI * los_bin_diff * (pow(rperp_bin[j+1], 2.0) - pow(rperp_bin[j], 2.0));
        vol_rpp[i+Nb_rpar*j] = 2.0 * vol_rpp[i+Nb_rpar*j];
    }
  }


//------------------------------------------------------------------------------------------------------
// reading data

  double P2=0.0, P4=0.0, rbin_mid=0; 
  
  FILE *snapfile        = NULL;
  FILE *corr_rmu_file   = NULL;
  FILE *velcorr_file   = NULL;
   
  pos = (double *)malloc(sub_samp_size*ndim*sizeof(double));
  vel = (double *)malloc(sub_samp_size*ndim*sizeof(double));
  
  nsub = 20;
  
  for (k = 0; k<nsub;k++){    

    memset(pos, 0, sub_samp_size*ndim*sizeof(double)); 
    memset(vel, 0, sub_samp_size*ndim*sizeof(double)); 
    
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "KMDPL2_SubSam_%d_%d_%d_phy.bin",(int)Lbox,(int) npart_sim,
            (int) sub_samp_size, (int) snapid,(int) snapid,(int) sub_samp_size,(int) k);

    cout<<filename1<<"\n"; 
    init_pair_arrays();
    snapfile = fopen(filename1, "r");
    //fread(&nblock_start,sizeof(int),1,snapfile);
    fread(&nblock_start,sizeof(double),1,snapfile);
    fread(pos,sizeof(double), sub_samp_size*ndim, snapfile);
    fread(vel,sizeof(double), sub_samp_size*ndim, snapfile);
    fread(&nblock_end, sizeof(double),1, snapfile);
    if( nblock_start != nblock_end ) {
       printf("MISMATCH ... snapid=%lf ... QUITTING \n", nblock_start);
//       printf("MISMATCH ... snapid=%d ... QUITTING \n", nblock_start);
       return(-1);
    }//if
    fclose(snapfile);
    
// reading data done


//------------------------------------------------------------------------------------------------------
//Compute CF

    corr_2D_box_bf(sub_samp_size, Nbr, Nb_mu, Nb_rpar, Nb_rperp, pos, vel, 
                   pairs_rpp, pairs_rmu, pairvel_l, pairvel_est, CijSqr, 
                    pairvel_est_los, CijSqr_los, 
                   pairvel_t, pairvdisp, pairvdisp_l, pairvdisp_t, 
                   0, 1, rmin, rmax, Rp_min, Rp_max, 
                   Rperp_min, Rperp_max, Lbox, 
                   Lbox_half, bin_type_rmu, bin_type_rpp);

//------------------------------------------------------------------------------------------------------
//  Make various CF in (r, mu) space and pair velocities
    
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_1Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat",(int)Lbox, (int)npart_sim,(int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size, rmax, (int)Nbr, (int)Nb_mu, (int)k);
            
    cout<<filename1<<"\n"; 
    
    
    sprintf(filename2, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_2Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat",(int)Lbox, (int)npart_sim,(int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size, rmax, (int)Nbr, (int)Nb_mu, (int)k);
    cout<<filename2<<"\n"; 
     

    sprintf(filename3, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_velcorr_%d_%d_rmax_%0.1fMpc_Nbr"
            "_%d_Nbmu_%d_%d.dat",(int)Lbox, (int)npart_sim,(int) sub_samp_size,
            (int) snapid,(int) snapid,(int) sub_samp_size, rmax, (int)Nbr, (int)Nb_mu, (int)k);
    cout<<filename3<<"\n"; 
    
           

    snapfile = fopen(filename1, "w");
    fprintf(snapfile,"rbin  rbin_mid  pairs  avpairs  Xi_0  Xi_2"  
            "  X_4  Xi_par  Xi_perp  Xi_bar  err_Xi\n");
    
    corr_rmu_file = fopen(filename2, "w");
    fprintf(corr_rmu_file,"rbin  mubin  rmu_avpairs  rmu_pairs  xi_rmu\n");
    
    velcorr_file = fopen(filename3, "w");
    fprintf(velcorr_file,"rbin  rbin_mid  vel_est  vel_l  CijSqr  vel_est_los  CijSqr_los"
            "  v_mean  -2xi_01  vdisp  vel_t_x  vel_t_y  vel_t_z  vdisp_l  vdisp_t\n");

//--------------------------------------------------------------------------------------------------------------

    sum3 =0.0,sum4=0.0;
    double v_mean, xi_01, v_mean_los; 
    
    for(i=0; i<Nbr; i++) {

      sum1   = 0.0, sum2 = 0.0;
      xi_0   = 0.0, xi_2 = 0.0, xi_4 = 0.0;
      xi_rperp= 0.0, xi_rpar = 0.0;
      v_mean=0.0, xi_01 = 0.0;

      for(j=0; j<Nb_mu; j++) {
            
        dmu    =  mu_bin[j+1]-mu_bin[j];
        avpart = ((double) sub_samp_size) * vol_rmu[i+Nbr*j] * numden;
        xi_rmu = ((double) pairs_rmu[i+Nbr*j]/avpart) - 1;

        if((mu_bin[j]<0.5) && (mu_bin[j]>=0)) // For clustering Wedge see Kayzin et al. 2012
          xi_rperp += xi_rmu/dmu; //if

        if((mu_bin[j]<1) && (mu_bin[j]>=0.5))
          xi_rpar += xi_rmu/dmu; //if
        
        //  Legendre polynomial \ell =2 and \ell=4 
        
        P2 = (3. * mu_bin[j]*mu_bin[j] - 1.)*0.5;
        P4 = (35. * pow(mu_bin[j], 4) - 30. * pow(mu_bin[i], 2.)+ 3)/8.;
            
        sum1 += (double) pairs_rmu[i+Nbr*j];     //Total number of pairs in a rbin 
        sum2 += avpart;                         //Total average number of pairs in a rbin 
        
        xi_0 += xi_rmu * dmu * 0.5  ;    // monopole  since intergral instead of -1 to 1 it's 0 to 1
        xi_2 += xi_rmu * dmu * P2 * (2*2. + 1.)/2. ;   //quadrapole
        xi_4 += xi_rmu * dmu * P4 * (2*4. + 1.)/2. ;   //hexadecapole

        fprintf(corr_rmu_file,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
                rbin[i], mu_bin[j], avpart, (double) pairs_rmu[i+Nbr*j], 
                xi_rmu);
      }//for j mu bin
      
      sum3     += sum1;
      sum4     += sum2;
      xi_bar    = (sum3/sum4) - 1.0;
      rbin_mid  = (rbin[i]+rbin[i+1])*0.5;
      v_mean    = pairvel_est[i]/CijSqr[i];
      v_mean_los= pairvel_est_los[i]/CijSqr_los[i];
      xi_01     = (1+2.*xi_0)*v_mean; 


      fprintf(snapfile,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f"  
              "  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
              rbin[i], rbin_mid, sum1, sum2, 2.*xi_0, 2.*xi_2, 2.*xi_4, 
              xi_rpar, xi_rperp, xi_bar, (1. + 2.*xi_0)/sqrt(sum1));
        
      fprintf(velcorr_file, "%2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f"
              "  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n", rbin[i], 
               rbin_mid, pairvel_est[i], pairvel_l[i]/sum1, CijSqr[i], pairvel_est_los,
               CijSqr_los, v_mean, v_mean_los, xi_01, 
               pairvdisp[i]/sum1, pairvel_t[0+3*i]/sum1, pairvel_t[1+3*i]/sum1, 
               pairvel_t[2+3*i]/sum1, pairvdisp_l[i]/sum1, pairvdisp_t[i]/sum1);
               
    }//for i r bin
    
    fclose(corr_rmu_file);
    fclose(snapfile);
    fclose(velcorr_file);
    cout<<"Done computing correaltion in (r, mu) space and pair velocities"<<"\n";

//------------------------------------------------------------------------------------------    
//  Make various CF in (r, mu) space and pair velocities

    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_2Dcorr_rpp_%d_%d_rmax_%0.1fMpc_Nbrpar"
            "_%d_Nrperp_%d_%d.dat", (int) Lbox, (int) npart_sim, (int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size,  rmax,(int) Nb_rpar,
            (int) Nb_rperp,(int) k);
    
    
    snapfile = fopen(filename1, "w");
    //fprintf(snapfile,"rpar  rperp  rpp_avpairs  rpp_pairs  xi_rpp, vp_l_rpp\n");
    fprintf(snapfile,"rpar  rperp  rpp_avpairs  rpp_pairs  xi_rpp\n");
    
    for(i=0; i<Nb_rpar; i++){
      for(j=0; j<Nb_rperp; j++){
      
        avpart = ((double) sub_samp_size) * vol_rpp[i+Nb_rpar*j] * numden;
        xi_rpp = ((double) pairs_rpp[i+Nb_rpar*j]/avpart) - 1;
        
        fprintf(snapfile,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
                rp_bin[i], rperp_bin[j], avpart, (double) pairs_rpp[i+Nb_rpar*j], 
                xi_rpp);
      }// for i rp bin
    }//for j rperp_bin
    fclose(snapfile);

    cout<<"Done computing correaltion in (rpar, rperp) space"<<"\n";

  }//for k 
  cleanup();
  return 0;
}//main
//--------------------------------------------------------------------------------

double* binning(double amin, double amax , 
               int nbin, double *bins, int bin_Key){
    double temp=0.;
    double bin_fac=0.;
    if (bin_Key==1){
        bin_fac = (log10(amax)-log10(amin))/(double)(nbin);
        for(int l=0; l < nbin+1; l++){
           temp    = log10(amin) + l * bin_fac;
           bins[l] =  (pow(10., temp));
        }
    }
    else{
         bin_fac = (amax-amin)/(double)(nbin);
        for(int l=0; l < nbin+1; l++){
           temp    = (amin) + l * bin_fac;
           bins[l] =  temp;
        }

    }
    return bins;
}//binning()

void init_pair_arrays() {
  int i;
  for(i=0; i<Nb_rpar*Nb_rperp; i++) {
    pairs_rpp[i] = 0;
    }
  for(i=0; i<Nbr*Nb_mu; i++)
    pairs_rmu[i] = 0;

  for(i=0; i<Nbr; i++) {

    pairvel_l[i]        = 0.0;     
    pairvel_est[i]      = 0.0;     
    pairvel_est_los[i]  = 0.0;     
    CijSqr[i]           = 0.0;     
    CijSqr_los[i]       = 0.0;     
    pairvel_t[0+3*i]    = 0.0; 
    pairvel_t[1+3*i]    = 0.0; 
    pairvel_t[2+3*i]    = 0.0; 
    pairvdisp[i]        = 0.0;     
    pairvdisp_l[i]      = 0.0;     
    pairvdisp_t[i]      = 0.0;     

  }

}//init_pair_arrays()


void cleanup(){
  
  free(pos);
  free(vel);
  free(rbin);
  free(rp_bin);
  free(rperp_bin);
  free(mu_bin);
  free(pairs_rmu);
  free(pairs_rpp);
  free(pairvel_l);
  free(pairvel_est);
  free(CijSqr);
  free(pairvel_est_los);
  free(CijSqr_los);
  free(pairvel_t);
  free(pairvdisp);
  free(pairvdisp_l);
  free(pairvdisp_t);
  free(filename1);
  free(filename2);
  free(filename3);

}

//------------------------------------------------------------------------

void  corr_2D_box_bf(int np, int nbin, int nb_mu, int nb_rp, int nb_rperp, 
                     double *pos, double *vel, long int *p_rpp, long int *p_rmu, 
                     double *v_l_hh, double *v_est_hh, double *cij_sqr,double *v_est_los_hh, 
                     double *cij_sqr_los, double *v_t_hh, double *vdis_hh, 
                     double *vdis_l_hh, double *vdis_t_hh, 
                     double mu_min, double mu_max, double rmin, double rmax, 
                     double rp_min, double rp_max, double rperp_min, double rperp_max,
                     double l_box, double l_box_half, int bin_key, int bin_key1){

 //////
 // Correlator for monopole in the periodic-box case
 // by brute-force
 

 double delta_r;
 double delta_rperp;
 if(bin_key==1){ 
   delta_r     = log10(rmax/rmin)/(double) nbin;
 }
 else{
   delta_r     = (rmax-rmin)/(double) nbin;
 }
 
 if(bin_key1==1){ 
   delta_rperp = log10(rperp_max/rperp_min)/(double) nb_rperp;
 }
 else{
   delta_rperp = (rperp_max-rperp_min)/(double) nb_rperp;
 }

 double delta_rp    = (rp_max-rp_min)/(double) nb_rp;
 double delta_mu = (mu_max-mu_min)/(double) nb_mu;
  
#pragma omp parallel default(none)      \
  shared(np, nbin, nb_mu, nb_rp, nb_rperp,\
         pos, vel, p_rpp, p_rmu, mu_min, mu_max, cij_sqr,\
         v_l_hh, v_est_hh, v_est_los_hh, cij_sqr_los, v_t_hh, vdis_hh, vdis_l_hh,\
         vdis_t_hh, rmin, rmax, rp_min, rp_max, rperp_min,\
         rperp_max, l_box, l_box_half, bin_key, bin_key1, delta_r, delta_rp, delta_rperp, delta_mu)
  {
  

    int ii, kk;
    long int p_rpp_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
//    double   p_vpp_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
//    double   p_vpp1_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu

    long int p_rmu_hthread[nbin*nb_mu]; //Histogram filled by each thread for log(r_parallel)-log(rp)

    double v_l_hthread[nbin]; //Histogram filled by each thread
    double v_est_hthread[nbin]; //Histogram filled by each thread
    double cij_sqr_hthread[nbin]; //Histogram filled by each thread
    
    double v_est_los_hthread[nbin]; //Histogram filled by each thread
    double cij_sqr_los_hthread[nbin]; //Histogram filled by each thread
 
    double v_t_hthread_x[nbin]; //Histogram filled by each thread
    double v_t_hthread_y[nbin]; //Histogram filled by each thread
    double v_t_hthread_z[nbin]; //Histogram filled by each thread
    double vdis_hthread[nbin]; //Histogram filled by each thread
    double vdis_l_hthread[nbin]; //Histogram filled by each thread
    double vdis_t_hthread[nbin]; //Histogram filled by each thread
    
    for(ii=0;ii<nb_rp*nb_rperp;ii++){
        p_rpp_hthread[ii]=0.; //Clear private histogram
    }
    
    for(ii=0;ii<nbin*nb_mu;ii++)
      p_rmu_hthread[ii]=0.; //Clear private histogram
   
    for(ii=0;ii<nbin;ii++){
      v_l_hthread[ii]=0.; //Clear private histogram
      v_est_hthread[ii]=0.; //Clear private histogram
      v_est_los_hthread[ii]=0.; //Clear private histogram
      cij_sqr_los_hthread[ii]=0.; //Clear private histogram
      v_t_hthread_x[ii]=0.; //Clear private histogram
      v_t_hthread_y[ii]=0.; //Clear private histogram
      v_t_hthread_z[ii]=0.; //Clear private histogram
      vdis_hthread[ii]=0.; //Clear private histogram
      vdis_l_hthread[ii]=0.; //Clear private histogram
      vdis_t_hthread[ii]=0.; //Clear private histogram
    }


#pragma omp for nowait schedule(dynamic)
    for(ii=0;ii<np;ii++) {
    

      int jj, m; 
      double *pos1=&(pos[3*ii]);
      double *vel1=&(vel[3*ii]);
      
      for(jj=0;jj<np;jj++) {

        double xr[3], mu;
        double r2, pi, rperp;
        int ir, icth, ipi, irprep;        
        double vr[3];
        double  c_ij=0.0, c_ij_los=0.0;
        double v2, rdotv, rCrossv[3];
        double v2dotx2, v1dotx1;   
        double v2dotx2_los, v1dotx1_los;   
        double rdotx1, rdotx2, x1_sqr, x2_sqr;
        double vtdisp;

        xr[0]=(pos1[0]-pos[3*jj]);
        xr[1]=(pos1[1]-pos[3*jj+1]);
        xr[2]=(pos1[2]-pos[3*jj+2]);
        
        vr[0] = (vel1[0]-vel[0+3*jj]);
        vr[1] = (vel1[1]-vel[1+3*jj]);
        vr[2] = (vel1[2]-vel[2+3*jj]);
        

 
        if(fabs(xr[0])>l_box_half){
            xr[0]=l_box-fabs(xr[0]);
            if (xr[0]<0){
                xr[0]=-1.0*fabs(xr[0]);
            }
            else{
                xr[0]=1.0*fabs(xr[0]);
            }
        }
        if(fabs(xr[1])>l_box_half){
            xr[1]=l_box-fabs(xr[1]);
            if (xr[1]<0){
                xr[1]=-1.0*fabs(xr[1]);
            }
            else{
                xr[1]=1.0*fabs(xr[1]);
            }
        }
        if(fabs(xr[2])>l_box_half){
            xr[2]=l_box-fabs(xr[2]);
            if (xr[2]<0){
                xr[2]=-1.0*fabs(xr[2]);
            }
            else{
                xr[2]=1.0*fabs(xr[2]);
            }
        }
        
        
//--------Estimator by Firrera & Juszkowize ------------

        x1_sqr    = pos1[0]*pos1[0] + pos1[1]*pos1[1] + pos1[2]*pos1[2];
        x2_sqr    = pos[3*jj]*pos[3*jj]+ pos[1+3*jj]*pos[1+3*jj] + pos[2+3*jj]*pos[2+3*jj];
        
        rdotx1 = xr[0]*pos1[0] + xr[1]*pos1[1] + xr[2]*pos1[2];
        rdotx2 = xr[0]*pos[3*jj] + xr[1]*pos[1+3*jj] + xr[2]*pos[2+3*jj];

        v1dotx1 = pos1[0]*vel1[0] + pos1[1]*vel1[1] + pos1[2]*vel1[2];
        v2dotx2 = pos[3*jj]*vel[3*jj] + pos[1+3*jj]*vel[1+3*jj] + pos[2+3*jj]*vel[2+3*jj];

        r2         = xr[0]*xr[0]+xr[1]*xr[1]+xr[2]*xr[2]; 
        v2         = vr[0]*vr[0]+vr[1]*vr[1]+vr[2]*vr[2]; 
        rdotv      = xr[0]*vr[0]+xr[1]*vr[1]+xr[2]*vr[2]; 
        
        // This will only include los effects
        
        v1dotx1_los = vel1[2];     // This is in Kaiser limit small angle approximation 
        v2dotx2_los = vel[2+3*jj]; // in this v1.x1 is just v1_z and cij is mu angle
        c_ij_los    = xr[2]/sqrt(r2);    // it's nothing but \mu the direction cosine along the los
        
        // This will include wide angle effects
        
        v1dotx1 = v1dotx1/sqrt(x1_sqr);
        v2dotx2 = v2dotx2/sqrt(x2_sqr);
        c_ij = (rdotx1/2./sqrt(r2)/sqrt(x1_sqr))  +  (rdotx2/2./sqrt(r2)/sqrt(x2_sqr));

//----Simple our estimator for pair vel -------------------------------

        rCrossv[0] = xr[1]*vr[2]-xr[2]*vr[1];
		rCrossv[1] = xr[2]*vr[0]-xr[0]*vr[2];
		rCrossv[2] = xr[0]*vr[1]-xr[1]*vr[0];


//************************log(r)-mu*********************
        mu = fabs(xr[2]/sqrt(r2)); 
        pi = fabs(xr[2]);
        rperp = sqrt(xr[0]*xr[0]+xr[1]*xr[1]);


        if(bin_key==1){ // log binning
            if( (sqrt(r2)>=rmin) && (sqrt(r2)<=rmax) ){

                ir                  = (int)(log10(sqrt(r2)/rmin)/delta_r); 
                v_l_hthread[ir]     = v_l_hthread[ir]+ rdotv/r2;  // its v_12/r that's why r2
                cij_sqr_hthread[ir] = cij_sqr_hthread[ir] + (c_ij*c_ij);
                v_est_hthread[ir]   = v_est_hthread[ir] + (v1dotx1-v2dotx2)*c_ij;  // its v_12/r that's why r2
                
                cij_sqr_los_hthread[ir] = cij_sqr_los_hthread[ir] + (c_ij_los*c_ij_los);
                v_est_los_hthread[ir]   = v_est_los_hthread[ir] + (v1dotx1_los-v2dotx2_los)*c_ij_los;  // its v_12/r that's why r2
         
                vdis_hthread[ir]    = vdis_hthread[ir] + v2/r2;
                vdis_l_hthread[ir]  = vdis_hthread[ir] + (rdotv*rdotv)/(r2*r2);
               
                v_t_hthread_x[ir] = v_t_hthread_x[ir]+ rCrossv[0]/r2;
                v_t_hthread_y[ir] = v_t_hthread_y[ir]+ rCrossv[1]/r2;
                v_t_hthread_z[ir] = v_t_hthread_z[ir]+ rCrossv[2]/r2;

                vtdisp=0.0;
                for(m=0; m<3;m++)
                   vtdisp  = vtdisp + rCrossv[m]*rCrossv[m]; //for m... all direction
                vdis_t_hthread[ir] = vdis_t_hthread[ir] + vtdisp/(r2*r2);

                if( (mu>=0) && (mu<=1) ){
                    icth = (int)((mu-mu_min)/delta_mu);
                    p_rmu_hthread[ir+nbin*icth]+=1;
                }//if mu
            }//if r
        }

//************************(r)-mu*********************
        
        else{ // if you want linear bins in r
            if( (sqrt(r2)>=rmin)&& (sqrt(r2)<=rmax) ){
            
              ir =(int)((sqrt(r2)-rmin)/delta_r); 
              v_l_hthread[ir]     = v_l_hthread[ir]+ rdotv/r2;  // its v_12/r that's why r2
              cij_sqr_hthread[ir] = cij_sqr_hthread[ir] + (c_ij*c_ij);
              v_est_hthread[ir]   = v_est_hthread[ir] + (v1dotx1-v2dotx2)*c_ij;  // its v_12/r that's why r2
              
              cij_sqr_los_hthread[ir] = cij_sqr_los_hthread[ir] + (c_ij_los*c_ij_los);
              v_est_los_hthread[ir]   = v_est_los_hthread[ir] + (v1dotx1_los-v2dotx2_los)*c_ij_los;  // its v_12/r that's why r2


              vdis_hthread[ir]  = vdis_hthread[ir] + v2/r2;
              vdis_l_hthread[ir]= vdis_hthread[ir] + (rdotv*rdotv)/(r2*r2);
           
              v_t_hthread_x[ir] = v_t_hthread_x[ir]+ rCrossv[0]/r2;
              v_t_hthread_y[ir] = v_t_hthread_y[ir]+ rCrossv[1]/r2;
              v_t_hthread_z[ir] = v_t_hthread_z[ir]+ rCrossv[2]/r2;

              vtdisp=0.0;
              for(m=0; m<3;m++)
                vtdisp  = vtdisp + rCrossv[m]*rCrossv[m];//for m... all direction
                vdis_t_hthread[ir] = vdis_t_hthread[ir] + vtdisp/(r2*r2);
              if( (mu>=0) && (mu<=1) ){
                icth = (int)((mu-mu_min)/delta_mu);
                p_rmu_hthread[ir+nbin*icth]+=1;
              }//if mu
           }//if r
        }

//************************log(rpar)-rper*********************

        if(bin_key1==1){ // log binning
            if( (pi>=rp_min) && (pi<=rp_max) ){
              if( (rperp>=rperp_min) && (rperp<=rperp_max) ){
                //ipi     =  (int)(log10(pi/rp_min)/delta_rp);
                ipi     =  (int)((pi-rp_min)/delta_rp);
                irprep  =  (int)(log10(rperp/rperp_min)/delta_rperp);
                p_rpp_hthread[ipi+nb_rp*(irprep)]+=1;
              }//if rper
            }//if rpar
        }
//************************rpar-rper*********************
        else{
          if( (pi>=rp_min) && (pi<=rp_max) ){
            if( (rperp>=rperp_min) && (rperp<=rperp_max) ){
              ipi     =  (int)((pi-rp_min)/delta_rp);
              irprep  =  (int)((rperp-rperp_min)/delta_rperp);
              p_rpp_hthread[ipi+nb_rp*(irprep)]+= 1;
            }//if rper
          }//if rpar
        }
      }//jj
    }//ii
    
#pragma omp critical
    {
      
      for(ii=0;ii<nbin;ii++) //Check bound
        for(kk=0;kk<nb_mu;kk++){ //Check bound
            p_rmu[ii+nbin*kk]+= p_rmu_hthread[ii+nbin*kk]; //Add private histograms to shared one
        }
      for(ii=0;ii<nb_rp;ii++) //Check bound
        for(kk=0;kk<nb_rperp;kk++){ //Check bound
            p_rpp[ii+nb_rp*kk]+= p_rpp_hthread[ii+nb_rp*kk]; //Add private histograms to shared one
        }

      for(ii=0;ii<nbin;ii++){ //Check bound

        v_est_hh[ii]     += v_est_hthread[ii];
        v_est_los_hh[ii] += v_est_los_hthread[ii];
        cij_sqr[ii]      += cij_sqr_hthread[ii];
        cij_sqr_los[ii]  += cij_sqr_los_hthread[ii];
        
        v_l_hh[ii]     += v_l_hthread[ii]; //Add private histograms to shared one
        v_t_hh[0+3*ii] += v_t_hthread_x[ii]; //Add private histograms to shared one
        v_t_hh[1+3*ii] += v_t_hthread_y[ii]; //Add private histograms to shared one
        v_t_hh[2+3*ii] += v_t_hthread_z[ii]; //Add private histograms to shared one
        vdis_hh[ii]    += vdis_hthread[ii]; //Add private histograms to shared one
        vdis_l_hh[ii]  += vdis_l_hthread[ii]; //Add private histograms to shared one
        vdis_t_hh[ii]  += vdis_t_hthread[ii]; //Add private histograms to shared one
      }

    } // end pragma omp critical
    
  } // end pragma omp parallel

}


