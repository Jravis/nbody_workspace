
#################################################################
# Copyright (c) 2019, Sandeep Rana                              #
#################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from astropy.cosmology import LambdaCDM
from astropy.io import ascii
import argparse
sys.path.insert(1, '../Gadget2_helper_routines/')
import Cosmo_Growth_Fac_raw as CGF_raw
import readsnap
#
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")

#-----------------------------------------------------

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    parser.add_argument("n_sample", type=int, 
    help='number of subsample')
    parser.add_argument("samp_size", type=int, 
    help='number of subsample')


    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    n_sample   = args.n_sample
    samp_size  = args.samp_size 
    

    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)

    inp_file = dir_name +"snapshot_%03d.0"%17
    print inp_file
    print "-------------KMDPL2 header-----------\n"
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    print('-'*20)
    
    L_boxhalf = Lbox/2.
    print "Lbox: ", Lbox, "NpartSim: ", NpartSim
    
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
 
    OmegaMatter = snapshot.Omega0
    OmegaLambda = snapshot.OmegaLambda
    H0 = snapshot.HubbleParam*100.
    cosmo  = LambdaCDM(H0= snapshot.HubbleParam*100, Om0 = OmegaMatter, Ode0= OmegaLambda)
    model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)
    D  = model.growth_factor_fnc(snapshot.redshift)    

#-----------------------------------------------------------


    snapshots = np.array([16, 15, 13, 11, 8])

    ii = 0
    for snapnumber in snapshots:
        dir_name1 = ("/mnt/data1/sandeep/Kalinga_run_Analysis/"\
                    "kalinga_velocity_correlation/sub_sample_data"\
                    "_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
                    "new_corr_results/"%(Lbox, NpartSim, samp_size, snapnumber))

        inp_file = dir_name +"snapshot_%03d.0"%snapnumber
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)

        scale_factor = 1./(1.+snapshot.redshift)

        D  = model.growth_factor_fnc(snapshot.redshift)    
        f  = model.growth_rate_fnc(snapshot.redshift)
        Hz = cosmo.H(snapshot.redshift).value
        
        corr_file    = (dir_name1+"logbin_1Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"\
                       "_50_Nbmu_50_%d.dat"%(snapnumber, samp_size, Lbox/5., 0))

        rbin     = (np.genfromtxt(corr_file, usecols=0))[1:]
        rbin_mid = (np.genfromtxt(corr_file, usecols=1))[1:]
        
        Xi        = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # comoving Xi spatial corr func
        Xi_D2     = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # comoving Xi spatial corr func by D+**2
        Xibr      = np.zeros((n_sample, len(rbin)), dtype=np.float64)
        Xibr_D2   = np.zeros((n_sample, len(rbin)), dtype=np.float64)
        Xi2       = np.zeros((n_sample, len(rbin)), dtype=np.float64)
        Xi4       = np.zeros((n_sample, len(rbin)), dtype=np.float64)

        Xi_wedge_par    = np.zeros((n_sample, len(rbin)), dtype=np.float64)
        Xi_wedge_perp   = np.zeros((n_sample, len(rbin)), dtype=np.float64)

        Xibr_f    = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # 2/3 Xibarf
        Xibr_D2_f = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # (2/3 Xibarf)/D+**2
        v_x       = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # (v.x)/x  in comoving
        v_x_H     = np.zeros((n_sample, len(rbin)), dtype=np.float64)      #  (v.x)/(x*H)
        vdisp     = np.zeros((n_sample, len(rbin)), dtype=np.float64)      # (v/x)**2
    

        for count in xrange(n_sample):

            fin    = (dir_name1+"logbin_1Dcorr_rmu_%d_%d_rmax_%0.1fMpc_Nbr"\
                     "_50_Nbmu_50_%d.dat"%(snapnumber, samp_size, Lbox/5.0, count))
                     
            Xi[count,:]     = (np.genfromtxt(fin, usecols=4))[1:]
            Xi_D2[count,:]  = (np.genfromtxt(fin, usecols=4)/D**2)[1:]
        
            Xi2[count,:]     = (np.genfromtxt(fin, usecols=5))[1:]
            Xi4[count,:]     = (np.genfromtxt(fin, usecols=6))[1:]

            Xi_wedge_par[count,:]   = (np.genfromtxt(fin, usecols=7))[1:]
            Xi_wedge_perp[count,:]  = (np.genfromtxt(fin, usecols=8))[1:]
            Xibr[count,:]      = (np.genfromtxt(fin, usecols=9))[1:]
            Xibr_D2[count,:]   = (np.genfromtxt(fin, usecols=9)/D**2)[1:]
            Xibr_f[count,:]    = (np.genfromtxt(fin, usecols=9)*f*2/3.)[1:]
            Xibr_D2_f[count,:] = (np.genfromtxt(fin, usecols=9)*f*2/(3.*D**2))[1:]
            
            fin = (dir_name1+"logbin_velcorr_%d_%d_rmax_%0.1fMpc_Nbr"\
                  "_50_Nbmu_50_%d.dat"%(snapnumber, samp_size, Lbox/5.0, count))
                  
            v_x[count,:]    = (np.genfromtxt(fin, usecols=2))[1:]
            v_x_H[count,:]  = (np.genfromtxt(fin, usecols=2)*(-1./Hz))[1:]
            vdisp[count,:]  = (np.genfromtxt(fin, usecols=3))[1:]

        Xi_mean     = np.mean(Xi, axis=0)
        #print Xi_mean
        Xi_std      = np.std(Xi, axis=0)
        Xibr_mean   = np.mean(Xibr, axis=0)
        Xibr_std    = np.std(Xibr, axis=0)
        v_x_mean    = np.mean(v_x, axis=0)
        v_x_std     = np.std(v_x, axis=0)
        vdisp_mean  = np.mean(vdisp, axis=0)
        vdisp_std   = np.mean(vdisp, axis=0)

        Xi_D2_mean  = np.mean(Xi_D2, axis=0)
        #print Xi_D2_mean
        Xi_D2_std   = np.std(Xi_D2, axis=0)
        Xibr_f_mean = np.mean(Xibr_f, axis=0)
        Xibr_f_std  = np.mean(Xibr_f, axis=0)
        
        Xibr_D2_mean   = np.mean(Xibr_D2, axis=0)
        Xibr_D2_std    = np.std(Xibr_D2, axis=0)
        Xibr_D2_f_mean = np.mean(Xibr_D2_f, axis=0)
        Xibr_D2_f_std  = np.mean(Xibr_D2_f, axis=0)

        v_x_H_mean  = np.mean(v_x_H, axis=0)
        v_x_H_std   = np.std(v_x_H, axis=0)

        Xi2_mean     = np.mean(Xi2, axis=0)
        Xi2_std      = np.std(Xi2, axis=0)
        Xi4_mean     = np.mean(Xi4, axis=0)
        Xi4_std      = np.std(Xi4, axis=0)

        Xi_wedge_par_mean  = np.mean(Xi_wedge_par, axis=0)
        Xi_wedge_par_std   = np.std(Xi_wedge_par, axis=0)

        Xi_wedge_perp_mean  = np.mean(Xi_wedge_perp, axis=0)
        Xi_wedge_perp_std   = np.std(Xi_wedge_perp, axis=0)

        Xi_Cov   = np.cov(Xi.T)
        Xibr_Cov = np.cov(Xibr.T)
        Xi_corrcoef = np.corrcoef(Xi.T)
        Xibr_corrcoef = np.corrcoef(Xibr.T)
    

        fout  = (dir_name1+"Avg_stat_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))
        fout1 = (dir_name1+"Cosmo_quant_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax_%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))
        print fout
    
        table = [rbin, rbin_mid,  Xi_mean, Xi_std, Xi2_mean, Xi2_std, Xi4_mean, 
                 Xi4_std, Xi_wedge_par_mean, Xi_wedge_par_std, Xi_wedge_perp_mean, 
                 Xi_wedge_perp_std, Xibr_mean, Xibr_std, 
                 Xi_D2_mean, Xi_D2_std, Xibr_D2_mean, Xi_D2_std,
                 Xibr_f_mean, Xibr_f_std, Xibr_D2_f_mean, Xibr_D2_f_std,
                 v_x_mean, v_x_std, v_x_H_mean, v_x_H_std, 
                 vdisp_mean, vdisp_std ]

        table1 = [[D], [f], [scale_factor], [snapshot.redshift], [Hz]]       
    
        names   = ['rbin', 'rbin_mid',  'Xi_mean', 'Xi_std', 'Xi2_mean', 'Xi2_std', 'Xi4_mean', 
                   'Xi4_std', 'Xi_wedge_par_mean', 'Xi_wedge_par_std', 'Xi_wedge_perp_mean', 
                   'Xi_wedge_perp_std', 'Xibr_mean', 'Xibr_std', 'Xi_D2_mean',
                   'Xi_D2_std', 'Xibr_D2_mean', 'Xibr_D2_std', 'Xibr_f_mean', 'Xibr_f_std', 
                   'Xibr_D2_f_mean', 'Xibr_D2_f_std', 'v_x_mean', 'v_x_std', 'v_x_H_mean', 'v_x_H_std', 
                   'vdisp_mean', 'vdisp_std'] 

        names1  = ['D+', 'fgrowth', 'scale_factor', 'redshift', 'Hz']        
            
        Formats = {'rbin':'%2.14f', 'rbin_mid':'%2.14f', 'Xi_mean':'%2.14f', 'Xi_std':'%2.14f',  
                   'Xi2_mean':'%2.14f', 'Xi2_std':'%2.14f', 'Xi4_mean':'%2.14f', 'Xi4_std':'%2.14f', 
                   'Xi_wedge_par_mean':'%2.14f', 'Xi_wedge_par_std':'%2.14f', 'Xi_wedge_perp_mean':'%2.14f', 
                   'Xi_wedge_perp_std':'%2.14f','Xibr_mean':'%2.14f', 'Xibr_std':'%2.14f', 
                   'Xi_D2_mean':'%2.14f', 'Xi_D2_std':'%2.14f','Xibr_D2_mean':'%2.14f', 'Xibr_D2_std':'%2.14f', 
                   'Xibr_f_mean':'%2.14f', 'Xibr_f_std':'%2.14f','Xibr_D2_f_mean':'%2.14f', 'Xibr_D2_f_std':'%2.14f', 
                   'v_x_mean':'%2.14f', 'v_x_std':'%2.14f', 'v_x_H_mean':'%2.14f', 'v_x_H_std':'%2.14f', 
                   'vdisp_mean':'%2.14f', 'vdisp_std':'%2.14f' }
               
        Formats1= {'D+':'%2.14f', 'fgrowth':'%2.14f', 'scale_factor':'%2.14f', 'redshift':'%2.14f', 'Hz':'%2.14f'}
    
        ascii.write(table, fout, names=names, delimiter='\t', formats=Formats, overwrite=True)
        ascii.write(table1, fout1, names=names1, delimiter='\t', formats=Formats1, overwrite=True)


        fout2 = (dir_name1+"Cov_Xi_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))


        np.savetxt(fout2, Xi_Cov, fmt="%lf")

        fout3 = (dir_name1+"Cov_Xibr_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))

        np.savetxt(fout3, Xibr_Cov, fmt="%lf")

        fout4 = (dir_name1+"Corr_Xi_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))

        np.savetxt(fout4, Xi_corrcoef, fmt="%lf")

        fout5 = (dir_name1+"Corr_Xibr_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_samp_size-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))

        np.savetxt(fout5, Xibr_corrcoef, fmt="%lf")


if __name__ == "__main__":
    main()





