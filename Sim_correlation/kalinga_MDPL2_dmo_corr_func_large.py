

#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# Fast grid based correlation fucntion routine for DM subsample #
# it use Corrfunc package by Manodeep Sinha.                    #
#################################################################


import numpy as np
import sys,os,string,time,re,struct

import argparse
from Corrfunc.theory.xi import xi as xi_corr
from Corrfunc.io import read_catalog
from Corrfunc.utils import convert_3d_counts_to_cf
from astropy.cosmology import LambdaCDM
import pandas as pd 
sys.path.insert(1, '..//Gadget2_helper_routines/')
import readsnap




def MDPL2_dmo_correlation(dir_name, samp_size, n_samples, Lbox, numden, 
                          NpartSim, snapID, counter):
    """
    """


    print("Enter number of rbins\n")
    nbins   = np.int32(raw_input(""))
    print("Enter number of thread you want to use\n")
    nthreads= np.int32(raw_input(""))
    

    rmin = 4*(np.float64(Lbox)/np.float64(NpartSim)/25.)
    
    print "Lbox: ", Lbox, "Npart_Sim: ", NpartSim
    rmax = Lbox/5.0
    print "rmin: %0.4f, rmax:%0.4f"%(rmin, rmax)
    
    rbins   = np.logspace(np.log10(rmin), np.log10(rmax), nbins + 1)
    xi1     = np.zeros(nbins, dtype=np.float64)
    xi      = np.zeros(nbins, dtype=np.float64)
    xi_bar  = np.zeros(nbins, dtype=np.float64)
    ravg    = np.zeros(nbins, dtype=np.float64)
    r_min   = np.zeros(nbins, dtype=np.float64)
    r_max   = np.zeros(nbins, dtype=np.float64)
    npairs  = np.zeros(nbins, dtype=np.float64)
    err     = np.zeros(nbins, dtype=np.float64)
    Avpart     = np.zeros(nbins, dtype=np.float64)
    
    # Input subsample file name
    infile = dir_name+"KMDPL2_SubSam_%d_%d_%d_phy.bin"%(snapID, samp_size, counter)    
    print infile
    

    f = open(infile,'rb')    # rb is for read binary
    blockini = np.fromfile(f, dtype=np.float64, count=1)
    data_pos = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    data_vel = np.fromfile(f, dtype=np.float64, count=int(samp_size*3))
    blockfin = np.fromfile(f, dtype=np.float64, count=1)
    f.close()
    if blockini != blockfin:
        raise ValueError("Block doesn't match")

    data_pos = data_pos.reshape(samp_size, 3)
    X = data_pos[:, 0]
    Y = data_pos[:, 1]
    Z = data_pos[:, 2]

    weights = np.ones_like(X)   
    xi_counts = xi_corr(Lbox, nthreads, rbins, X, Y, Z, weights=weights, 
                            weight_type='pair_product', verbose=True, output_ravg=True)

    del X; del Y; del Z
    
    count=0
    sum1=0
    sum2=0
    for r in xi_counts: 
        
        vol = (4.0*np.pi/3.0) * ( r['rmax']**3.0 - r['rmin']**3.0 )
        avpart= np.float64(samp_size) * vol * numden;
        
        xi1[count]    = (np.float64(r['npairs'])/avpart) - 1.
        sum2         += r['npairs']
        sum1         += avpart
        xi_bar[count] = (sum2/sum1) - 1.0

        xi[count]      = r['xi']
        npairs[count]  = r['npairs']
        ravg[count]    = r['ravg']
        r_min[count]   = r['rmin']
        r_max[count]   = r['rmax']
        err[count]     = (1.+r['xi'])/np.sqrt(r['npairs'])
        Avpart[count]  = avpart
        count+=1
        

    outfile = (dir_name+ "/corr_func_results/KMDPL2_Xi_"\
               "%dMpc%d_SnapID_%d_SampSize_%d_%d.txt"%(Lbox, NpartSim, snapID, samp_size, counter))
    np.savetxt(outfile, zip(r_min, r_max, ravg, xi, npairs, Avpart, xi1, xi_bar, err), 
               fmt="%2.14f", delimiter='\t')
 

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 17 for KMDPL2')

    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 or 800000')
    parser.add_argument("nsamples", type=int, 
    help='Total number of Subsample drawn 
         from parent sample e.g 50 or 40')

    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    snapID     = args.snapID
    samp_size  = args.samp_size
    nsamples   = args.nsamples

    dir_name="/mnt/data1/sandeep/Kalinga_run_Analysis/"\
             "%dMpc_%d/kalinga_snapdir_seed_1690811/"%(Lbox, NpartSim)


    inp_file = dir_name +"snapshot_%03d.0"%snapID
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    
    L_boxhalf = Lbox/2.
    numden = np.float64(samp_size/Lbox**3)
    print "Lbox: ", Lbox, "Npart_Sim: ", NpartSim

    dir_name = ("/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation"\
                "/sub_sample_data_%dMpc_%d/%d_sample/RSD/snapshot_%d_data/"%(Lbox, NpartSim, samp_size, snapID))
    print dir_name
    for ii in xrange(nsamples):    
       MDPL2_dmo_correlation(dir_name, samp_size, nsamples, Lbox, 
                             numden, NpartSim, snapID, ii)
       



if __name__ == "__main__":
    main()










