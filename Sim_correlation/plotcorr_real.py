import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from astropy.cosmology import LambdaCDM
from lmfit.models import LinearModel
from astropy.io import ascii
from matplotlib.colors import LogNorm

import argparse
from sphviewer.tools import QuickView
from sphviewer.tools.cmaps import *

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")

#mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

from collections import OrderedDict

linestyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])


#------------------------------------------------------------------------------------


def plot_Xi_types(plot_dir, Lbox, Npart, Xi_D2, Xi2, xi_th, r_th, rbin_mid, snap_red):

    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '<', 's', 'D']

    ax1.plot(r_th, xi_th, linestyle='-', color='orangered', 
             linewidth=2, mew=1.8, marker='', ms=12, 
             label=r"$\xi_{\rm{Linear}}$")

    print"----------------Plotting Xi mean--------------------" 

    ax1.axhline(y=1, color='k', linestyle='--', lw=2)
    for i in xrange(len(snap_red)):
    
        ax1.plot(rbin_mid, Xi_D2[i,:], linestyle='-', linewidth=1.8, 
                 alpha=0.9, mew=1.2, marker=mark[i], ms=8, 
                 label=r"$\rm{z}=%0.2f$"%snap_red[i])
        
        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
                       
        ax1.set_ylabel(r'$\xi_{\rm{DM}}(r)/D_{+}^{2}$', fontsize=22)
        ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_ylim(0.0004, 1000)
        ax1.set_xlim(0.1, 210)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=1)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        
    fout = (plot_dir+"KMDPL2_Xi_D2_%dMpc%d_all_SampSize-400000"\
            "_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))
            
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    
    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '<', 's', 'D']

    print"----------------Plotting Xi_2 mean--------------------" 
    
    ax1.axhline(y=0, color='k', linestyle='--', lw=2)

    for i in xrange(len(snap_red)):
    
        ax1.plot(rbin_mid, rbin_mid**2*Xi2[i,:], linestyle='', 
                 linewidth=1.8, alpha=0.9, mew=1.5, marker=mark[i], 
                 ms=8, label=r"$\rm{z}=%0.2f$"%snap_red[i])
                 
        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
        
        ax1.set_ylabel(r'$r^{2}\xi_{2}^{\rm{DM}}(r)$', fontsize=22)
        ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
        ax1.set_xscale("log")
    #    ax1.set_yscale("log")
        #ax1.set_ylim(0.004, 1000)
        ax1.set_xlim(0.1, 210)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=2)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        
    fout = (plot_dir+"KMDPL2_Xi2_%dMpc%d_all_SampSize-400000"\
            "_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

#------------------------------------------------------------------------------------

def plot_Xibr(plot_dir, Lbox, Npart, Xibr_D2, 
              xibr_th, r_th, rbin_mid, snap_red):

    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '<', 's']
    mark = ['d', 'o', '<', '^', 's']
    

    ax1.plot(r_th, xibr_th, linestyle='-', color='orangered', 
             linewidth=2, mew=1.8, marker='', ms=12, label=
             r"$\xi_{\rm{Linear}}$")

    print"----------------Plotting Xibr mean--------------------" 

    ax1.axhline(y=1, color='k', linestyle='--', lw=2)
    for i in xrange(len(snap_red)):
    
        ax1.plot(rbin_mid, Xibr_D2[i,:], linestyle='-', 
                 linewidth=1.8, alpha=0.9, mew=1.5, marker=mark[i], 
                 ms=8, label=r"$\rm{z}=%0.2f$"%snap_red[i])
                 
        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
                       
        ax1.set_ylabel(r'$\bar{\xi}_{\rm{DM}}(r)/D_{+}^{2}$', fontsize=22)
        ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        
        ax1.set_ylim(0.0004, 1000)
        ax1.set_xlim(0.1, 220)
        
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=1)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        
    fout = (plot_dir+"KMDPL2_Xibr_D2_%dMpc%d_all_SampSize-400000"\
            "_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))
            
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()


#------------------------------------------------------------------------------------

def plot_pair_velocity(plot_dir, Lbox, Npart, Xibr_mean, Xibr_f_mean, 
                       Xibr_D2_f_mean, vp, vp1, rbin_mid, snap_red):
                       

    fig= plt.figure(1, figsize=(8, 6.5))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])

    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    
    mark = ['d', 'o', '<', '^', 's']

    print"----------------Plotting Xibr mean vs -vp/Hr --------------------"
    
    ax1.axhline(y=1, color='k', linestyle='--', lw=2)
    for i in xrange(len(snap_red)):
    
        ax1.plot(Xibr_mean[i, :], vp[i, :], linestyle='-', 
                 linewidth=1.8, alpha=0.9, mew=1.5, marker=
                 mark[i], ms=8, label=r"$\rm{z}=%0.2f$"%snap_red[i])

        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
        
        ax1.set_ylabel(r'$-\rm{v}_{p}(\rm{r})/\rm{Hr}$', fontsize=22)
        ax1.set_xlabel(r'$\bar{\xi} (\rm{r})$', fontsize=22)
       
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_ylim(0.0001, 10)
        ax1.set_xlim(0.0001, 1000)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)

    fout = (plot_dir+"KMDPL2_Xibr_pairvel_%dMpc%d_all_SampSize"\
            "-400000_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))

    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    

    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '<', '^', 's']
    print"----------------Plotting 2*f*Xibr/3 mean vs -vp/Hr --------------------"
    
    for i in xrange(len(snap_red)):
        
        ax1.plot(Xibr_f_mean[i,:], vp[i,:], linestyle='-', 
                 linewidth=1.8, mew=1.5, marker=mark[i], 
                 ms=8, label=r"$\rm{z}=%0.2f$"%snap_red[i])

        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
        
        ax1.set_ylabel(r'$-\rm{v}_{p}(\rm{r})/\rm{Hr}$', fontsize=22)
        ax1.set_xlabel(r'$2 f \bar{\xi}(\rm{r})/3$', fontsize=22)
       
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        
        ax1.set_ylim(0.0001, 10)
        ax1.set_xlim(0.0001, 1000)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=4)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)

    fout = (plot_dir+"KMDPL2_Xibr_f_pairvel_%dMpc%d_all_SampSize"\
            "-400000_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

    print"----------------Plotting 2*f*(Xibr/Dplus^2)/3 mean vs -vp/Hr --------------------" 
    
    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '<', '^', 's']
    
    for i in xrange(len(snap_red)):
    
        ax1.plot(Xibr_D2_f_mean[i, :], vp[i, :], linestyle='-', 
                 linewidth=1.8, mew=1.5, marker=mark[i], ms=8, 
                 label=r"$\rm{z}=%0.2f$"%snap_red[i])

        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
                       
        ax1.set_ylabel(r'$-\rm{v}_{p}(\rm{r})/\rm{Hr}$', fontsize=22)
        ax1.set_xlabel(r'$\frac{2}{3} f \bar{\xi}(\rm{r})/D_{+}^2$', fontsize=22)
       
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_ylim(0.0001, 10)
        ax1.set_xlim(0.0001, 1000)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=2)#, bbox_to_anchor=(1, 0.5) )
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)

    fout =  (plot_dir+"KMDPL2_Xibr_D2_f_pairvel_%dMpc%d_all_SampSize"\
            "-400000_rmax_200.0Mpc_nbins_50_REAL.png"%(Lbox, Npart))
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    
 
    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    NUM_COLORS = len(snap_red)
    cm = plt.get_cmap('gist_rainbow')
    ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    mark = ['d', 'o', '*', '^']

    mark = ['d', 'o', '<', '^', 's']
    print"----------------Plotting -vp/r vs r --------------------" 
    
    for i in xrange(len(snap_red)):
        
        ax1.plot(rbin_mid, -1*vp1[i,:], linestyle='-', linewidth=1.8, 
                 alpha=0.9, mew=1.5, marker=mark[i], ms=8, label=
                 r"$\rm{z}=%0.2f$"%snap_red[i])
        
        ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
        
        ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
        ax1.set_ylabel(r'$-\rm{v}_{p}(r)/r$', fontsize=22)
        
        ax1.set_xscale("log")
        ax1.set_yscale("log")

        ax1.set_ylim(0.1, 1000)
        ax1.set_xlim(0.1, 210)
        ax1.minorticks_on()
        
        ax1.legend(frameon=False, ncol=1, fontsize=18, loc=1)#, bbox_to_anchor=(1, 0.5) )
        
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)

    fout =  (plot_dir+"KMDPL2_Vp_%dMpc%d_all_SampSize-400000"\
            "_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart))
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

#----------------------------------------------------------------------

def plot_Xi_CovMat(plot_dir, Lbox, Npart, Xi_Cov, 
                   Xibr_Cov, snap_red, snapID):
    
    print"Plotting Covariance matrix of Xi at redshift: %0.1f"%snap_red
    
    fig = plt.figure(1, figsize=(8, 6)) 
    
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 
    
    im = ax1.imshow(Xi_Cov, origin= 'lower', cmap='RdYlBu', 
                    interpolation='none', norm = LogNorm(
                    vmin=0.001, vmax=10))
    
    ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$, $\rm{Z}:%0.1f$"%(Lbox, 
                   Npart, snap_red), fontsize=18)
    
    plt.setp(ax1.get_xticklabels(),visible=False)
    plt.setp(ax1.get_yticklabels(),visible=False)
    ax1.minorticks_on()
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.5)
    ax1.spines['left'].set_linewidth(1.5)
    ax1.spines['top'].set_linewidth(1.5)
    ax1.spines['right'].set_linewidth(1.5)
    plt.tight_layout()
    cbar = plt.colorbar(im, pad=0.04)#, fraction=0.046, pad=0.04)
    cbar.set_label(r"$C_{ij}$",fontsize=20, rotation=0, labelpad=15)

    fout= (plot_dir+"KMDPL2_Cov_Xi_D2_%dMpc%d_SnapID_%d_"\
          "SampSize_400000_rmax_204.0Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))
          
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    
    print"Plotting Covariance matrix of Xibar at redshift: %0.1f"%snap_red

    fig = plt.figure(1, figsize=(8, 6)) 
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 

    im = ax1.imshow(Xibr_Cov, origin= 'lower', cmap='RdYlBu', 
                    interpolation='none', norm = LogNorm(vmin=0.001, vmax=10) )
    
    ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$, $\rm{Z}:%0.1f$"%(Lbox, 
                   Npart, snap_red), fontsize=18)
    
    ax1.minorticks_on()
    
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.5)
    ax1.spines['left'].set_linewidth(1.5)
    ax1.spines['top'].set_linewidth(1.5)
    ax1.spines['right'].set_linewidth(1.5)
    plt.tight_layout()
    
    cbar = plt.colorbar(im, pad=0.04)#, fraction=0.046, pad=0.04)

    cbar.set_label(r"$\bar{C}_{ij}$",fontsize=20, rotation=0, labelpad=15)
    
    fout= (plot_dir+"KMDPL2_Cov_Xibr_D2_%dMpc%d_SnapID_%d_"\
          "SampSize_400000_rmax_204.0Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))
    
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

#----------------------------------------------------------------------------

def plot_Xi_CorrMat(plot_dir, Lbox, Npart, Xi_corrcoef, 
                    Xibr_corrcoef, snap_red, snapID):

    print"Plotting Correlation matrix of Xi at redshift: %0.1f"%snap_red
    
    fig = plt.figure(1, figsize=(8, 6)) 
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 
    im = ax1.imshow(Xi_corrcoef, origin= 'lower', cmap='RdYlBu', 
                    interpolation='none', norm = LogNorm(vmin=0.01, vmax=1) )
                    
    ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$, $\rm{Z}:%0.1f$"%(Lbox, 
                   Npart, snap_red), fontsize=18)
    
    ax1.minorticks_on()
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.5)
    ax1.spines['left'].set_linewidth(1.5)
    ax1.spines['top'].set_linewidth(1.5)
    ax1.spines['right'].set_linewidth(1.5)
    plt.tight_layout()
    cbar = plt.colorbar(im, pad=0.04)#, fraction=0.046, pad=0.04)
    cbar.set_label(r"$R_{ij}$",fontsize=20, rotation=0, labelpad=15)

    fout= (plot_dir+"KMDPL2_CorrCoeff_Xi_D2_%dMpc%d_SnapID_%d_SampSize"\
          "_400000_rmax_204.0Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))
    
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    
    print"Plotting Correlation matrix of Xibr at redshift: %0.1f"%snap_red

    fig = plt.figure(1, figsize=(8, 6)) 
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 

    im = ax1.imshow(Xibr_corrcoef, origin= 'lower', cmap='RdYlBu', 
                    interpolation='none', norm = LogNorm(
                    vmin=0.01, vmax=1) )
    
    ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$, $\rm{Z}:%0.1f$"%(Lbox, 
                   Npart, snap_red), fontsize=18)
        
    ax1.minorticks_on()
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.5)
    ax1.spines['left'].set_linewidth(1.5)
    ax1.spines['top'].set_linewidth(1.5)
    ax1.spines['right'].set_linewidth(1.5)
    plt.tight_layout()
    cbar = plt.colorbar(im, pad=0.04)#, fraction=0.046, pad=0.04)
    cbar.set_label(r"$\bar{R}_{ij}$",fontsize=20, rotation=0, labelpad=15)
    
    fout= (plot_dir + "KMDPL2_CorrCoeff_Xibr_D2_%dMpc%d_SnapID_%d"\
          "_SampSize_400000_rmax_204.0Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))

    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

#---------------------------------------------------------------------- 

def plot_SnapCorr(plot_dir, Lbox, Npart, xi_D2_mean, xibr_D2_mean, 
                  rbin_mid, r_th, xi_th, xibr_th, snap_red, snapID):


    fig= plt.figure(1, figsize=(8, 6))
#    gs = gridspec.GridSpec(2, 1, hspace=0.0, height_ratios=[3,1])
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    mark = ['d', 'o', '<', '^', 's']
    
    ax1.plot(rbin_mid, xi_D2_mean, linestyle='', color='b', 
             linewidth=1.8, mew=1.2, marker=mark[1], ms=8, 
             label=r"$\rm{DM}$, $\rm{z}:%0.1f$"%snap_red)

    ax1.plot(r_th, xi_th, linestyle='-', color='orangered', 
             linewidth=2, label=r"$\xi_{\rm{Linear}}$")

    ax1.axhline(y=1, color='k', linestyle='--', lw=2)
    
    ax1.set_title(r"$\rm{BoxSize}:%d$, $\rm{h}^{-1}\rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)

    ax1.set_ylabel(r'$\xi_{\rm{DM}}(r)/D_{+}^{2}$', fontsize=22)
    ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
    
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax1.set_ylim(0.0004, 1000)
    ax1.set_xlim(0.1, 210)
    ax1.minorticks_on()
#    plt.setp(ax1.get_xticklabels(),visible=False)
    ax1.legend(frameon=False, ncol=1, fontsize=22, loc=1)#, bbox_to_anchor=(1, 0.5) )
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)

    """
    ax2 = plt.subplot(gs[1, 0])
    
    ax2.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
    Xi_interp = np.interp(rbin_mid, r_th, xi_th)
    ratio  = np.abs(Xi_mean-Xi_interp)/Xi_interp

    ax2.plot(rbin_mid, ratio*100, linestyle='-', linewidth=2)
    ax2.set_ylabel(r'$\Delta (\%)$', fontsize=20)
    ax2.set_yscale("log")
    ax2.set_xscale("log")
    ax2.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
    ax2.set_xlim(0.3, 210)
    

    ax2.minorticks_on()
    ax2.legend(frameon=False, ncol=1, fontsize=18, loc=1)#, bbox_to_anchor=(1, 0.5) )
    ax2.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.8)
    ax2.spines['left'].set_linewidth(1.8)
    ax2.spines['top'].set_linewidth(1.8)
    ax2.spines['right'].set_linewidth(1.8)
    """

    fout= (plot_dir+"KMDPL2_Xi_D2_%dMpc%d_SnapID_%d_"\
          "SampSize_400000_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))

    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()

    print"----------------Plotting Xibr mean--------------------" 

    fig= plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    mark = ['o', 'd', '*', '^']

    ax1.plot(rbin_mid, xibr_D2_mean, linestyle='', color='b', 
             mew=1.2, marker=mark[0], ms=8, label=r"$\rm{DM}$, $\rm{z}:%0.1f$"%snap_red)
             
    ax1.plot(r_th, xibr_th, linestyle='-',color='orangered', linewidth=2, 
             label=r"$\bar{\xi}_{\rm{Linear}}$")
             
    ax1.axhline(y=1, color='k', linestyle='--', lw=2)
    
    ax1.set_title(r"$\rm{BoxSize}:%d$ $\rm{h}^{-1} \rm{Mpc}$, $\rm{Npart}:%0.1f^{3}$"%(Lbox, Npart), fontsize=20)
                   
    ax1.set_ylabel(r'$\bar{\xi}_{DM}(r)/D_{+}^{2}$', fontsize=22)
    ax1.set_xlabel(r'$\rm{r}(\rm{h}^{-1} \rm{Mpc})$', fontsize=22)
    
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax1.set_ylim(0.0004, 1000)
    ax1.set_xlim(0.1, 220)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=22, loc=1)
    
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    
    fout= (plot_dir+ "KMDPL2_Xibr_D2_%dMpc%d_SnapID_%d"\
           "_SampSize_400000_rmax_204.8Mpc_nbins_50_REAL.png"%(Lbox, Npart, snapID))
           
    fig.savefig(fout, bbox_inches='tight', dpi=300)
    plt.close()
    

#---------------------------------------------------------------------- 

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='Lbox e.g 1024 for KMDPL2')
    parser.add_argument("NpartSim", type=int, 
    help='Num part simulation e.g 1024, 512 for KMDPL2')
    parser.add_argument("samp_size", type=int, 
    help='Subsample size e.g 400000 for KMDPL2')

    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    
    Lbox       = args.Lbox
    NpartSim   = args.NpartSim
    samp_size  = args.samp_size
    
    snapshots = np.array([16, 15, 13, 11, 8])
    fname= "/mnt/data1/sandeep/velocity_correlation/Xi_theory/multidark2_Xiofr_z0_norm.dat"
    r_th = np.genfromtxt(fname, usecols=0)
    xi_th= np.genfromtxt(fname, usecols=1)
    xibr_th= np.genfromtxt(fname, usecols=2)

#-------------------------------------------------------------------------------

    temp = ("./snapshot_11_data/new_corr_results/Avg_stat_snapshot"\
            "-11_Lbox-1024_Npart-1024_nsub-400000_rmax204.8Mpc_nbins_50.dat")
    rbin_mid = ascii.read(temp)['rbin_mid']

    Xi_mean        = np.zeros((len(snapshots), rbin_mid.size))
    Xi2_mean        = np.zeros((len(snapshots), rbin_mid.size))
    Xi4_mean        = np.zeros((len(snapshots), rbin_mid.size))
    Xibr_mean      = np.zeros((len(snapshots), rbin_mid.size))
    
    Xibr_D2_mean   = np.zeros((len(snapshots), rbin_mid.size))
    Xi_D2_mean     = np.zeros((len(snapshots), rbin_mid.size))
    
    Xibr_f_mean    = np.zeros((len(snapshots), rbin_mid.size))
    Xibr_D2_f_mean = np.zeros((len(snapshots), rbin_mid.size))
    
    vp             = np.zeros((len(snapshots), rbin_mid.size))
    vp1            = np.zeros((len(snapshots), rbin_mid.size))
    f_growth       = np.zeros(len(snapshots))
    snap_red       = np.zeros(len(snapshots))
    
    ii=0
    
    plot_dir = './all_real_plots/'
    for snapnumber in snapshots:

        dir_name = ("./snapshot_%d_data/new_corr_results/"%snapnumber)
        fin      = (dir_name+"Avg_stat_snapshot-%d_Lbox-%d_Npart-%d_nsub-"\
                   "%d_rmax204.8Mpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size))
        data     = ascii.read(fin)
    
    #   names    = ['rbin', 'rbin_mid',  'Xi_mean', 'Xi_std', 'Xi2_mean', 'Xi2_std', 'Xi4_mean', 
    #               'Xi4_std', 'Xi_wedge_par_mean', 'Xi_wedge_par_std', 'Xibr_mean', 'Xibr_std', 'Xi_D2_mean',
    #               'Xi_D2_std', 'Xibr_D2_mean', 'Xibr_D2_std', 'Xibr_f_mean', 'Xibr_f_std', 
    #               'Xibr_D2_f_mean', 'Xibr_D2_f_std', 'v_x_mean', 'v_x_std', 'v_x_H_mean', 'v_x_H_std', 
    #               'vdisp_mean', 'vdisp_std'] 

     
        Xi_mean[ii,:]   = data['Xi_mean']
        Xi2_mean[ii,:]  = data['Xi2_mean']
        Xi4_mean[ii,:]  = data['Xi4_mean']
        Xibr_mean[ii,:] = data['Xibr_mean']
        
        Xi_D2_mean[ii,:]    = data['Xi_D2_mean']
        Xibr_D2_mean[ii,:]  = data['Xibr_D2_mean']
        
        xi_D2_mean      = data['Xi_D2_mean']
        xibr_D2_mean    = data['Xibr_D2_mean']
        

        Xibr_f_mean[ii,:]   = data['Xibr_f_mean']
        Xibr_D2_f_mean[ii,:]= data['Xibr_D2_f_mean']
        vp[ii,:]            = data['v_x_H_mean']
        vp1[ii,:]           = data['v_x_mean']
        
        fout = (dir_name+"Cosmo_quant_snapshot-%d_Lbox-%d_Npart-%d_nsub-"\
               "%d_rmax_204.8Mpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size))
        data = ascii.read(fout)
        f_growth[ii] = data['fgrowth']
        snap_red[ii] = data['redshift']

        #print Xi_D2_mean[ii,:]
        

        fout2 = (dir_name+"Cov_Xi_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_nsub-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, 
                 NpartSim, samp_size, Lbox/5.))

        fout3 = (dir_name+"Cov_Xibr_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_nsub-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, 
                 NpartSim, samp_size, Lbox/5.))

        Xi_Cov   = np.loadtxt(fout2)
        Xibr_Cov = np.loadtxt(fout3)
        plot_Xi_CovMat(plot_dir, Lbox, NpartSim, Xi_Cov, 
                        Xibr_Cov, snap_red[ii], snapshots[ii])
 
        fout4 = (dir_name+"Corr_Xi_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_nsub-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, 
                 NpartSim, samp_size, Lbox/5.))

        fout5 = (dir_name+"Corr_Xibr_snapshot-%d_Lbox-%d_Npart-%d"\
                 "_nsub-%d_rmax%0.1fMpc_nbins_50.dat"%(snapnumber, Lbox, NpartSim, samp_size, Lbox/5.))

        Xi_corrcoef  = np.loadtxt(fout4)
        Xibr_corrcoef= np.loadtxt(fout5)
                        
        plot_Xi_CorrMat(plot_dir, Lbox, NpartSim, Xi_corrcoef, 
                        Xibr_corrcoef, snap_red[ii], snapshots[ii])
                   
        plot_SnapCorr(plot_dir, Lbox, NpartSim, (xi_D2_mean), 
                      (xibr_D2_mean), rbin_mid, r_th, xi_th, xibr_th, 
                      snap_red[ii], snapshots[ii])

        ii+=1

 
    plot_Xi_types(plot_dir, Lbox, NpartSim, Xi_D2_mean, Xi2_mean, xi_th, r_th, rbin_mid, snap_red)
    plot_Xibr(plot_dir, Lbox, NpartSim, Xibr_D2_mean, xibr_th, r_th, rbin_mid, snap_red)
    plot_pair_velocity(plot_dir, Lbox, NpartSim, Xibr_mean, Xibr_f_mean, Xibr_D2_f_mean, 
                        vp, vp1, rbin_mid, snap_red)
                       


if __name__ == "__main__":
    main()



