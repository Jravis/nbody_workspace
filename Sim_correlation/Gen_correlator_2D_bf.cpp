/*
This is an open-mpi enabled brute force 
correlation code.
Including functionality for 
2-D real-space for a set of arrays. 
The calculation is for auto-correlation, 
and with or without periodic boundaries. 
Following binning-schems are available
log(r)-mu
log(rperp)- log(rpar)
rperp-rpar. 
Z direction is by default los direction while 
the projected separation, rp is calculated 
in the X-Y plane.
This compute varitey of correaltion functions.
Xi(r, mu)
Xi_0, Xi_2 monopole and quadrapole
Xi_0  = Xi(r) angle avg correaltion function
Xi(rpar, rperp)
Xi_par, Xi_perp from clustring wedges Kayzin et al 2012
*/

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<random>
#include<math.h>
#include<string>
#include <algorithm> 
#include <bits/stdc++.h> 
#include <sstream> // for ostringstream
using namespace std;

char *filename1;
double *pos;
double *vel;
int nsub, sub_samp_size, ndim=3, ndm;
long int *pairs_rpp ;   // pairs as a function of (rpar, rperp)
double   *pairs_vpp ;   // pairs as a function of (rpar, rperp)
double   *pairs_vpp1 ;   // pairs as a function of (rpar, rperp)
double   *pairs_vpp2 ;   // pairs as a function of (rpar, rperp)

int Nb_rpar; // along to line of sight bins
int Nb_rperp; // Perpendicular to line of sight bins

double *rp_bin   ;
double *rperp_bin;

//-------------------------------------------------------------------------------

void init_pair_arrays();
void cleanup();
double* binning(double amin, double amax , 
               int nbin, double *bins, int bin_Key);
                  
void  corr_2D_box_bf(int np, int nb_rp, int nb_rperp, 
                     double *pos, double *vel, long int *p_rpp, 
                     double *p_vpp, double *p_vpp1, double *p_vpp2, 
                     double rp_min, double rp_max, double rperp_min, 
                     double rperp_max, double l_box, double l_box_half, 
                     int bin_key);

//-------------------------------------------------------------------------------

int main(int argc, char **argv){

  int snapid, i, k, j;
//  int nblock_start, nblock_end;
  double nblock_start, nblock_end;
  double avpart,numden, bin_type_rpp;
  double  Rp_min, Rp_max;
  double  Rperp_min, Rperp_max;
  double Lbox, Lbox_half;
  double scale_factor, HubbleParam;
  double xi_rpp;

  double npart_sim, softening_length;
  if(argc != 8){
    cout<< "\n./a.out   [snapid] [Nb_rpar] [Nb_rperp]  [sub_samp_size] [npartsim] [boxsize]]  [bin_type_rpp]"<<"\n";
    cout<< "\n./a.out     16        100        100           400000           512       1024  1 for logbin 0 linear"      <<"\n";
    return(-1);
  }//if


//-------------------------------------------------------------------------------------------------
// Setting up initial parameters

  snapid      = atoi(argv[1]);
  Nb_rpar     = atoi(argv[2]);
  Nb_rperp    = atoi(argv[3]);
  npart_sim   = (double) atof(argv[5]); 
  bin_type_rpp= atoi(argv[6]); 
  
  filename1  = (char *) malloc(500*sizeof(char));
  

  sprintf(filename1,"/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
           "sub_sample_data_%sMpc_%s/%s_sample/REAL/snapshot_%s_data/"
           "subsamp_param_%s.dat",argv[6], argv[5], argv[4], argv[1], argv[1]);

  cout << filename1 << "\n";
  
  FILE  *param_file=NULL;
  param_file = fopen(filename1, "r");
  
  fscanf(param_file,"%d %d %d %d %d %lf %lf %lf %lf\n", 
            &ndim, &sub_samp_size, &nsub, &ndm, &snapid, &scale_factor, 
            &HubbleParam, &Lbox, &Lbox_half);
  fclose(param_file);
 
  
  cout<<"sub_samp_size: "<<sub_samp_size<<"\n" ;
  cout<<"snapid: "<<snapid<<"\n" ;
  cout<<"ndm: "<<ndm<<"\n" ;
  cout<<"Lbox: "<<Lbox<<"\n"; 
  softening_length = (Lbox*1000.)/npart_sim;
  softening_length *= (1./25.);
  
  cout<<"softening length in kpc h^-1: " << softening_length << "\n";
  Rp_min    = 0.0;  
  
  if (bin_type_rpp==1){
      Rperp_min = (softening_length/1000.);
  }
  else{
      Rperp_min = 0;
  }
  
  Rp_max    = 50.0;
  Rperp_max = 50.0;

  numden=(double)(sub_samp_size)/(double)(Lbox*Lbox*Lbox);
  printf("numden = %2.10f, nsub = %d ndim=%d\n",numden, nsub, ndim);
 
//------------------------------------------------------------------------------------------------------
//Setting bins and computing bin volume also

  rp_bin      = (double*) malloc((Nb_rpar)*sizeof(double));
  rperp_bin   = (double*) malloc((Nb_rperp)*sizeof(double));
  
  pairs_rpp  = (long int*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  pairs_vpp  = (double*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  pairs_vpp1  = (double*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  pairs_vpp2  = (double*) malloc(Nb_rpar*Nb_rperp*sizeof(long int));
  
  
  binning(Rp_min   , Rp_max    , Nb_rpar , rp_bin   , 0);
  binning(Rperp_min, Rperp_max , Nb_rperp, rperp_bin, (int) bin_type_rpp);

  double vol_rpp[Nb_rpar*Nb_rperp];// nbin1=Nb_rp(rpar), nbin2=Nb_perp(bin in r_perp)
  double los_bin_diff=0.0; 
  
  for(i=0; i < Nb_rpar; i++){
    for(j=0; j <  Nb_rperp; j++){
        los_bin_diff = rp_bin[i+1]-rp_bin[i];
        vol_rpp[i+Nb_rpar*j] = M_PI * los_bin_diff * (pow(rperp_bin[j+1], 2.0) - pow(rperp_bin[j], 2.0));
        vol_rpp[i+Nb_rpar*j] = 2.0 * vol_rpp[i+Nb_rpar*j];
    }
  }


//------------------------------------------------------------------------------------------------------
// reading data
  FILE *snapfile = NULL;
  pos = (double *)malloc(sub_samp_size*ndim*sizeof(double));
  vel = (double *)malloc(sub_samp_size*ndim*sizeof(double));
  nsub = 50;
  for (k = 0; k<nsub;k++){    

    memset(pos, 0, sub_samp_size*ndim*sizeof(double)); 
    memset(vel, 0, sub_samp_size*ndim*sizeof(double)); 
    
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "KMDPL2_SubSam_%d_%d_%d_phy.bin",(int)Lbox,(int) npart_sim,(int) sub_samp_size,(int) 
            snapid,(int) snapid,(int) sub_samp_size,(int) k);
            
    cout<<filename1<<"\n"; 
    init_pair_arrays();
    snapfile = fopen(filename1, "r");
    //fread(&nblock_start,sizeof(int),1,snapfile);
    fread(&nblock_start,sizeof(double),1,snapfile);
    fread(pos,sizeof(double), sub_samp_size*ndim, snapfile);
    fread(vel,sizeof(double), sub_samp_size*ndim, snapfile);
    fread(&nblock_end, sizeof(double),1, snapfile);
    //fread(&nblock_end, sizeof(int),1, snapfile);
    if( nblock_start != nblock_end ) {
       printf("MISMATCH ... snapid=%lf ... QUITTING \n", nblock_start);
       //printf("MISMATCH ... snapid=%d ... QUITTING \n", nblock_start);
       return(-1);
    }//if

    fclose(snapfile);
    
// reading data done
//------------------------------------------------------------------------------------------------------
//Compute CF
    corr_2D_box_bf(sub_samp_size, Nb_rpar, Nb_rperp, 
                   pos, vel, pairs_rpp, 
                   pairs_vpp, pairs_vpp1, pairs_vpp2,
                   Rp_min, Rp_max, Rperp_min, Rperp_max, 
                   Lbox, Lbox_half, bin_type_rpp);


//------------------------------------------------------------------------------------------------------
//  Make various CF
    
    cout<<"Done"<<"\n";
    sprintf(filename1, "/mnt/data1/sandeep/Kalinga_run_Analysis/kalinga_velocity_correlation/"
            "sub_sample_data_%dMpc_%d/%d_sample/REAL/snapshot_%d_data/"
            "new_corr_results/logbin_2Dcorr_rpp_%d_%d_rmax_%0.1fMpc_Nbrpar"
            "_%d_Nrperp_%d_%d.dat", (int) Lbox, (int) npart_sim, (int) sub_samp_size, 
            (int) snapid,(int) snapid,(int) sub_samp_size, 
             Rp_max,(int) Nb_rpar,(int) Nb_rperp,(int) k);
    
    snapfile = fopen(filename1, "w");
    fprintf(snapfile,"#rpar  rperp  rpp_avpairs  rpp_pairs  xi_rpp  vp_l_rpp  vdis_rpp  vp_rpp\n");
    
   cout<<"Done"<<"\n";
   
    for(i=0; i<Nb_rpar; i++){
      for(j=0; j<Nb_rperp; j++){
      
        avpart=((double)sub_samp_size) * vol_rpp[i+Nb_rpar*j] * numden;
        xi_rpp = ((double) pairs_rpp[i+Nb_rpar*j]/avpart) - 1;
        
        fprintf(snapfile,"%2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f  %2.14f\n",
                rp_bin[i], rperp_bin[j], avpart, (double) pairs_rpp[i+Nb_rpar*j], 
                xi_rpp, pairs_vpp[i+Nb_rpar*j]/(double) pairs_rpp[i+Nb_rpar*j], 
                pairs_vpp1[i+Nb_rpar*j]/(double) pairs_rpp[i+Nb_rpar*j],
                pairs_vpp2[i+Nb_rpar*j]/(double) pairs_rpp[i+Nb_rpar*j] );
      }// for i rp bin
    }//for j rperp_bin
    fclose(snapfile);

   cout<<"Done"<<"\n";

  }//for k 
 cleanup();
  return 0;
}//main
//--------------------------------------------------------------------------------

double* binning(double amin, double amax , 
               int nbin, double *bins, int bin_Key){
    double temp=0.;
    double bin_fac=0.;
    if (bin_Key==1){
        bin_fac = (log10(amax)-log10(amin))/(double)(nbin);
        for(int l=0; l < nbin+1; l++){
           temp    = log10(amin) + l * bin_fac;
           bins[l] =  (pow(10., temp));
        }
    }
    else{
         bin_fac = (amax-amin)/(double)(nbin);
        for(int l=0; l < nbin+1; l++){
           temp    = (amin) + l * bin_fac;
           bins[l] =  temp;
        }

    }
    return bins;
}//binning()

void init_pair_arrays(){
  int i;
  for(i=0; i<Nb_rpar*Nb_rperp; i++){
    pairs_rpp[i] = 0;
    pairs_vpp[i] = 0.0;
    pairs_vpp1[i] = 0.0;
    pairs_vpp2[i] = 0.0;
    }


}//init_pair_arrays()


void cleanup(){
  
  free(pos);
  free(vel);
  free(rp_bin);
  free(rperp_bin);
  free(pairs_rpp);
  free(pairs_vpp1);
  free(pairs_vpp2);
  free(filename1);
}

//------------------------------------------------------------------------

void  corr_2D_box_bf(int np, int nb_rp, int nb_rperp, 
                     double *pos, double *vel, long int *p_rpp, 
                     double *p_vpp, double *p_vpp1, double *p_vpp2, 
                     double rp_min, double rp_max, double rperp_min, 
                     double rperp_max, double l_box, double l_box_half, int bin_key){

 //////
 // Correlator for monopole in the periodic-box case
 // by brute-force
 double delta_rperp;
 if(bin_key==1){ 
   delta_rperp = log10(rperp_max/rperp_min)/(double) nb_rperp;
 }
 else{
   delta_rperp = (rperp_max-rperp_min)/(double) nb_rperp;
 }


 double delta_rp    = (rp_max-rp_min)/(double) nb_rp;
  
#pragma omp parallel default(none)      \
  shared(np, nb_rp, nb_rperp,\
         pos, vel, p_rpp, p_vpp, p_vpp1, p_vpp2,\
         rp_min, rp_max, rperp_min,\
         rperp_max, l_box, l_box_half, bin_key, delta_rp, delta_rperp)
  {
    int ii, kk;
    long int p_rpp_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
    double   p_vpp_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
    double   p_vpp1_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
    double   p_vpp2_hthread[nb_rp*nb_rperp]; //Histogram filled by each thread for log(r)-mu
    
    for(ii=0;ii<nb_rp*nb_rperp;ii++){
        p_rpp_hthread[ii]=0.; //Clear private histogram
        p_vpp_hthread[ii]=0.; //Clear private histogram
        p_vpp1_hthread[ii]=0.; //Clear private histogram
        p_vpp2_hthread[ii]=0.; //Clear private histogram
    }
    


#pragma omp for nowait schedule(dynamic)
    for(ii=0;ii<np;ii++) {
      int jj; 
      double *pos1=&(pos[3*ii]);
      double *vel1=&(vel[3*ii]);
      
      for(jj=0;jj<np;jj++) {

        double xr[3];//, mu;
        double r2, pi, rperp;
        int  ipi, irprep;        
        double vr[3];
        double v2, rdotv;//, rCrossv[3];

        xr[0]=(pos1[0]-pos[3*jj]);
        xr[1]=(pos1[1]-pos[3*jj+1]);
        xr[2]=(pos1[2]-pos[3*jj+2]);
    
 
        if(fabs(xr[0])>l_box_half){
            xr[0]=l_box-fabs(xr[0]);
            if (xr[0]<0){
                xr[0]=-1.0*fabs(xr[0]);
            }
            else{
                xr[0]=1.0*fabs(xr[0]);
            }
        }
        if(fabs(xr[1])>l_box_half){
            xr[1]=l_box-fabs(xr[1]);
            if (xr[1]<0){
                xr[1]=-1.0*fabs(xr[1]);
            }
            else{
                xr[1]=1.0*fabs(xr[1]);
            }
        }
        if(fabs(xr[2])>l_box_half){
            xr[2]=l_box-fabs(xr[2]);
            if (xr[2]<0){
                xr[2]=-1.0*fabs(xr[2]);
            }
            else{
                xr[2]=1.0*fabs(xr[2]);
            }
        }
        
        vr[0] = (vel1[0]-vel[0+3*jj]);
        vr[1] = (vel1[1]-vel[1+3*jj]);
        vr[2] = (vel1[2]-vel[2+3*jj]);
        
        r2         = xr[0]*xr[0]+xr[1]*xr[1]+xr[2]*xr[2]; 
        v2         = vr[0]*vr[0]+vr[1]*vr[1]+vr[2]*vr[2]; 
        rdotv      = xr[0]*vr[0]+xr[1]*vr[1]+xr[2]*vr[2]; 
        

        //rCrossv[0] = xr[1]*vr[2]-xr[2]*vr[1];
		//rCrossv[1] = xr[2]*vr[0]-xr[0]*vr[2];
		//rCrossv[2] = xr[0]*vr[1]-xr[1]*vr[0];
        //mu = fabs(xr[2]/sqrt(r2)); 
        pi = fabs(xr[2]);
        rperp = sqrt(xr[0]*xr[0]+xr[1]*xr[1]);

//************************log(rpar)-rper*********************
        if(bin_key==1){ // log binning
            if( (pi>=rp_min) && (pi<=rp_max) ){
              if( (rperp>=rperp_min) && (rperp<=rperp_max) ){
                //ipi     =  (int)(log10(pi/rp_min)/delta_rp);
                ipi     =  (int)((pi-rp_min)/delta_rp);
                irprep  =  (int)(log10(rperp/rperp_min)/delta_rperp);
                p_rpp_hthread[ipi+nb_rp*(irprep)]+=1;
              }//if rper
            }//if rpar
        }
//************************rpar-rper*********************
        else{
          if( (pi>=rp_min) && (pi<=rp_max) ){
            if( (rperp>=rperp_min) && (rperp<=rperp_max) ){
              ipi     =  (int)((pi-rp_min)/delta_rp);
              irprep  =  (int)((rperp-rperp_min)/delta_rperp);
              p_rpp_hthread[ipi+nb_rp*(irprep)]+= 1;
              p_vpp_hthread[ipi+nb_rp*(irprep)]+= rdotv/r2;
              p_vpp1_hthread[ipi+nb_rp*(irprep)]+= sqrt(v2);
              p_vpp2_hthread[ipi+nb_rp*(irprep)]+= rdotv/sqrt(r2);            
            }//if rper
          }//if rpar
        }
      }//jj
    }//ii
    
#pragma omp critical
    {
      
     for(ii=0;ii<nb_rp;ii++) //Check bound
        for(kk=0;kk<nb_rperp;kk++){ //Check bound
            p_rpp[ii+nb_rp*kk]+= p_rpp_hthread[ii+nb_rp*kk]; //Add private histograms to shared one
            p_vpp[ii+nb_rp*kk]+= p_vpp_hthread[ii+nb_rp*kk]; //Add private histograms to shared one
            p_vpp1[ii+nb_rp*kk]+= p_vpp1_hthread[ii+nb_rp*kk]; //Add private histograms to shared one
            p_vpp2[ii+nb_rp*kk]+= p_vpp1_hthread[ii+nb_rp*kk]; //Add private histograms to shared one
        }

    } // end pragma omp critical
    
  } // end pragma omp parallel

}


